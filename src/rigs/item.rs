use json::JsonValue;

use crate::utils::ItemClass;

pub trait ItemOverview {
    fn get_image(&self) -> &str;
    fn get_name(&self) -> &str;
    fn get_url(&self) -> &str;
    fn get_class(&self) -> &ItemClass;
    fn get_unique_name(&self) -> &str;
    fn get_json(&self) -> &JsonValue;
}
