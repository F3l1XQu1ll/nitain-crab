use json::JsonValue;

pub trait Component {
    fn get_unique_name(&self) -> &str;
    fn get_name(&self) -> &str;
    fn get_desc(&self) -> &str;
    fn get_count(&self) -> &u32;
    fn get_image(&self) -> &str;
    fn get_tradable(&self) -> &bool;
    fn get_drops(&self) -> Option<JsonValue>;
}

pub struct UniversalComponent {
    unique_name: String,
    name: String,
    desc: String,
    count: u32,
    image: String,
    tradable: bool,
    drops: Option<JsonValue>,
}

impl UniversalComponent {
    pub fn new(unique_name: &str, name: &str, desc: &str, count: u32, image: &str, tradable: bool, drops: Option<JsonValue>) -> UniversalComponent {
        UniversalComponent {
            unique_name: unique_name.to_string(),
            name: name.to_string(),
            desc: desc.to_string(),
            count,
            image: image.to_string(),
            tradable,
            drops,
        }
    }
}

impl Component for UniversalComponent {
    fn get_unique_name(&self) -> &str {
        &self.unique_name
    }

    fn get_name(&self) -> &str {
        &self.name
    }

    fn get_desc(&self) -> &str {
        &self.desc
    }

    fn get_count(&self) -> &u32 {
        &self.count
    }

    fn get_image(&self) -> &str {
        &self.image
    }

    fn get_tradable(&self) -> &bool {
        &self.tradable
    }

    fn get_drops(&self) -> Option<JsonValue> {
        self.drops.clone()
    }
}