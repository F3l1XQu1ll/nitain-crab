use json::JsonValue;

use crate::rigs::item::ItemOverview;
use crate::utils::ItemClass;

#[derive(Clone)]
pub struct WarframeOverview {
    name: String,
    unique_name: String,
    url: String,
    class: ItemClass,
    image: String,
    json: JsonValue,
}

impl WarframeOverview {
    pub fn new(name: String, unique_name: String, url: String, class: ItemClass, image: String, json: JsonValue) -> WarframeOverview {
        WarframeOverview {
            name,
            unique_name,
            url,
            class,
            image,
            json,
        }
    }
}

impl ItemOverview for WarframeOverview {
    fn get_image(&self) -> &str { &self.image }
    fn get_name(&self) -> &str { &self.name }
    fn get_url(&self) -> &str { &self.url }
    fn get_class(&self) -> &ItemClass { &self.class }
    fn get_unique_name(&self) -> &str { &self.unique_name }
    fn get_json(&self) -> &JsonValue { &self.json }
}