use std::usize;

#[derive(Clone)]
pub enum ModPolarity {
    V,
    D,
    Bar,
    Scratch,
    R,
    Y,
    U,
    O,
    None,
}
pub trait ModGrid {
    fn get_slot(&self, row: usize, column: usize) -> &ModSlot;
}

pub trait GridHasAura {
    fn get_aura(&self) -> &ModSlot;
}

pub trait GridHasExilus {
    fn get_exilus(&self) -> &ModSlot;
}

pub trait GridHasArcanes {
    fn get_arcane(&self, element: usize) -> &ModSlot;
}

pub struct WarframeModGrid {
    rows: Vec<Vec<ModSlot>>,
    aura: ModSlot,
    exilus: ModSlot,
    arcanes: Vec<ModSlot>,
}

#[derive(Clone)]
pub struct ModSlot {
    asigned_mod: Option<ItemMod>,
}

#[derive(Clone)]
pub struct ItemMod {
    name: &'static str,
    unique_name: &'static str,
    desc: &'static str,
    image: &'static str,
    polarity: ModPolarity,
    rank: u32,
    stats: ModStats,
}

#[derive(Clone)]
pub struct ModStats {
    stats: Vec<String>,
}

#[allow(dead_code)]
impl ModSlot {
    fn get_mod(&self) -> &Option<ItemMod> {
        &self.asigned_mod
    }

    fn set_mod(&mut self, item_mod: Option<ItemMod>) {
        self.asigned_mod = item_mod;
    }
}

impl ModGrid for WarframeModGrid {
    fn get_slot(&self, row: usize, column: usize) -> &ModSlot {
        &self.rows[row][column]
    }
}

impl GridHasAura for WarframeModGrid {
    fn get_aura(&self) -> &ModSlot {
        &self.aura
    }
}

impl GridHasExilus for WarframeModGrid {
    fn get_exilus(&self) -> &ModSlot {
        &self.exilus
    }
}

impl GridHasArcanes for WarframeModGrid {
    fn get_arcane(&self, element: usize) -> &ModSlot {
        &self.arcanes[element]
    }
}

#[allow(dead_code)]
impl ModStats {
    fn get_stats(&self) -> Vec<String> {
        self.stats.clone()
    }
}
