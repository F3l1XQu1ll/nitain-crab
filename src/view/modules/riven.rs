use std::{
    collections::{BTreeMap, HashMap},
    convert::TryInto,
    option::Option::Some,
    thread,
};

use chrono::{DateTime, Duration, FixedOffset, Utc};
use glib::Type;
use gtk::prelude::*;
use gtk::{Box, Grid, Orientation};
use gtk::{
    CellRendererText, Label, ListBox, ListStore, ScrolledWindowBuilder, SortColumn, TreeView,
    TreeViewColumn,
};
use json::JsonValue;
use pango::{AttrList, Attribute};
use reqwest::{blocking::Response, Client, Error};

use crate::rigs::item::ItemOverview;
use crate::uplink::fetch::do_fetch;

trait RivenHasReroll {
    fn get_rerolled(&self) -> bool;
}

trait RivenHasPriceSpec {
    fn get_price_min(&self) -> f32;
    fn get_price_max(&self) -> f32;
    fn get_price_avg(&self) -> f32;
}

trait RivenValueExt {
    fn get_buff(&self) -> RivenBuff;
    fn get_curse(&self) -> bool;
}

trait SemlarRivenExt {
    fn get_price(&self) -> i32;
    fn get_values(&self) -> Vec<RivenValue>;
    fn get_rolls(&self) -> i32;
    fn get_last_seen(&self) -> DateTime<FixedOffset>;
}
struct RivenPrice {
    rerolled: bool,
    avg: f32,
    min: f32,
    max: f32,
    pop: i32,
}

#[derive(Clone, Copy)]
struct RivenValue {
    buff: RivenBuff,
    curse: bool,
}

#[derive(Clone)]
struct SemlarRiven {
    price: i32,
    values: Vec<RivenValue>,
    rolls: i32,
    last_seen: DateTime<FixedOffset>,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
enum RivenBuff {
    ComboCountChance,
    AmmoMaximum,
    DmgCorpus,
    DmgGrineer,
    DmgInfested,
    Cold,
    ComboDuration,
    CriticalChance,
    CriticalChanceSlide,
    CriticalDmg,
    Damage,
    Electricity,
    Heat,
    FinisherDmg,
    Speed,
    ProjectileSpeed,
    Combo,
    Impact,
    Magazine,
    ComboEfficiency,
    Multishot,
    Toxin,
    PunchThrough,
    Puncture,
    Reload,
    Range,
    Slash,
    StatusChance,
    StatusDuration,
    Recoil,
    Zoom,
}

impl RivenHasReroll for RivenPrice {
    fn get_rerolled(&self) -> bool {
        self.rerolled
    }
}

impl RivenHasPriceSpec for RivenPrice {
    fn get_price_min(&self) -> f32 {
        self.min
    }

    fn get_price_max(&self) -> f32 {
        self.max
    }

    fn get_price_avg(&self) -> f32 {
        self.avg
    }
}

impl RivenPrice {
    fn new(rerolled: bool, avg: f32, min: f32, max: f32, pop: i32) -> RivenPrice {
        RivenPrice {
            rerolled,
            avg,
            min,
            max,
            pop,
        }
    }

    fn get_popularity(&self) -> i32 {
        self.pop
    }
}

impl RivenValueExt for RivenValue {
    fn get_buff(&self) -> RivenBuff {
        self.buff
    }

    fn get_curse(&self) -> bool {
        self.curse
    }
}

impl RivenValue {
    fn new(buff: RivenBuff, curse: bool) -> RivenValue {
        RivenValue { buff, curse }
    }
}

impl SemlarRivenExt for SemlarRiven {
    fn get_price(&self) -> i32 {
        self.price
    }
    fn get_rolls(&self) -> i32 {
        self.rolls
    }

    fn get_values(&self) -> Vec<RivenValue> {
        self.values.clone()
    }

    fn get_last_seen(&self) -> DateTime<FixedOffset> {
        self.last_seen
    }
}

impl SemlarRiven {
    fn new(
        price: i32,
        values: Vec<RivenValue>,
        rolls: i32,
        last_seen: DateTime<FixedOffset>,
    ) -> SemlarRiven {
        SemlarRiven {
            price,
            values,
            rolls,
            last_seen,
        }
    }
}

pub fn build_riven(rig: &std::boxed::Box<dyn ItemOverview>) -> Box {
    let root = Box::new(Orientation::Horizontal, 5);

    let riven_grid = Grid::new();
    riven_grid.set_row_spacing(5);

    let desc_label = Label::new(Some(
        format!("Riven Information for {}", rig.get_name()).as_str(),
    ));

    if let Some(attr) = Attribute::new_scale(2.0) {
        //debug!("Attrs exist!");
        let attrs = AttrList::new();
        attrs.insert(attr);
        desc_label.set_attributes(Some(&attrs));
    }

    let weapon_name = rig.get_name().to_string();
    //let semlar_url = format!(
    //    "https://semlar.com/rivenprices/{}",
    //    weapon_name.to_lowercase()
    //);
    let url = String::from("https://n9e5v4d8.ssl.hwcdn.net/repos/weeklyRivensPC.json");

    let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

    thread::spawn(move || {
        let rt = tokio::runtime::Runtime::new().expect("Failed to initialize the tokio runtime!");
        let async_response = rt.block_on(do_fetch(url.as_str(), Client::new()));
        if let Err(e) = tx.send(async_response) {
            error!("{:?}", e.to_string())
        }
    });
    let grid_clone = riven_grid.clone();
    rx.attach(None, move |async_response| match async_response {
        Ok(weekly_riven) => {
            let json = json::parse(weekly_riven.as_str());
            if let Ok(json) = json {
                let mut matching_rivens = Vec::new();
                for riven_info in json.members() {
                    let compatibility = riven_info["compatibility"].clone();
                    if let Some(compatibility) = compatibility.as_str() {
                        let unprimed = weapon_name.replace(" Prime", "").to_uppercase();
                        let fallback_primed = weapon_name.to_uppercase();
                        if *compatibility == unprimed || *compatibility == fallback_primed {
                            matching_rivens.push(riven_info);
                        }
                    }
                }

                let mut to_compare_rivens_rolled = Vec::new();

                for riven in matching_rivens {
                    let rerolled = riven["rerolled"].as_bool().unwrap_or_default();
                    let avg = riven["avg"].as_f32().unwrap_or_default();
                    let min = riven["min"].as_f32().unwrap_or_default();
                    let max = riven["max"].as_f32().unwrap_or_default();
                    let pop = riven["pop"].as_i32().unwrap_or_default();
                    let price = RivenPrice::new(rerolled, avg, min, max, pop);
                    to_compare_rivens_rolled.push(price);
                }

                let grid_compare = Grid::new();
                let mut compare_boxes = Vec::new();
                for item in &to_compare_rivens_rolled {
                    let label_rerolled = Label::new(Some(&item.get_rerolled().to_string()));
                    let label_avg = Label::new(Some(&item.get_price_avg().to_string()));
                    let label_min = Label::new(Some(&item.get_price_min().to_string()));
                    let label_max = Label::new(Some(&item.get_price_max().to_string()));
                    let label_pop = Label::new(Some(&item.get_popularity().to_string()));

                    let compare_box = ListBox::new();
                    compare_box.add(&label_rerolled);
                    compare_box.add(&label_avg);
                    compare_box.add(&label_min);
                    compare_box.add(&label_max);
                    compare_box.add(&label_pop);
                    compare_box.set_selection_mode(gtk::SelectionMode::None);
                    compare_boxes.push(compare_box);
                }

                let label_rerolled_desc = Label::new(Some("Rerolled"));
                let label_avg_desc = Label::new(Some("Average Price"));
                let label_min_desc = Label::new(Some("Minimal Price"));
                let label_max_desc = Label::new(Some("Maximum Price"));
                let label_pop_desc = Label::new(Some("Popularity"));
                grid_compare.attach(&label_rerolled_desc, 0, 0, 1, 1);
                grid_compare.attach(&label_avg_desc, 0, 1, 1, 1);
                grid_compare.attach(&label_min_desc, 0, 2, 1, 1);
                grid_compare.attach(&label_max_desc, 0, 3, 1, 1);
                grid_compare.attach(&label_pop_desc, 0, 4, 1, 1);
                grid_compare.set_column_homogeneous(true);

                for (i, list) in compare_boxes.iter().enumerate() {
                    grid_compare.attach(list, (i + 1).try_into().unwrap(), 0, 1, 5);
                }

                grid_compare.set_column_spacing(10);

                grid_clone.attach(&desc_label, 0, 0, 1, 1);

                let compare_desc =
                    Label::new(Some("Average prices for rolled and unrolled rivens"));
                if let Some(compare_desc_scale) = Attribute::new_scale(1.5) {
                    let compare_desc_attrs = AttrList::new();
                    compare_desc_attrs.insert(compare_desc_scale);
                    compare_desc.set_attributes(Some(&compare_desc_attrs));
                }
                grid_clone.attach_next_to(
                    &compare_desc,
                    Some(&desc_label),
                    gtk::PositionType::Bottom,
                    1,
                    1,
                );

                grid_clone.attach_next_to(
                    &grid_compare,
                    Some(&compare_desc),
                    gtk::PositionType::Bottom,
                    1,
                    1,
                );

                if let Some(history) = build_history(weapon_name.as_str()) {
                    let history_scroll = ScrolledWindowBuilder::new().build();
                    let history_desc =
                        Label::new(Some("Average platinum prices for the most sold stats"));
                    if let Some(history_desc_scale) = Attribute::new_scale(1.5) {
                        let history_desc_attrs = AttrList::new();
                        history_desc_attrs.insert(history_desc_scale);
                        history_desc.set_attributes(Some(&history_desc_attrs));
                    }

                    history_scroll.add(&history);
                    history_scroll.set_vexpand(true);
                    grid_clone.attach_next_to(
                        &history_desc,
                        Some(&grid_compare),
                        gtk::PositionType::Bottom,
                        1,
                        1,
                    );
                    grid_clone.attach_next_to(
                        &history_scroll,
                        Some(&history_desc),
                        gtk::PositionType::Bottom,
                        1,
                        1,
                    );
                }
            }
            grid_clone.show_all();
            Continue(false)
        }
        Err(error) => {
            warn!("{}", error.to_string());
            Continue(false)
        } //Continue(false)
    });

    root.set_center_widget(Some(&riven_grid));

    root
}

fn load_history() -> Result<Response, Error> {
    reqwest::blocking::get("https://10o.io/pricehistory.json?v2")
}

fn find_weapon_in_history(history: JsonValue, to_find_name: &str) -> Option<JsonValue> {
    for weapon in history.members() {
        let name = weapon["name"].as_str().unwrap_or_default();
        if name == to_find_name.to_uppercase().as_str()
            || name == to_find_name.replace(" Prime", "").to_uppercase().as_str()
        {
            return Some(weapon.clone());
        } else {
            continue;
        }
    }
    None
}

fn riven_buff_to_str(buff: RivenBuff) -> &'static str {
    match buff {
        RivenBuff::AmmoMaximum => "Ammo Maximum",
        RivenBuff::ComboCountChance => "Additional Combo Count Chance",
        RivenBuff::ComboDuration => "Combo Duration",
        RivenBuff::CriticalDmg => "Critical Damage",
        RivenBuff::CriticalChance => "Critical Chance",
        RivenBuff::CriticalChanceSlide => "Critical Chance on Slide Attack",
        RivenBuff::Cold => "Cold Damage",
        RivenBuff::Combo => "Initial Combo",
        RivenBuff::ComboEfficiency => "Combo Efficiency",
        RivenBuff::Damage => "Base Damage",
        RivenBuff::DmgCorpus => "Damage to Corpus",
        RivenBuff::DmgGrineer => "Damage to Grineer",
        RivenBuff::DmgInfested => "Damage to Infested",
        RivenBuff::Electricity => "Electricity Damage",
        RivenBuff::FinisherDmg => "Finisher Damage",
        RivenBuff::Heat => "Heat Damage",
        RivenBuff::Impact => "Impact Damage",
        RivenBuff::Magazine => "Magazine Capacity",
        RivenBuff::Multishot => "Multishot",
        RivenBuff::ProjectileSpeed => "Projectile Speed",
        RivenBuff::PunchThrough => "Punch through",
        RivenBuff::Puncture => "Puncture Damage",
        RivenBuff::Range => "Range",
        RivenBuff::Recoil => "Recoil",
        RivenBuff::Reload => "Reload Speed",
        RivenBuff::Slash => "Slash Damage",
        RivenBuff::Speed => "Fire Rate / Attack Speed",
        RivenBuff::StatusChance => "Status Chance",
        RivenBuff::StatusDuration => "Status Duration",
        RivenBuff::Toxin => "Toxin Damage",
        RivenBuff::Zoom => "Zoom",
    }
}

fn riven_str_to_buff(string: &str) -> RivenBuff {
    match string {
        "Ammo Maximum" => RivenBuff::AmmoMaximum,
        "Additional Combo Count Chance" | "Chance to Gain Combo Count" => {
            RivenBuff::ComboCountChance
        }
        "Combo Duration" => RivenBuff::ComboDuration,
        "Critical Chance" | "Critical Chance (x2 for Heavy Attacks)" => RivenBuff::CriticalChance,
        "Critical Chance for Slide Attack" => RivenBuff::CriticalChanceSlide,
        "Critical Damage" => RivenBuff::CriticalDmg,
        "Cold" => RivenBuff::Cold,
        "Initial Combo" => RivenBuff::Combo,
        "Melee Combo Efficiency" => RivenBuff::ComboEfficiency,
        "Damage" | "Melee Damage" => RivenBuff::Damage,
        "Damage to Corpus" => RivenBuff::DmgCorpus,
        "Damage to Grineer" => RivenBuff::DmgGrineer,
        "Damage to Infested" => RivenBuff::DmgInfested,
        "Electricity" => RivenBuff::Electricity,
        "Finisher Damage" => RivenBuff::FinisherDmg,
        "Fire Rate (x2 for Bows)" | "Attack Speed" => RivenBuff::Speed,
        "Heat" => RivenBuff::Heat,
        "Impact" => RivenBuff::Impact,
        "Magazine Capacity" => RivenBuff::Magazine,
        "Multishot" => RivenBuff::Multishot,
        "Projectile Speed" => RivenBuff::ProjectileSpeed,
        "Punch Through" => RivenBuff::PunchThrough,
        "Puncture" => RivenBuff::Puncture,
        "Range" => RivenBuff::Range,
        "Reload Speed" => RivenBuff::Reload,
        "Weapon Recoil" => RivenBuff::Recoil,
        "Slash" => RivenBuff::Slash,
        "Status Chance" => RivenBuff::StatusChance,
        "Status Duration" => RivenBuff::StatusDuration,
        "Toxin" => RivenBuff::Toxin,
        "Zoom" => RivenBuff::Zoom,
        &_ => {
            warn!("Unknown Buff '{}'", string);
            RivenBuff::Zoom
        }
    }
}

fn parse_rivens(history: JsonValue) -> Vec<SemlarRiven> {
    let mut rivens = Vec::new();
    for riven_info in history["data"].members() {
        let price = riven_info["price"].as_i32().unwrap_or_default();
        let values = get_riven_values(riven_info);

        let rolls = riven_info["rolls"].as_i32().unwrap_or_default();
        if rolls == 0 {
            continue;
        }

        let last_seen_str = riven_info["lastseen"].as_str().unwrap_or_default();
        if let Ok(last_seen) = DateTime::parse_from_rfc3339(last_seen_str) {
            if Utc::now().signed_duration_since(last_seen) <= Duration::weeks(1) {
                let semlar_riven = SemlarRiven::new(price, values, rolls, last_seen);
                rivens.push(semlar_riven);
            }
        }
    }

    rivens
}

fn build_history(name: &str) -> Option<TreeView> {
    if let Ok(history) = load_history() {
        let response = history.text().unwrap();
        let json = json::parse(response.as_str()).unwrap();

        let history = find_weapon_in_history(json, name); // Rivens matching weapons
        if let Some(history) = history {
            let rivens = parse_rivens(history);

            let rivens_by_prices = rivens_by_prices(rivens);

            let buffs = get_buffs(rivens_by_prices);

            let mut prices_total = 0;
            for prices in buffs.values() {
                prices_total += prices.len() as i32;
            }

            //let mut price_counter = 0;
            let buffs_by_price = buffs_by_price(buffs, prices_total);

            //let price_list = ListBox::new();
            let price_tree = TreeView::new();
            let buff_column = TreeViewColumn::new();
            let price_column = TreeViewColumn::new();
            let popular_column = TreeViewColumn::new();
            let buff_renderer = CellRendererText::new();
            let price_renderer = CellRendererText::new();
            let popular_renderer = CellRendererText::new();

            buff_column.pack_start(&buff_renderer, true);
            buff_column.add_attribute(&buff_renderer, "text", 0);
            buff_column.set_title("Buff");
            price_column.pack_start(&price_renderer, true);
            price_column.add_attribute(&price_renderer, "text", 1);
            price_column.set_title("Average Price");
            popular_column.pack_start(&popular_renderer, true);
            popular_column.add_attribute(&popular_renderer, "text", 2);
            popular_column.set_title("Popularity");

            price_tree.insert_column(&buff_column, 0);
            price_tree.insert_column(&price_column, 1);
            price_tree.insert_column(&popular_column, 2);

            let model = ListStore::new(&[Type::String, Type::I32, Type::String]);
            model.set_sort_column_id(SortColumn::Index(1), gtk::SortType::Descending);

            price_tree.set_model(Some(&model));

            for (price, buff) in buffs_by_price {
                let buff_str = riven_buff_to_str(buff.0);
                //let price_string = price.to_string();
                let popularity = buff.1;
                let popularity_percent = format!("{:.2}%", 100.0 * popularity);

                let row = model.append();
                model.set_value(&row, 0, &buff_str.to_value());
                model.set_value(&row, 1, &price.to_value());
                model.set_value(&row, 2, &popularity_percent.to_value());
            }
            Some(price_tree)
        } else {
            None
        }
    } else {
        None
    }
}

fn rivens_by_prices(rivens: Vec<SemlarRiven>) -> BTreeMap<i32, Vec<RivenValue>> {
    let mut rivens_by_prices = BTreeMap::new();
    for riven in &rivens {
        let price = riven.get_price();
        let stats = riven.get_values();
        let _last_seen = riven.get_last_seen();
        //debug!("Lastseen {}", last_seen);

        rivens_by_prices.insert(price, stats);
    }
    rivens_by_prices
}

fn get_buffs(rivens_by_prices: BTreeMap<i32, Vec<RivenValue>>) -> HashMap<RivenBuff, Vec<i32>> {
    let mut buffs: HashMap<RivenBuff, Vec<i32>> = HashMap::new();
    for (key, value) in rivens_by_prices {
        let price = key;
        for buff in value {
            let buff_type = buff.get_buff();
            let curse = buff.get_curse();
            if curse {
                continue;
            } else {
                let prices = buffs.entry(buff_type).or_insert_with(Vec::new);
                prices.push(price);
            }
        }
    }
    buffs
}

fn buffs_by_price(
    buffs: HashMap<RivenBuff, Vec<i32>>,
    prices_total: i32,
) -> BTreeMap<i32, (RivenBuff, f32)> {
    let mut buffs_by_price = BTreeMap::new();
    for (riven_buff, prices) in buffs {
        let price_count = prices.len() as f32;
        let popularity = price_count / (prices_total as f32);
        debug!(
            "Buff: {}, prices: {}, total prices: {}, popularity: {}",
            riven_buff_to_str(riven_buff),
            price_count,
            prices_total,
            popularity
        );
        //debug!("{}", price_count);
        //price_counter += price_count as i32;
        //debug!("{}", price_counter);
        let mut price_sum = 0;
        let mut price_count = 0;
        for price in &prices {
            price_sum += price;
            price_count += 1;
            //price_sum = ((price_sum / price_count) as f32 * popularity) as i32;
        }
        let medium_price = ((price_sum / price_count) as f32 * popularity) as i32;
        //debug!("Medium: {}", medium_price);
        buffs_by_price.insert(medium_price as i32, (riven_buff, popularity));
    }
    buffs_by_price
}

fn get_riven_values(riven_info: &JsonValue) -> Vec<RivenValue> {
    let mut values = Vec::new();
    for buff in riven_info["buffs"].members() {
        let tag = buff["tag"].as_str().unwrap_or_default();
        let buff_str = tag
            .replace("|val|% ", "")
            .replace("|val| ", "")
            .replace("|val|s ", "");
        let riven_buff = riven_str_to_buff(buff_str.as_str());
        let curse = buff["curse"].as_bool().unwrap_or_default();
        let riven_value = RivenValue::new(riven_buff, curse);
        values.push(riven_value);
    }
    values
}
