use std::thread;

use gtk::prelude::*;
use gtk::{Box, Grid, Notebook, Orientation, PositionType, Stack, StackSidebar};

use crate::rigs::item::ItemOverview;
use crate::uplink::extractor::weapon::scrape_weapon_info;
use crate::view::modules::item_list::build_closeable_tab;
use crate::view::modules::riven::build_riven;
use crate::view::templates::weapon_base_stats::ui_template_base_stats_weapon;

use super::description::load_description;

pub fn on_primary_more_clicked(rig: &std::boxed::Box<dyn ItemOverview>, item_notebook: &Notebook) {
    // Benchmark
    let start = std::time::SystemTime::now();
    let base = on_base_load(rig);
    let cp_base = std::time::SystemTime::now();
    let riven = build_riven(rig);
    let end = std::time::SystemTime::now();
    debug!(
        "fn on_base_load took {:?}",
        cp_base.duration_since(start).unwrap()
    );
    debug!(
        "fn build_riven took {:?}",
        end.duration_since(cp_base).unwrap()
    );

    let page_container = Grid::new();

    let page_navigation = StackSidebar::new();

    let page = Stack::new();
    if let Some(description_page) = load_description(&rig) {
        page.add_titled(&description_page, "description", "Description");
    }
    page.add_titled(&base, "base", "Base");
    page.add_titled(&riven, "riven", "Riven");
    page.set_homogeneous(false);

    page_navigation.set_stack(&page);

    let tab_label = build_closeable_tab(rig.get_name(), item_notebook, &page_container);

    page_container.attach(&page_navigation, 0, 0, 1, 1);
    page_container.attach_next_to(&page, Some(&page_navigation), PositionType::Right, 1, 1);

    item_notebook.insert_page(&page_container, Some(&tab_label), None);
    item_notebook.show_all();
    item_notebook.set_current_page(None);
}

fn on_base_load(rig: &std::boxed::Box<dyn ItemOverview>) -> Box {
    let wiki_url = rig.get_url().to_string();

    let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
    thread::spawn(move || {
        let scraped_data = scrape_weapon_info(wiki_url.as_str());
        if let Err(e) = tx.send(scraped_data) {
            error!("{}", e.to_string());
        }
    });

    let container = Box::new(Orientation::Vertical, 5);
    let tmp_container = container.clone();
    rx.attach(None, move |scraped_data| {
        let data_list = ui_template_base_stats_weapon(scraped_data);
        tmp_container.add(&data_list);
        tmp_container.show_all();
        Continue(false)
    });

    container
}
