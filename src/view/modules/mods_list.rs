use gtk::prelude::*;
use gtk::{
    Box, Grid, Label, ListBox, Notebook, Orientation, PositionType, SelectionMode, Stack,
    StackSidebar,
};

use crate::rigs::item::ItemOverview;
use crate::view::modules::drops_list::load_drops;
use crate::view::modules::item_list::build_closeable_tab;
use crate::view::templates::value::build_value_ui;

use super::description::load_description;

pub fn on_mod_more_clicked(rig: &std::boxed::Box<dyn ItemOverview>, item_notebook: &Notebook) {
    let root = Box::new(Orientation::Vertical, 5);

    let source = rig.get_json();
    let name = rig.get_name();
    let polarity = source["polarity"].as_str().unwrap_or_default();
    let rarity = source["rarity"].as_str().unwrap_or_default();
    let base_drain = source["baseDrain"].as_i32().unwrap_or_default();
    let fusion_limit = source["fusionLimit"].as_i32().unwrap_or_default();
    let item_type = source["type"].as_str().unwrap_or_default();
    let mut stats = Vec::new();
    for level_stat in source["levelStats"].members() {
        let mut stats_str = String::new();
        for stat in level_stat["stats"].members() {
            stats_str.push_str(stat.as_str().unwrap_or_default());
        }
        stats.push(stats_str);
    }
    let tradable = source["tradable"].as_bool().unwrap_or_default();

    let label_name = build_value_ui("Name", name, None);
    let label_polarity = build_value_ui("Polarity", polarity, None);
    let label_rarity = build_value_ui("Rarity", rarity, None);
    let label_base_drain = build_value_ui("Base Drain", base_drain.to_string().as_str(), None);
    let label_fusion_limit =
        build_value_ui("Fusion Limit", fusion_limit.to_string().as_str(), None);
    let label_item_type = build_value_ui("Item Type", item_type, None);
    let label_stats = Label::new(Some("Level Stats"));
    let box_stats = ListBox::new();
    box_stats.set_selection_mode(SelectionMode::None);
    //let mut level_counter: u32 = 0;
    for (level_counter, stat) in stats.into_iter().enumerate() {
        let label_stat = build_value_ui(
            format!("Level {}", level_counter).as_str(),
            stat.as_str(),
            None,
        );
        box_stats.add(&label_stat);
        //level_counter += 1;
    }
    let label_tradable = build_value_ui("Tradable", tradable.to_string().as_str(), None);

    root.add(&label_name);
    root.add(&label_polarity);
    root.add(&label_rarity);
    root.add(&label_base_drain);
    root.add(&label_fusion_limit);
    root.add(&label_item_type);
    root.add(&label_stats);
    root.add(&box_stats);
    root.add(&label_tradable);
    root.set_vexpand(true);

    let alignment_box_h = Box::new(Orientation::Horizontal, 0);
    alignment_box_h.set_center_widget(Some(&root));

    let drop_table = load_drops(&rig);

    let page_container = Grid::new();

    let page_navigation = StackSidebar::new();

    let page = Stack::new();
    if let Some(description_page) = load_description(&rig) {
        page.add_titled(&description_page, "description", "Description");
    }
    page.add_titled(&alignment_box_h, "base", "Base");
    page.add_titled(&drop_table, "drops", "Drops");
    page.set_homogeneous(false);

    page_navigation.set_stack(&page);

    let tab_label = build_closeable_tab(rig.get_name(), item_notebook, &page_container);

    page_container.attach(&page_navigation, 0, 0, 1, 1);
    page_container.attach_next_to(&page, Some(&page_navigation), PositionType::Right, 1, 1);

    item_notebook.insert_page(&page_container, Some(&tab_label), None);
    item_notebook.show_all();
    item_notebook.set_current_page(None);
}
