use gtk::prelude::*;
use gtk::{Box, Label};
use pango::{AttrList, Attribute};

use crate::rigs::item::ItemOverview;

pub fn load_description(rig: &std::boxed::Box<dyn ItemOverview>) -> Option<gtk::Box> {
    let desc = rig.get_json()["description"].as_str().unwrap_or_default();
    if !desc.is_empty() {
        let desc_label = Label::new(Some(desc));
        desc_label.set_line_wrap(true);
        if let Some(scale) = Attribute::new_scale(1.5) {
            let attr_list = AttrList::new();
            attr_list.insert(scale);
            desc_label.set_attributes(Some(&attr_list));
        }

        let desc_alignment = Box::new(gtk::Orientation::Horizontal, 0);
        desc_alignment.set_center_widget(Some(&desc_label));
        Some(desc_alignment)
    } else {
        None
    }
}
