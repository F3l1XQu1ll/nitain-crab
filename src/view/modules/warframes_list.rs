use std::thread;

use gtk::prelude::*;
use gtk::{Box, Grid, Notebook, Orientation, PositionType, Stack, StackSidebar};

use crate::view::modules::item_list::build_closeable_tab;
use crate::view::templates::acquisition::template_parts;
use crate::view::templates::warframe_abilities::ui_template_warframe_abilities;
use crate::view::templates::warframe_base_stats::{
    template_base_stats_warframe, ui_template_base_stats_warframe,
};
use crate::{rigs::item::ItemOverview, uplink::fetch};
use crate::{
    uplink::extractor::warframe::{scrape_warframe_abilities, scrape_warframe_base},
    view,
};

use super::description::load_description;

pub fn on_warframe_more_clicked(rig: &std::boxed::Box<dyn ItemOverview>, item_notebook: &Notebook) {
    // Benchmark
    let start = std::time::SystemTime::now();
    let base = on_warframe_base_load(rig);
    let cp_1 = std::time::SystemTime::now();
    let abilities = on_warframe_template_abilities(rig);
    let cp_2 = std::time::SystemTime::now();
    let acquisition = template_parts(rig);
    let end = std::time::SystemTime::now();
    let dur_1 = cp_1.duration_since(start).unwrap();
    let dur_2 = cp_2.duration_since(cp_1).unwrap();
    let dur_3 = end.duration_since(cp_2).unwrap();
    debug!("fn on_warframe_base_load took {:?}", dur_1);
    debug!("fn on_warframe_template_abilities took {:?}", dur_2);
    debug!("fn template_parts took {:?}", dur_3);

    let page_container = Grid::new();

    let page_navigation = StackSidebar::new();

    let page = Stack::new();
    if let Some(description_page) = load_description(rig) {
        page.add_titled(&description_page, "description", "Description");
    }
    page.add_titled(&base, "base", "Base");
    page.add_titled(&abilities, "abilities", "Abilities");
    page.add_titled(&acquisition, "acquisition", "Acquisition");
    //page.add_titled(&load_builds(rig.get_name()), "builds", "Builds");
    page.set_homogeneous(false);

    page_navigation.set_stack(&page);

    let tab_label = build_closeable_tab(rig.get_name(), item_notebook, &page_container);

    page_container.attach(&page_navigation, 0, 0, 1, 1);
    page_container.attach_next_to(&page, Some(&page_navigation), PositionType::Right, 1, 1);

    item_notebook.insert_page(&page_container, Some(&tab_label), None);
    item_notebook.show_all();
    item_notebook.set_current_page(None);
}

pub fn on_warframe_template_abilities(rig: &std::boxed::Box<dyn ItemOverview>) -> Box {
    let abilities_container = Box::new(Orientation::Vertical, 5);
    //let data = uplink::get_nexus_data(api_url_tmp.as_str());
    let data = rig.get_json();

    let abilities = data["abilities"].members();
    let mut ability_names = Vec::new();
    for ability in abilities {
        ability_names.push(String::from(ability["name"].as_str().unwrap()));
    }

    let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
    thread::spawn(move || {
        let scraped_abilites = scrape_warframe_abilities(ability_names);
        if let Err(e) = tx.send(scraped_abilites) {
            error!("{}", e.to_string());
        }
    });

    let tmp_container = abilities_container.clone();
    rx.attach(None, move |scraped_abilites| {
        let ui = ui_template_warframe_abilities(scraped_abilites);
        tmp_container.add(&ui);
        tmp_container.show_all();
        Continue(false)
    });

    abilities_container
}

pub(crate) fn on_warframe_base_load(rig: &std::boxed::Box<dyn ItemOverview>) -> Box {
    let wiki_url = rig.get_url().to_string();
    debug!("{}", wiki_url);
    let name = rig.get_name().to_string();

    let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
    thread::spawn(move || {
        let scraped_data = scrape_warframe_base(wiki_url.as_str(), name.ends_with("Prime"));
        let templated_data = template_base_stats_warframe(scraped_data);
        if let Err(e) = tx.send(templated_data) {
            error!("{}", e.to_string());
        }
    });

    let container = Box::new(Orientation::Vertical, 5);
    let tmp_container = container.clone();
    rx.attach(None, move |templated_data| {
        let data_list = ui_template_base_stats_warframe(templated_data);
        tmp_container.add(&data_list);
        tmp_container.show_all();
        Continue(false)
    });

    container
}

#[deprecated()]
#[allow(dead_code)]
#[allow(deprecated)]
pub fn load_builds(name: &str) -> gtk::ScrolledWindow {
    let scroll = gtk::ScrolledWindowBuilder::new().build();
    let link = fetch::OverframeLink {};
    if let Some(response) = link.find_builds(name) {
        //debug!("{}", response);
        let builds_json = json::parse(&response).unwrap_or_else(|_| json::object! {});
        let mut builds_collection = Vec::new();

        for build in builds_json["results"].members() {
            debug!("{}", build.to_string());
            let item_build = view::modules::builds::ItemBuild::from_json(build.clone());
            builds_collection.push(item_build);
        }

        let builds_list = view::modules::builds::BuildsList {
            builds: builds_collection,
        };

        scroll.add(&builds_list.build());
    }

    scroll
}
