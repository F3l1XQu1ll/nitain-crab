use crate::rigs::item_mod_conf::ModPolarity;
use gtk::{
    BinExt, ButtonExt, ContainerExt, GridExt, RevealerExt, WidgetExt,
};
//use webkit2gtk::WebViewExt;

#[allow(dead_code)]
pub struct BuildAuthor {
    id: u32,
    username: String,
    url: String,
}

#[allow(dead_code)]
pub struct OverframeItemData {
    id: u32,
    texture: String,
}

pub struct ItemBuild {
    id: u32,
    //created: String,
    updated: String,
    score: u32,
    //url: String,
    author: BuildAuthor,
    formas: u32,
    //guide_wordcount: u32,
    //item_data: OverframeItemData,
    title: String,
    //description: String,
    //build_string: String,
}
pub struct BuildsList {
    pub(crate) builds: Vec<ItemBuild>,
}

impl BuildsList {
    pub fn build(&self) -> gtk::ListBox {
        let builds_list = gtk::ListBox::new();
        let mut build_vec = Vec::new();
        for build in &self.builds {
            build_vec.push((build.get_score(), build));
        }

        build_vec.sort_by(|a, b| b.0.cmp(&a.0)); // Reverse sort

        for entry in build_vec {
            let build_entry_grid = entry.1.build();
            let row = gtk::ListBoxRow::new();
            build_entry_grid.set_property_margin(5);
            //build_entry_grid.set_row_spacing(2);
            //build_entry_grid.set_column_spacing(2);
            row.add(&build_entry_grid);

            builds_list.add(&row);
        }

        builds_list
    }
}

impl ItemBuild {
    fn build(&self) -> gtk::Box {
        let label_title = gtk::Label::new(Some(self.title.as_str()));
        let updated_str = format!("Last updated: {}", self.updated);
        let label_updated = gtk::Label::new(Some(updated_str.as_str()));
        let label_score = gtk::Label::new(Some(&self.score.to_string().as_str()));
        let author_str = format!("By {}", &self.author.get_username());
        let label_autor = gtk::Label::new(Some(author_str.as_str()));
        let formas_str = format!("Formas: {}", &self.formas);
        let label_formas = gtk::Label::new(Some(formas_str.as_str()));
        let button_expand_build = gtk::Button::new();
        let revealer_build = gtk::Revealer::new();

        let id = self.id;
        button_expand_build.connect_clicked(glib::clone!(@weak revealer_build => move |_| {
            //revealer_build.set_reveal_child(!revealer_build.get_child_revealed());
            if revealer_build.get_child_revealed(){
                revealer_build.set_reveal_child(false);
                //button.set_label("Show");
            }else{
                if revealer_build.get_child().is_none(){
                    revealer_build.add(&Self::make_grid(id));
                    revealer_build.show_all();
                }
                revealer_build.set_reveal_child(true);
                //button.set_label("Hide");
            }
            //revealer_build.show_all();
            //revealer_build.add(&self.make_grid());
        }));

        let grid_build = gtk::Grid::new();
        grid_build.attach(&label_title, 0, 0, 3, 1);
        grid_build.attach_next_to(
            &label_autor,
            Some(&label_title),
            gtk::PositionType::Bottom,
            1,
            1,
        );
        grid_build.attach_next_to(
            &label_updated,
            Some(&label_autor),
            gtk::PositionType::Right,
            1,
            1,
        );
        grid_build.attach_next_to(
            &label_formas,
            Some(&label_updated),
            gtk::PositionType::Right,
            1,
            1,
        );
        grid_build.attach_next_to(
            &label_score,
            Some(&label_title),
            gtk::PositionType::Right,
            1,
            2,
        );
        /*grid_build.attach_next_to(
            &revealer_build,
            Some(&label_autor),
            gtk::PositionType::Bottom,
            3,
            1,
        );*/
        //grid_build.attach(&button_expand_build, 0, 3, 3, 1);

        grid_build.set_column_spacing(2);
        grid_build.set_row_spacing(2);

        button_expand_build.add(&grid_build);

        let build_box = gtk::Box::new(gtk::Orientation::Vertical, 2);
        build_box.add(&button_expand_build);
        build_box.add(&revealer_build);

        build_box
    }

    pub fn from_json(source: json::JsonValue) -> Self {
        debug!("{}", source.to_string());
        Self {
            id: source["id"].as_u32().unwrap_or_default(),
            //created: source["created"].as_str().unwrap_or_default().to_string(),
            updated: source["updated"].as_str().unwrap_or_default().to_string(),
            score: source["score"].as_u32().unwrap_or_default(),
            //url: source["url"].as_str().unwrap_or_default().to_string(),
            author: BuildAuthor {
                id: source["author"]["id"].as_u32().unwrap_or_default(),
                username: source["author"]["username"]
                    .as_str()
                    .unwrap_or_default()
                    .to_string(),
                url: source["author"]["url"]
                    .as_str()
                    .unwrap_or_default()
                    .to_string(),
            },
            formas: source["formas"].as_u32().unwrap_or_default(),
            //guide_wordcount: source["guide_wordcount"].as_u32().unwrap_or_default(),
            /*item_data: OverframeItemData {
                id: source["item_data"]["id"].as_u32().unwrap_or_default(),
                texture: source["item_data"]["texture"]
                    .as_str()
                    .unwrap_or_default()
                    .to_string(),
            },*/
            title: source["title"].as_str().unwrap_or_default().to_string(),
            /*description: source["description"]
                .as_str()
                .unwrap_or_default()
                .to_string(),*/
            /*build_string: source["buildstring"]
                .as_str()
                .unwrap_or_default()
                .to_string(),*/
        }
    }

    pub fn get_score(&self) -> u32 {
        self.score
    }

    fn make_grid(id: u32) -> gtk::Grid {
        let url = format!("https://overframe.gg/api/v1/builds/{}", id);
        let grid = gtk::Grid::new();
        if let Ok(response) = reqwest::blocking::get(url.as_str()) {
            if let Ok(response) = response.text() {
                if let Ok(json_str) = json::parse(&response) {
                    let build_str = json_str["buildstring"].as_str().unwrap_or_default();
                    let decoded_build_str =
                        String::from_utf8(base64::decode(build_str).unwrap_or_default())
                            .unwrap_or_default();
                    let build_json = json::parse(&decoded_build_str);
                    if let Ok(build_json) = build_json {
                        for member in build_json[4].members().take(8).enumerate() {
                            let mod_box = Self::make_mod_box(member);
                            let mod_button = gtk::Button::new();
                            mod_button.add(&mod_box);
                            if member.0 < 4 {
                                //debug!("{}", 4 - (member.0 as i32));
                                grid.attach(&mod_button, 4 - (member.0 as i32), 1, 1, 1);
                            } else {
                                //debug!("{}", 4 - (member.0 as i32) + 4);
                                grid.attach(&mod_button, 4 - (member.0 as i32) + 4, 0, 1, 1);
                            }
                        }
                        for member in build_json[4].members().skip(8).take(2).enumerate() {
                            let mod_box = Self::make_mod_box(member);
                            let mod_button = gtk::Button::new();
                            mod_button.add(&mod_box);
                            grid.attach(&mod_button, member.0 as i32 + 2, -1, 1, 1);
                        }
                        for member in build_json[4].members().skip(10).take(2).enumerate() {
                            let mod_box = Self::make_mod_box(member);
                            let mod_button = gtk::Button::new();
                            mod_button.add(&mod_box);
                            grid.attach(&mod_button, member.0 as i32 + 2, 2, 1, 1);
                        }
                    }
                }
            }
        }
        grid
    }

    fn make_mod_box(data: (usize, &json::JsonValue)) -> gtk::Box {
        let id = data.1[0].as_u32().unwrap_or_default();
        let rank = data.1[1].as_u32().unwrap_or_default();
        let polarity = data.1[2].as_u32().unwrap_or_default();
        let mod_box = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        if let Ok(mod_info) =
            reqwest::blocking::get(format!("https://overframe.gg/api/v1/items/{}", id).as_str())
        {
            let mod_info = mod_info.text().unwrap_or_default();
            let mod_json = json::parse(&mod_info).unwrap();
            let mod_name = mod_json["name"].as_str().unwrap_or_default();

            let mod_title_label = gtk::Label::new(Some(mod_name));
            let mod_rank_label = gtk::Label::new(Some(&rank.to_string()));
            /*let mod_polarity_label =
            gtk::Label::new(Some(Self::polarity_to_sign(Self::polarity_by_id(polarity))));*/
            mod_box.add(&mod_title_label);
            mod_box.add(&mod_rank_label);
            mod_box.add(&Self::image_from_polarity(Self::polarity_by_id(polarity)));
        }

        mod_box
    }

    fn polarity_by_id(id: u32) -> ModPolarity {
        match id {
            0 => ModPolarity::None,
            1 => ModPolarity::V,
            2 => ModPolarity::D,
            3 => ModPolarity::Bar,
            4 => ModPolarity::Scratch,
            5 => ModPolarity::Y,
            6 => ModPolarity::R,
            7 => ModPolarity::O,
            8 => ModPolarity::U,
            _ => ModPolarity::None,
        }
    }

    #[allow(dead_code)]
    fn polarity_to_sign(polartiy: ModPolarity) -> &'static str {
        match polartiy {
            ModPolarity::V => "V",
            ModPolarity::D => "D",
            ModPolarity::Bar => "–",
            ModPolarity::Scratch => "=",
            ModPolarity::R => "r",
            ModPolarity::Y => "y",
            ModPolarity::U => "U",
            ModPolarity::O => "O",
            ModPolarity::None => "",
        }
    }

    fn image_from_polarity(polartiy: ModPolarity) -> gtk::Image {
        let res_str;
        match polartiy {
            ModPolarity::V => res_str = "madurai_polarity",
            ModPolarity::D => res_str = "vazarin_polarity",
            ModPolarity::Bar => res_str = "naramon_polarity",
            ModPolarity::Scratch => res_str = "zenurik_polarity",
            ModPolarity::R => res_str = "unairu_polarity",
            ModPolarity::Y => res_str = "penjaga_polarity",
            ModPolarity::U => res_str = "umbra_polarity",
            ModPolarity::O => res_str = "aura_polarity",
            ModPolarity::None => res_str = "",
        }

        let res_root = "/com/archyt/Nitain_Crab/icons";
        gtk::Image::from_resource(format!("{}/{}", res_root, res_str).as_str())
    }
}

impl BuildAuthor {
    fn get_username(&self) -> &String {
        &self.username
    }
}

/*pub fn build_overframe_browsing_context() -> gtk::Overlay {
    let web_view = webkit2gtk::WebView::new();
    web_view.load_uri("https://overframe.gg/");

    let button_reload = Button::new();
    let button_home = Button::new();
    let button_back = Button::new();
    let button_forth = Button::new();

    let image_reload = Image::from_icon_name(Some("view-refresh-symbolic"), gtk::IconSize::Button);
    let image_home = Image::from_icon_name(Some("go-home-symbolic"), gtk::IconSize::Button);
    let image_back = Image::from_icon_name(Some("go-previous-symbolic"), gtk::IconSize::Button);
    let image_forth = Image::from_icon_name(Some("go-next-symbolic"), gtk::IconSize::Button);

    button_reload.add(&image_reload);
    button_reload.connect_clicked(clone!(@weak web_view =>  move |_| {
        web_view.reload();
    }));

    button_home.add(&image_home);
    button_home.connect_clicked(clone!(@weak web_view => move |_| {
        web_view.load_uri("https://overframe.gg/");
    }));

    button_back.add(&image_back);
    button_back.connect_clicked(clone!(@weak web_view => move |_| {
        if web_view.can_go_back() {
            web_view.go_back();
        }
    }));

    button_forth.add(&image_forth);
    button_forth.connect_clicked(clone!(@weak web_view => move |_| {
        if web_view.can_go_forward() {
            web_view.go_forward();
        }
    }));

    let scrolled_window = gtk::ScrolledWindowBuilder::new().build();
    scrolled_window.add(&web_view);

    let overlay = gtk::Overlay::new();
    let buttons_grid = gtk::Grid::new();

    buttons_grid.set_column_spacing(5);

    overlay.add(&scrolled_window);
    overlay.add_overlay(&buttons_grid);
    overlay.set_overlay_pass_through(&buttons_grid, true);

    buttons_grid.attach(&button_back, 0, 0, 1, 1);
    buttons_grid.attach(&button_home, 1, 0, 1, 1);
    buttons_grid.attach(&button_reload, 2, 0, 1, 1);
    buttons_grid.attach(&button_forth, 3, 0, 1, 1);
    buttons_grid.set_hexpand(false);
    buttons_grid.set_vexpand(false);

    overlay
}*/
