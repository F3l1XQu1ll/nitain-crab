use gtk::prelude::*;
use gtk::{Grid, Notebook, PositionType, Stack, StackSidebar};

use crate::rigs::item::ItemOverview;
use crate::view::modules::item_list::build_closeable_tab;
use crate::view::modules::warframes_list::on_warframe_base_load;

use super::{description::load_description, warframes_list::on_warframe_template_abilities};

pub fn on_archwing_more_clicked(rig: &std::boxed::Box<dyn ItemOverview>, item_notebook: &Notebook) {
    let base = on_warframe_base_load(rig);
    let abilities = on_warframe_template_abilities(rig);

    let page_container = Grid::new();

    let page_navigation = StackSidebar::new();

    let page = Stack::new();
    if let Some(description_page) = load_description(&rig) {
        page.add_titled(&description_page, "description", "Description");
    }
    page.add_titled(&base, "base", "Base");
    page.add_titled(&abilities, "abilities", "Abilities");
    page.set_homogeneous(false);

    page_navigation.set_stack(&page);

    let tab_label = build_closeable_tab(rig.get_name(), item_notebook, &page_container);

    page_container.attach(&page_navigation, 0, 0, 1, 1);
    page_container.attach_next_to(&page, Some(&page_navigation), PositionType::Right, 1, 1);

    item_notebook.insert_page(&page_container, Some(&tab_label), None);
    item_notebook.show_all();
    item_notebook.set_current_page(None);
}
