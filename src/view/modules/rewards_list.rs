use std::{collections::HashMap, thread};

use glib::{Type, PRIORITY_DEFAULT};
use gtk::{prelude::*, CellRendererText, Label, TreeViewColumn};
use gtk::{Box, ListStore, ScrolledWindowBuilder, SortColumn, SortType, TreeView};
use gtk::{Orientation, ScrolledWindow};

use crate::{
    rigs::item::ItemOverview,
    uplink::extractor::relic_drops::load_relic_drops,
    utils::{scale_label, ItemClass},
};

pub fn load_rewards(rig: &std::boxed::Box<dyn ItemOverview>) -> ScrolledWindow {
    let root = Box::new(Orientation::Vertical, 5);

    if let ItemClass::Relics = rig.get_class() {
        let info_label = Label::new(Some("This relic may reward:"));
        scale_label(&info_label, 1.5);

        root.add(&info_label);

        let (tx, rx) = glib::MainContext::channel(PRIORITY_DEFAULT);
        let rig_name = rig.get_name().to_string();
        thread::spawn(move || {
            let drops_list = load_relic_drops(rig_name.as_str());
            if let Err(e) = tx.send(drops_list) {
                error!("{}", e.to_string());
            }
        });

        let tmp_root = root.clone();
        rx.attach(None, move |drops_list| {
            if let Some(drops_list) = drops_list {
                let table = build_drops_list_from_map(drops_list);
                tmp_root.add(&table);
            }
            tmp_root.show_all();
            Continue(false)
        });
    }

    root.set_vexpand(true);

    let alignment_box_h = Box::new(Orientation::Horizontal, 0);
    alignment_box_h.set_center_widget(Some(&root));

    let scrolled_window = ScrolledWindowBuilder::new().build();
    scrolled_window.add(&alignment_box_h);
    scrolled_window
}

fn build_drops_list_from_map(map: HashMap<String, String>) -> TreeView {
    let model = ListStore::new(&[Type::String, Type::String, Type::F32]);
    let list = TreeView::with_model(&model);
    let item_column = TreeViewColumn::new();
    let rarity_column = TreeViewColumn::new();
    let sort_column = TreeViewColumn::new();
    let item_renderer = CellRendererText::new();
    let rarity_renderer = CellRendererText::new();

    model.set_sort_column_id(SortColumn::Index(2), SortType::Descending);

    sort_column.set_visible(false);

    item_column.set_title("Item");
    rarity_column.set_title("Drop Chance");

    list.insert_column(&item_column, 0);
    list.insert_column(&rarity_column, 1);
    list.insert_column(&sort_column, 2);

    item_column.pack_start(&item_renderer, true);
    rarity_column.pack_start(&rarity_renderer, true);

    item_column.add_attribute(&item_renderer, "text", 0);
    rarity_column.add_attribute(&rarity_renderer, "text", 1);

    for item in map {
        let row = model.append();
        model.set_value(&row, 0, &item.0.to_value());
        model.set_value(&row, 1, &item.1.to_value());
        let percentage = item.1.split(' ').collect::<Vec<&str>>()[1];
        let percentage_fixed = percentage
            .replace("%", "")
            .replace("(", "")
            .replace(")", "");
        let chance = percentage_fixed.parse::<f32>().unwrap_or_default();
        debug!("{}", chance);
        model.set_value(&row, 2, &chance.to_value());
    }

    list
}
