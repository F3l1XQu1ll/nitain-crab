use gtk::prelude::*;
use gtk::{Box, Grid, Notebook, Orientation, PositionType, Stack, StackSidebar};

use crate::rigs::item::ItemOverview;
use crate::view::modules::drops_list::load_drops;
use crate::view::modules::item_list::build_closeable_tab;
use crate::view::templates::acquisition::template_parts;
use crate::view::templates::value::build_value_ui;

use super::description::load_description;

pub fn on_resource_more_clicked(rig: &std::boxed::Box<dyn ItemOverview>, item_notebook: &Notebook) {
    let root = Box::new(Orientation::Vertical, 5);

    let source = rig.get_json();
    let name = rig.get_name();
    let desc = source["description"].as_str().unwrap_or_default();
    let tradable = source["tradable"].as_bool().unwrap_or_default();

    let label_name = build_value_ui("Name", name, None);
    let label_desc = build_value_ui("Description", desc, None);
    let label_tradable = build_value_ui("Tradable", tradable.to_string().as_str(), None);

    root.add(&label_name);
    root.add(&label_desc);
    root.add(&label_tradable);
    root.set_vexpand(true);

    let alignment_box_h = Box::new(Orientation::Horizontal, 0);
    alignment_box_h.set_center_widget(Some(&root));

    let drop_table = load_drops(&rig);

    let page_container = Grid::new();

    let page_navigation = StackSidebar::new();

    let page = Stack::new();
    if let Some(description_page) = load_description(&rig) {
        page.add_titled(&description_page, "description", "Description");
    }
    page.add_titled(&alignment_box_h, "base", "Base");
    if source.has_key("components") {
        let parts = template_parts(rig);
        page.add_titled(&parts, "parts", "Components");
    }
    page.add_titled(&drop_table, "drops", "Drops");
    page.set_homogeneous(false);

    page_navigation.set_stack(&page);

    let tab_label = build_closeable_tab(rig.get_name(), item_notebook, &page_container);

    page_container.attach(&page_navigation, 0, 0, 1, 1);
    page_container.attach_next_to(&page, Some(&page_navigation), PositionType::Right, 1, 1);

    item_notebook.insert_page(&page_container, Some(&tab_label), None);
    item_notebook.show_all();
    item_notebook.set_current_page(None);
}
