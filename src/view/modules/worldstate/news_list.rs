use chrono::{DateTime, Local};
use glib::{Continue, Sender, PRIORITY_DEFAULT};
use gtk::{
    Button, ButtonExt, ContainerExt, Image, LinkButton, ListBox, Overlay, OverlayExt, Spinner,
    SpinnerExt, WidgetExt,
};
use json::JsonValue;
use libhandy::{ActionRowExt, PreferencesGroup, PreferencesPage, PreferencesRowExt};
use std::{cell::RefCell, rc::Rc, thread};

use crate::{
    message::NitainMessage,
    uplink::{create_client, images::load_image},
    view::image_foundry::image_from_stream,
};

pub struct NewsList {
    link: Sender<NitainMessage>,
}

struct NewsEntry {
    date: String,
    image: String,
    prime: bool,
    stream: bool,
    update: bool,
    link: String,
    id: String,
    message: String,
    important: bool,
}

impl NewsList {
    pub fn new(link: Sender<NitainMessage>) -> Self {
        Self { link }
    }

    pub fn load(&self) -> libhandy::PreferencesPage {
        let layout = PreferencesPage::new();
        let group = PreferencesGroup::new();

        let back_group = PreferencesGroup::new();
        let back_button = Button::with_label("Back to first page");
        let tmp_link = self.link.clone();
        back_button.connect_clicked(move |_| {
            if let Err(e) = tmp_link.send(NitainMessage::NavHome) {
                warn!("{:?}", e.to_string())
            }
        });
        back_group.add(&back_button);

        let tmp_layout = group.clone();
        let loading_indicator = gtk::Spinner::new();
        tmp_layout.add(&loading_indicator);
        loading_indicator.start();
        let known_ids = Vec::new();
        let known_ids_wrpper = Rc::new(RefCell::new(known_ids));

        // Do Important stuff before timeout, otherwise the delay will be due BEFORE the code
        Self::background_fetch(&tmp_layout, known_ids_wrpper.clone(), &loading_indicator);
        tmp_layout.show_all();

        glib::timeout_add_seconds_local(60, move || {
            // … then repeat it ._.
            Self::background_fetch(&tmp_layout, known_ids_wrpper.clone(), &loading_indicator);
            tmp_layout.show_all();
            Continue(true)
        });
        layout.add(&back_group);
        layout.add(&group);
        layout
    }

    fn background_fetch(
        group: &PreferencesGroup,
        known_ids: Rc<RefCell<Vec<std::string::String>>>,
        loading_indicator: &Spinner,
    ) {
        //let page = libhandy::PreferencesPage::new();

        let (tx, rx) = glib::MainContext::channel(PRIORITY_DEFAULT);
        thread::spawn(move || {
            if let Err(e) = tx.send(Self::do_load()) {
                warn!("{:?}", e.to_string())
            }
        });

        let tmp_layout = group.clone();
        //let mut known_ids = Vec::new();
        //let mut tmp_ids = known_ids;
        let tmp_indicator = loading_indicator.clone();
        rx.attach(None, move |mut entries| {
            entries.reverse();
            let list = ListBox::new();
            for entry in entries {
                if !known_ids.borrow().contains(&entry.id) {
                    list.add(&entry.build());
                    let mut known_ids_copy = known_ids.take();
                    known_ids_copy.push(entry.id);
                    *known_ids.borrow_mut() = known_ids_copy;
                }
            }

            if !list.get_children().is_empty() {
                tmp_layout.add(&list);
                tmp_layout.show_all();
            }

            tmp_layout.remove(&tmp_indicator);
            Continue(false)
        });
        //page.add(&group);
        //page

        //debug!("{:?}", known_ids);
        //known_ids
    }

    fn do_load() -> Vec<NewsEntry> {
        let response = block_load!("https://api.warframestat.us/pc/news");
        match response {
            Ok(res) => {
                let json = json::parse(res.as_str());
                match json {
                    Ok(json_val) => Self::json_to_entries(json_val),
                    Err(e) => {
                        warn!("{:?}", e.to_string());
                        Self::do_load()
                    }
                }
            }
            Err(e) => {
                warn!("{:?}", e.to_string());
                Self::do_load()
            }
        }
    }

    fn json_to_entries(json_val: JsonValue) -> Vec<NewsEntry> {
        let mut entries = Vec::new();
        for member in json_val.members() {
            entries.push(NewsEntry {
                date: member["date"].as_str().unwrap_or_default().to_string(),
                image: member["imageLink"].as_str().unwrap_or_default().to_string(),
                prime: member["primeAccess"].as_bool().unwrap_or_default(),
                stream: member["stream"].as_bool().unwrap_or_default(),
                update: member["update"].as_bool().unwrap_or_default(),
                link: member["link"].as_str().unwrap_or_default().to_string(),
                id: member["id"].as_str().unwrap_or_default().to_string(),
                message: member["message"].as_str().unwrap_or_default().to_string(),
                important: member["important"].as_bool().unwrap_or_default(),
            });
        }
        entries
    }
}

impl NewsEntry {
    fn build(&self) -> libhandy::ActionRow {
        let row = libhandy::ActionRow::new();
        row.set_title(Some(self.message.as_str()));

        let tmp_date = self.date.clone();
        let tmp_row = row.clone();
        glib::timeout_add_seconds_local(1, move || {
            if tmp_row.in_destruction() {
                Continue(false)
            } else {
                let now = Local::now();
                let then = DateTime::parse_from_rfc3339(&tmp_date.as_str());
                if let Ok(then) = then {
                    let duration = now.signed_duration_since(then);
                    let weeks = duration.num_weeks();
                    let days = duration.num_days() % 7;
                    let hours = duration.num_hours() % 24;
                    let minutes = duration.num_minutes() % 60;
                    //let seconds = duration.num_seconds() % 60;

                    let mut duration_str = String::new();
                    Self::add_to_duration(&mut duration_str, weeks, "Week");
                    Self::add_to_duration(&mut duration_str, days, "Day");
                    Self::add_to_duration(&mut duration_str, hours, "Hour");
                    Self::add_to_duration(&mut duration_str, minutes, "Minute");
                    //Self::add_to_duration(&mut duration_str, seconds, "Second");

                    //debug!("Row exists? {}", tmp_row.get_realized());
                    if tmp_row.get_realized() {
                        tmp_row.set_subtitle(Some(format!("{} ago", duration_str).as_str()));
                    }
                }

                Continue(true)
            }
        });

        let tag_container = Overlay::new();

        let link_button = LinkButton::new(self.link.as_str());
        link_button.set_always_show_image(true);
        link_button.set_label("");

        if self.important {
            let image = image_by_name!("emblem-important-symbolic");
            tag_container.add_overlay(&image);
            tag_container.set_overlay_pass_through(&image, true);
        } else if self.update {
            let image = image_by_name!("software-update-available-symbolic");
            tag_container.add_overlay(&image);
            tag_container.set_overlay_pass_through(&image, true);
        } else if self.stream {
            let image = image_by_name!("tv-symbolic");
            tag_container.add_overlay(&image);
            tag_container.set_overlay_pass_through(&image, true);
        } else if self.prime {
            let image = image_by_name!("starred-symbolic");
            tag_container.add_overlay(&image);
            tag_container.set_overlay_pass_through(&image, true);
        }

        link_button.set_image(Some(&Image::from_icon_name(
            Some("web-browser-symbolic"),
            gtk::IconSize::Dialog,
        )));

        let (tx, rx) = glib::MainContext::channel(PRIORITY_DEFAULT);
        let image = self.image.clone();
        thread::spawn(move || {
            if let Err(e) = tx.send(load_image(image.as_str(), create_client())) {
                warn!("{:?}", e.to_string())
            }
        });

        let tmp_link_button = link_button.clone();
        rx.attach(None, move |bytes| {
            if let Ok(image) = image_from_stream(bytes, 200, 200) {
                tmp_link_button.set_image(Some(&image));
                //tmp_row.reorder_child(&image, 0);
                tmp_link_button.show_all();
            }
            Continue(false)
        });

        tag_container.add(&link_button);

        row.add(&tag_container);
        //row.add(&link_button);

        row
    }

    fn add_to_duration(duration_str: &mut String, unit: i64, unit_name: &str) {
        if unit > 0 {
            duration_str.push_str(format!("{} {}", unit, unit_name).as_str());
            if unit > 1 {
                duration_str.push('s');
            }
            duration_str.push_str(", ");
        }
    }
}
