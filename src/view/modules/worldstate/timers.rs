use chrono::{DateTime, Duration, Local, NaiveDate, NaiveDateTime, NaiveTime, Utc};
use glib::{Continue, Sender, PRIORITY_DEFAULT};
use gtk::{
    Box, BoxExt, Button, ButtonExt, ContainerExt, Label, LabelExt, Orientation, Popover, Spinner,
    SpinnerExt, WidgetExt,
};
use json::JsonValue;
use libhandy::{PreferencesGroup, PreferencesPage};
use pango::AttrList;
use std::{ops::Sub, thread};

use crate::message::NitainMessage;

pub struct WorldTimer {
    //activation: DateTime<FixedOffset>,
    expiry: DateTime<Utc>,
    state: String,
}

pub struct TimersUi {
    link: Sender<NitainMessage>,
}

impl TimersUi {
    pub fn new(link: Sender<NitainMessage>) -> Self {
        Self { link }
    }

    pub fn load(&self) -> libhandy::PreferencesPage {
        let layout = PreferencesPage::new();
        let group = PreferencesGroup::new();

        let back_group = PreferencesGroup::new();
        let back_button = Button::with_label("Back to first page");
        let tmp_link = self.link.clone();
        back_button.connect_clicked(move |_| {
            if let Err(e) = tmp_link.send(NitainMessage::NavHome) {
                warn!("{:?}", e.to_string())
            }
        });
        back_group.add(&back_button);

        let tmp_layout = group.clone();
        let loading_indicator = gtk::Spinner::new();
        tmp_layout.add(&loading_indicator);
        loading_indicator.start();

        // Do Important stuff before timeout, otherwise the delay will be due BEFORE the code
        Self::background_fetch(&tmp_layout, &loading_indicator);
        tmp_layout.show_all();

        glib::timeout_add_seconds_local(20, move || {
            // … then repeat it ._.
            Self::background_fetch(&tmp_layout, &loading_indicator);
            tmp_layout.show_all();
            Continue(true)
        });

        layout.add(&back_group);
        layout.add(&group);
        layout
    }

    fn background_fetch(group: &PreferencesGroup, loading_indicator: &Spinner) {
        //let page = libhandy::PreferencesPage::new();

        let (tx, rx) = glib::MainContext::channel(PRIORITY_DEFAULT);
        thread::spawn(move || {
            if let Err(e) = tx.send([
                Self::do_load("cetusCycle"),
                Self::do_load("vallisCycle"),
                Self::do_load("cambionCycle"),
                Self::do_load("earthCycle"),
            ]) {
                warn!("{:?}", e.to_string())
            }
        });

        let tmp_layout = group.clone();
        //let mut known_ids = Vec::new();
        //let mut tmp_ids = known_ids;
        let tmp_indicator = loading_indicator.clone();
        rx.attach(None, move |values| {
            for child in tmp_layout.get_children() {
                tmp_layout.remove(&child);
            }
            let timers_ui = Self::build_timers(
                values[0].clone(),
                values[1].clone(),
                values[2].clone(),
                values[3].clone(),
            );
            tmp_layout.add(&timers_ui);
            tmp_layout.show_all();

            tmp_layout.remove(&tmp_indicator);
            Continue(false)
        });
        //page.add(&group);
        //page

        //debug!("{:?}", known_ids);
        //known_ids
    }

    fn do_load(timer: &str) -> JsonValue {
        let response = block_load!(format!("https://api.warframestat.us/pc/{}", timer).as_str());
        match response {
            Ok(res) => {
                let json = json::parse(res.as_str());
                match json {
                    Ok(json_val) => json_val,
                    Err(e) => {
                        warn!("{:?}", e.to_string());
                        Self::do_load(timer)
                    }
                }
            }
            Err(e) => {
                warn!("{:?}", e.to_string());
                Self::do_load(timer)
            }
        }
    }

    fn build_timers(
        plains_time: JsonValue,
        valleys_time: JsonValue,
        drift_time: JsonValue,
        earth_time: JsonValue,
    ) -> Box {
        let plains_timer = Self::new_plains(plains_time);
        let valleys_timer = Self::new_valleys(valleys_time);
        let drift_timer = Self::new_drift(drift_time);
        let earth_timer = Self::new_earth(earth_time);

        let list_timers = Box::new(Orientation::Vertical, 5);
        list_timers.add(&Self::build_view_timer(
            plains_timer,
            "Plains Of Eidolon".to_string(),
        ));
        list_timers.add(&Self::build_view_timer(
            valleys_timer,
            "Orbvalleys".to_string(),
        ));
        list_timers.add(&Self::build_view_timer(
            drift_timer,
            "Cambion Drift".to_string(),
        ));
        list_timers.add(&Self::build_view_timer(
            earth_timer,
            "Earth Forests".to_string(),
        ));

        list_timers
    }
    fn build_view_timer(timer: WorldTimer, name: String) -> Button {
        let desc = Label::new(None);
        let state = Label::new(None);
        let left = Label::new(None);

        desc.set_label(name.as_str());
        state.set_label(timer.state.to_uppercase().as_str());

        let time_diff = timer
            .expiry
            .signed_duration_since(Local::now())
            .num_minutes();
        left.set_label((time_diff.to_string() + " minutes left").as_str());

        let layout = gtk::Box::new(Orientation::Horizontal, 5);
        layout.add(&desc);
        layout.add(&state);

        let big_icon_label_attrs = AttrList::new();
        let big_icon_label_attr = pango::Attribute::new_scale(2.0).unwrap();
        big_icon_label_attrs.insert(big_icon_label_attr);

        state.set_attributes(Some(&big_icon_label_attrs));

        match timer.state.to_lowercase().as_str() {
            "night" => {
                let icon = Label::new(Option::from("\u{1F319}")); // moon icon
                icon.set_attributes(Some(&big_icon_label_attrs));
                layout.add(&icon);
            }
            "day" => {
                let icon = Label::new(Option::from("\u{1F323}")); // sun icon
                icon.set_attributes(Some(&big_icon_label_attrs));
                layout.add(&icon);
            }
            "cold" => {
                let icon = Label::new(Option::from("\u{2744}")); // snow flake
                icon.set_attributes(Some(&big_icon_label_attrs));
                layout.add(&icon);
            }
            "warm" => {
                let icon = Label::new(Option::from("\u{1F323}"));
                icon.set_attributes(Some(&big_icon_label_attrs));
                layout.add(&icon);
            }
            "vome" => {
                let icon = Label::new(Option::from("\u{1F319}"));
                icon.set_attributes(Some(&big_icon_label_attrs));
                layout.add(&icon);
            }
            "fass" => {
                let icon = Label::new(Option::from("\u{1F323}"));
                icon.set_attributes(Some(&big_icon_label_attrs));
                layout.add(&icon);
            }
            _ => {
                error!("Received unknown state in timers")
            }
        }
        layout.pack_end(&left, false, false, 0);
        let button = Button::new();
        button.add(&layout);

        let popup = Popover::new(Some(&button));
        let popup_label = Label::new(Some(timer.state.to_uppercase().as_str()));
        popup.add(&popup_label);
        button.connect_clicked(move |_| {
            popup.show_all();
        });
        button
    }

    fn new_plains(time: JsonValue) -> WorldTimer {
        let _activation_str = time["activation"].as_str().unwrap_or_default();
        //let _activation = DateTime::parse_from_rfc3339(activation_str).unwrap_or_default();
        let expiry_str = time["expiry"].as_str().unwrap_or_default();

        let expiry = Self::parse_date_time_str(expiry_str);

        let state = time["state"].as_str().unwrap_or_default().to_string();
        WorldTimer {
            //activation,
            expiry,
            state,
        }
    }

    fn new_valleys(time: JsonValue) -> WorldTimer {
        let _activation_str = time["activation"].as_str().unwrap_or_default();
        //let _activation = DateTime::parse_from_rfc3339(activation_str).unwrap();
        let expiry_str = time["expiry"].as_str().unwrap_or_default();
        let expiry = Self::parse_date_time_str(expiry_str);
        let warm = time["isWarm"].as_bool().unwrap_or_default();

        let state: String;
        if warm {
            state = "Warm".to_string();
        } else {
            state = "Cold".to_string();
        }

        WorldTimer {
            //activation,
            expiry,
            state,
        }
    }

    fn new_drift(time: JsonValue) -> WorldTimer {
        let _activation_str = time["activation"].as_str().unwrap_or_default();
        //let _activation = DateTime::parse_from_rfc3339(activation_str).unwrap();
        let expiry_str = time["expiry"].as_str().unwrap_or_default();
        let expiry = Self::parse_date_time_str(expiry_str);
        let state = time["active"].as_str().unwrap_or_default().to_string();
        WorldTimer {
            //activation,
            expiry,
            state,
        }
    }

    fn new_earth(time: JsonValue) -> WorldTimer {
        let expiry_str = time["expiry"].as_str().unwrap_or_default();
        let expiry = Self::parse_date_time_str(expiry_str);
        let is_day = time["isDay"].as_bool().unwrap_or_default();
        let state;
        if is_day {
            state = "day".to_string();
        } else {
            state = "night".to_string();
        }

        let _activation = expiry.sub(Duration::hours(6));

        WorldTimer {
            //activation,
            expiry,
            state,
        }
    }

    fn parse_date_time_str(expiry_str: &str) -> DateTime<Utc> {
        let parsed_expiry = DateTime::parse_from_rfc3339(expiry_str);
        if let Ok(expiry) = parsed_expiry {
            DateTime::from_utc(expiry.naive_utc(), Utc)
        } else {
            DateTime::from_utc(
                NaiveDateTime::new(
                    NaiveDate::from_ymd(1970, 1, 1),
                    NaiveTime::from_hms(0, 0, 0),
                ),
                Utc,
            )
        }
    }
}
