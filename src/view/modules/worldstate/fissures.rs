use std::thread;

use chrono::DateTime;
use glib::{clone, Continue, Sender, PRIORITY_DEFAULT};
use gtk::{
    Box, Button, ButtonBox, ButtonBoxExt, ButtonBoxStyle, ButtonExt, CheckButton, ContainerExt,
    Grid, GridExt, Label, LabelExt, ListBox, ListBoxBuilder, ListBoxRowBuilder, Orientation,
    Revealer, RevealerExt, SelectionMode, Spinner, SpinnerExt, ToggleButtonExt, WidgetExt,
};
use json::JsonValue;
use libhandy::{PreferencesGroup, PreferencesPage};

use crate::message::NitainMessage;

pub struct FissuresUi {
    link: Sender<NitainMessage>,
}

impl FissuresUi {
    pub fn new(link: Sender<NitainMessage>) -> Self {
        Self { link }
    }

    pub fn load(&self) -> libhandy::PreferencesPage {
        let layout = PreferencesPage::new();
        let group = PreferencesGroup::new();

        let toggle_lith = CheckButton::new();
        toggle_lith.set_label("Lith");
        let toggle_meso = CheckButton::new();
        toggle_meso.set_label("Meso");
        let toggle_neo = CheckButton::new();
        toggle_neo.set_label("Neo");
        let toggle_axi = CheckButton::new();
        toggle_axi.set_label("Axi");
        let toggle_requiem = CheckButton::new();
        toggle_requiem.set_label("Requiem");

        let box_lith = ListBoxBuilder::new()
            .selection_mode(SelectionMode::None)
            .build(); //Box::new(Orientation::Vertical, 5);
        let box_meso = ListBoxBuilder::new()
            .selection_mode(SelectionMode::None)
            .build(); //Box::new(Orientation::Vertical, 5);
        let box_neo = ListBoxBuilder::new()
            .selection_mode(SelectionMode::None)
            .build(); //Box::new(Orientation::Vertical, 5);
        let box_axi = ListBoxBuilder::new()
            .selection_mode(SelectionMode::None)
            .build(); //Box::new(Orientation::Vertical, 5);
        let box_requiem = ListBoxBuilder::new()
            .selection_mode(SelectionMode::None)
            .build(); //Box::new(Orientation::Vertical, 5);

        let revealer_lith = Revealer::new();
        let revealer_meso = Revealer::new();
        let revealer_neo = Revealer::new();
        let revealer_axi = Revealer::new();
        let revealer_requiem = Revealer::new();

        revealer_lith.add(&box_lith);
        revealer_lith.set_reveal_child(true);
        toggle_lith.set_active(true);
        revealer_meso.add(&box_meso);
        revealer_meso.set_reveal_child(true);
        toggle_meso.set_active(true);
        revealer_neo.add(&box_neo);
        revealer_neo.set_reveal_child(true);
        toggle_neo.set_active(true);
        revealer_axi.add(&box_axi);
        revealer_axi.set_reveal_child(true);
        toggle_axi.set_active(true);
        revealer_requiem.add(&box_requiem);
        revealer_requiem.set_reveal_child(true);
        toggle_requiem.set_active(true);

        toggle_lith.connect_toggled(clone!(@weak revealer_lith => move |button| {
            revealer_lith.set_reveal_child(button.get_active());
        }));

        toggle_meso.connect_toggled(clone!(@weak revealer_meso => move |button| {
            revealer_meso.set_reveal_child(button.get_active());
        }));

        toggle_neo.connect_toggled(clone!(@weak revealer_neo => move |button| {
            revealer_neo.set_reveal_child(button.get_active());
        }));

        toggle_axi.connect_toggled(clone!(@weak revealer_axi => move |button| {
            revealer_axi.set_reveal_child(button.get_active());
        }));

        toggle_requiem.connect_toggled(clone!(@weak revealer_requiem => move |button| {
            revealer_requiem.set_reveal_child(button.get_active());
        }));

        let box_list = Box::new(Orientation::Vertical, 5);
        let buttonbox_controls = ButtonBox::new(Orientation::Horizontal);
        buttonbox_controls.set_layout(ButtonBoxStyle::Center);

        buttonbox_controls.add(&toggle_lith);
        buttonbox_controls.add(&toggle_meso);
        buttonbox_controls.add(&toggle_neo);
        buttonbox_controls.add(&toggle_axi);
        buttonbox_controls.add(&toggle_requiem);

        box_list.add(&buttonbox_controls);
        box_list.add(&revealer_lith);
        box_list.add(&revealer_meso);
        box_list.add(&revealer_neo);
        box_list.add(&revealer_axi);
        box_list.add(&revealer_requiem);

        group.add(&box_list);

        let back_group = PreferencesGroup::new();
        let back_button = Button::with_label("Back to first page");
        let tmp_link = self.link.clone();
        back_button.connect_clicked(move |_| {
            if let Err(e) = tmp_link.send(NitainMessage::NavHome) {
                warn!("{:?}", e.to_string())
            }
        });
        back_group.add(&back_button);

        let tmp_layout = group.clone();
        let tmp_lith = box_lith;
        let tmp_meso = box_meso;
        let tmp_neo = box_neo;
        let tmp_axi = box_axi;
        let tmp_requiem = box_requiem;
        let loading_indicator = gtk::Spinner::new();
        tmp_layout.add(&loading_indicator);
        loading_indicator.start();

        // HACK: Do Important stuff before timeout, otherwise the delay will be due BEFORE the code
        Self::background_fetch(
            &tmp_layout,
            &loading_indicator,
            tmp_lith.clone(),
            tmp_meso.clone(),
            tmp_neo.clone(),
            tmp_axi.clone(),
            tmp_requiem.clone(),
        );
        tmp_layout.show_all();

        debug!("Starting Fissures Timeout");
        glib::timeout_add_seconds_local(30, move || {
            // … then repeat it ._. - I hate this hack
            benchmark!(
                "Fissures Loader",
                Self::background_fetch(
                    &tmp_layout,
                    &loading_indicator,
                    tmp_lith.clone(),
                    tmp_meso.clone(),
                    tmp_neo.clone(),
                    tmp_axi.clone(),
                    tmp_requiem.clone(),
                )
            );
            tmp_layout.show_all();
            Continue(true)
        });
        debug!("Fissures timeout done");

        layout.add(&back_group);
        layout.add(&group);
        layout
    }

    fn background_fetch(
        group: &PreferencesGroup,
        loading_indicator: &Spinner,
        box_lith: ListBox,
        box_meso: ListBox,
        box_neo: ListBox,
        box_axi: ListBox,
        box_requiem: ListBox,
    ) {
        //let page = libhandy::PreferencesPage::new();

        let (tx, rx) = glib::MainContext::channel(PRIORITY_DEFAULT);
        thread::spawn(move || {
            if let Err(e) = tx.send(Self::do_load()) {
                warn!("{:?}", e.to_string())
            }
        });

        let tmp_layout = group.clone();
        //let mut known_ids = Vec::new();
        //let mut tmp_ids = known_ids;
        let tmp_indicator = loading_indicator.clone();
        rx.attach(None, move |entries| {
            benchmark!(
                for list in &[&box_lith, &box_meso, &box_neo, &box_axi, &box_requiem] {
                    for child in list.get_children() {
                        list.remove(&child);
                    }
                }
            );

            benchmark!(for member in entries.members() {
                let label_node = Label::new(None);
                let label_type = Label::new(None);
                let label_enemy = Label::new(None);
                let label_expiry = Label::new(None);
                let label_tier = Label::new(None);

                let expiry = DateTime::parse_from_rfc3339(member["expiry"].as_str().unwrap())
                    .unwrap()
                    .format("%c")
                    .to_string();

                label_node.set_label(member["node"].as_str().unwrap());
                label_tier.set_label(member["tier"].as_str().unwrap());
                label_type.set_label(member["missionType"].as_str().unwrap());
                label_enemy.set_label(member["enemy"].as_str().unwrap());
                label_expiry.set_label(expiry.as_str());

                let grid_fissure = Grid::new();

                let label_tier_desc = Label::new(Some("Tier"));
                let label_node_desc = Label::new(Some("Node"));
                let label_type_desc = Label::new(Some("Type"));
                let label_enemy_desc = Label::new(Some("Enemy"));
                let label_expiry_desc = Label::new(Some("Expiry"));
                label_tier_desc.set_sensitive(false);
                label_node_desc.set_sensitive(false);
                label_type_desc.set_sensitive(false);
                label_enemy_desc.set_sensitive(false);
                label_expiry_desc.set_sensitive(false);

                grid_fissure.attach(&label_tier_desc, 0, 0, 1, 1);
                grid_fissure.attach(&label_node_desc, 1, 0, 1, 1);
                grid_fissure.attach(&label_type_desc, 2, 0, 1, 1);
                grid_fissure.attach(&label_enemy_desc, 3, 0, 1, 1);
                grid_fissure.attach(&label_expiry_desc, 4, 0, 1, 1);
                grid_fissure.attach(&label_tier, 0, 1, 1, 1);
                grid_fissure.attach(&label_node, 1, 1, 1, 1);
                grid_fissure.attach(&label_type, 2, 1, 1, 1);
                grid_fissure.attach(&label_enemy, 3, 1, 1, 1);
                grid_fissure.attach(&label_expiry, 4, 1, 1, 1);
                grid_fissure.set_hexpand(true);
                grid_fissure.set_column_homogeneous(true);

                match member["tier"].as_str().unwrap() {
                    "Lith" => box_lith.add(&ListBoxRowBuilder::new().child(&grid_fissure).build()),
                    "Meso" => box_meso.add(&ListBoxRowBuilder::new().child(&grid_fissure).build()),
                    "Neo" => box_neo.add(&ListBoxRowBuilder::new().child(&grid_fissure).build()),
                    "Axi" => box_axi.add(&ListBoxRowBuilder::new().child(&grid_fissure).build()),
                    "Requiem" => {
                        box_requiem.add(&ListBoxRowBuilder::new().child(&grid_fissure).build())
                    }
                    _ => debug!("Unknown fissure tier"),
                }
            });

            tmp_layout.remove(&tmp_indicator);
            benchmark!(tmp_layout.show_all());
            Continue(false)
        });
        //page.add(&group);
        //page

        //debug!("{:?}", known_ids);
        //known_ids
    }

    fn do_load() -> JsonValue {
        let response = block_load!("https://api.warframestat.us/pc/fissures");
        match response {
            Ok(res) => {
                let json = json::parse(res.as_str());
                match json {
                    Ok(json_val) => json_val,
                    Err(e) => {
                        warn!("{:?}", e.to_string());
                        Self::do_load()
                    }
                }
            }
            Err(e) => {
                warn!("{:?}", e.to_string());
                Self::do_load()
            }
        }
    }
}
