use std::thread;

use glib::Sender;
use glib::PRIORITY_DEFAULT;
use gtk::{prelude::*, Grid, Spinner};
use gtk::{Button, Label, ListBox, Orientation};
use json::JsonValue;
use libhandy::{PreferencesGroup, PreferencesPage};

use crate::{message::NitainMessage, utils::json_extract_string, world::NightwaveChallengeTypeDef};

use crate::world::NightwaveTypeDef;

pub struct NightwaveUi {
    link: Sender<NitainMessage>,
}

impl NightwaveUi {
    pub fn new(link: Sender<NitainMessage>) -> Self {
        Self { link }
    }

    pub fn load(&self) -> libhandy::PreferencesPage {
        let layout = PreferencesPage::new();
        let group = PreferencesGroup::new();

        let top_label = Label::new(None);
        top_label.set_markup("<big>Nightwave Series 3 - Intermission</big>");
        group.add(&top_label);

        let list_daily = ListBox::new();
        let list_weekly = ListBox::new();
        let list_elite = ListBox::new();

        let revealer_layout = Grid::new();

        group.add(&revealer_layout);

        let label_daily = label!("Daily");
        let label_weekly = label!("Weekly");
        let label_elite = label!("Elite");
        crate::utils::scale_label(&label_daily, 1.2);
        crate::utils::scale_label(&label_weekly, 1.2);
        crate::utils::scale_label(&label_elite, 1.2);
        revealer_layout.attach(&label_daily, 0, 0, 1, 1);
        revealer_layout.attach(&label_weekly, 1, 0, 1, 1);
        revealer_layout.attach(&label_elite, 2, 0, 1, 1);
        revealer_layout.attach(&list_daily, 0, 1, 1, 1);
        revealer_layout.attach(&list_weekly, 1, 1, 1, 1);
        revealer_layout.attach(&list_elite, 2, 1, 1, 1);

        let back_group = PreferencesGroup::new();
        let back_button = Button::with_label("Back to first page");
        let tmp_link = self.link.clone();
        back_button.connect_clicked(move |_| {
            if let Err(e) = tmp_link.send(NitainMessage::NavHome) {
                warn!("{:?}", e.to_string())
            }
        });
        back_group.add(&back_button);

        let tmp_daily = list_daily;
        let tmp_weekly = list_weekly;
        let tmp_elite = list_elite;
        let tmp_layout = group.clone();
        let loading_indicator = gtk::Spinner::new();
        tmp_layout.add(&loading_indicator);
        loading_indicator.start();

        // Do Important stuff before timeout, otherwise the delay will be due BEFORE the code
        Self::background_fetch(
            &tmp_daily,
            &tmp_weekly,
            &tmp_elite,
            &loading_indicator,
            &tmp_layout,
        );
        tmp_layout.show_all();

        glib::timeout_add_seconds_local(60, move || {
            // … then repeat it ._.
            Self::background_fetch(
                &tmp_daily,
                &tmp_weekly,
                &tmp_elite,
                &loading_indicator,
                &tmp_layout,
            );
            tmp_layout.show_all();
            Continue(true)
        });

        layout.add(&back_group);
        layout.add(&group);
        layout
    }

    fn background_fetch(
        list_daily: &ListBox,
        list_weekly: &ListBox,
        list_elite: &ListBox,
        loading_indicator: &Spinner,
        group: &PreferencesGroup,
    ) {
        let (tx, rx) = glib::MainContext::channel(PRIORITY_DEFAULT);
        thread::spawn(move || {
            if let Err(e) = tx.send(Self::do_load()) {
                warn!("{:?}", e.to_string())
            }
        });

        let tmp_layout = group.clone();
        let tmp_daily = list_daily.clone();
        let tmp_weekly = list_weekly.clone();
        let tmp_elite = list_elite.clone();
        let tmp_indicator = loading_indicator.clone();
        rx.attach(None, move |nightwave| {
            tmp_layout.remove(&tmp_indicator);

            for child in tmp_daily.get_children() {
                tmp_daily.remove(&child);
            }
            for child in tmp_weekly.get_children() {
                tmp_weekly.remove(&child);
            }
            for child in tmp_elite.get_children() {
                tmp_elite.remove(&child);
            }
            Self::build_nightwave_challenge_ui(&tmp_daily, &tmp_weekly, &tmp_elite, nightwave);

            Continue(false)
        });
    }

    fn do_load() -> NightwaveTypeDef {
        let response = block_load!("https://api.warframestat.us/pc/nightwave");
        match response {
            Ok(res) => {
                let json = json::parse(res.as_str());
                match json {
                    Ok(json_val) => Self::json_to_nightwave(json_val),
                    Err(e) => {
                        warn!("{:?}", e.to_string());
                        Self::do_load()
                    }
                }
            }
            Err(e) => {
                warn!("{:?}", e.to_string());
                Self::do_load()
            }
        }
    }

    fn json_to_nightwave(json_val: JsonValue) -> NightwaveTypeDef {
        let mut nightwave_reward_types: Vec<String> = Vec::new();
        for member in json_val["rewardTypes"].members() {
            nightwave_reward_types.push(member.as_str().unwrap().to_string());
        }

        let mut nightwave_possible_challenges: Vec<NightwaveChallengeTypeDef> = Vec::new();
        for member in json_val["possibleChallenges"].members() {
            nightwave_possible_challenges.push(Self::build_nightwave_challenge(member.clone()));
        }

        let mut nightwave_active_challenges: Vec<NightwaveChallengeTypeDef> = Vec::new();
        for member in json_val["activeChallenges"].members() {
            nightwave_active_challenges.push(Self::build_nightwave_challenge(member.clone()));
        }

        let nightwave = NightwaveTypeDef::new(
            json_val["id"].as_str().unwrap_or_default().to_string(),
            json_val["activation"]
                .as_str()
                .unwrap_or_default()
                .to_string(),
            json_val["expiry"].as_str().unwrap_or_default().to_string(),
            nightwave_reward_types,
            json_val["season"].as_u32().unwrap_or_default(),
            json_val["tag"].as_str().unwrap_or_default().to_string(),
            json_val["phase"].as_u32().unwrap_or_default(),
            nightwave_possible_challenges,
            nightwave_active_challenges,
        );
        nightwave
    }

    fn build_nightwave_challenge(src: JsonValue) -> NightwaveChallengeTypeDef {
        NightwaveChallengeTypeDef::new(
            src["id"].as_str().unwrap().to_string(),
            json_extract_string(src["activation"].clone()),
            json_extract_string(src["expiry"].clone()),
            if !src["isDaily"].is_null() {
                src["isDaily"].as_bool().unwrap()
            } else {
                false
            },
            src["isElite"].as_bool().unwrap(),
            json_extract_string(src["title"].clone()),
            json_extract_string(src["desc"].clone()),
            src["reputation"].as_u32().unwrap(),
        )
    }

    fn build_nightwave_challenge_ui(
        list_daily: &ListBox,
        list_weekly: &ListBox,
        list_elite: &ListBox,
        nightwave_object: NightwaveTypeDef,
    ) {
        for challenge in nightwave_object.active_challenges {
            let label_title = Label::new(None);
            let label_description = Label::new(None);
            let label_reputation = Label::new(None);

            label_description.set_line_wrap(true);

            label_title.set_label(challenge.title.as_str());
            label_description.set_label(challenge.desc.as_str());
            label_reputation.set_label(challenge.reputation.to_string().as_str());

            let box_challenge = gtk::Box::new(Orientation::Vertical, 5);
            box_challenge.pack_start(&label_title, false, false, 0);
            box_challenge.pack_start(&label_description, true, true, 0);
            box_challenge.pack_start(&label_reputation, false, false, 0);

            if challenge.is_daily {
                list_daily.add(&box_challenge);
            } else if challenge.is_elite {
                list_elite.add(&box_challenge);
            } else {
                list_weekly.add(&box_challenge);
            }
        }
        list_daily.show_all();
        list_elite.show_all();
        list_weekly.show_all();
    }
}
