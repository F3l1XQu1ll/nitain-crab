use crate::rigs::item::ItemOverview;
use crate::rigs::mods::ModOverview;
use crate::rigs::primary::PrimaryOverview;
use crate::rigs::warframe::WarframeOverview;
use crate::utils::{ItemClass, match_wiki};

use super::super::json::JsonValue;

pub fn build_weapon_like_rigs(items: &JsonValue, class: ItemClass) -> Vec<Box<dyn ItemOverview>> {
    let mut rigs = Vec::new();
    for item in items.members() {
        let name = item["name"].as_str().unwrap_or_default();
        let unique_name = item["uniqueName"].as_str().unwrap_or_default();
        let url = item["wikiaUrl"].as_str().unwrap_or_default();
        debug!("{}", url);
        if url.is_empty() {
            continue;
        }
        let image_res = item["imageName"].as_str().unwrap_or_default();
        let image_res_fixed = image_res.replace("voidrig.png", "voidrig-necramech.png"); // Fix Warframe Hub error
        let item_rig = PrimaryOverview::new(
            name.to_string(),
            unique_name.to_string(),
            url.to_string(),
            class,
            image_res_fixed,
            item.clone(),
        );
        rigs.push(Box::new(item_rig) as Box<dyn ItemOverview>);
    }
    rigs
}

pub fn build_warframe_like_rigs(items: &JsonValue, class: ItemClass) -> Vec<Box<dyn ItemOverview>> {
    let mut rigs = Vec::new();
    for item in items.members() {
        let name = item["name"].as_str().unwrap_or_default();
        let unique_name = item["uniqueName"].as_str().unwrap_or_default();
        let mut url = item["wikiaUrl"].as_str().unwrap_or_default();
        debug!("{}", url);
        if url.is_empty() {
            let matched = match_wiki(unique_name);
            if matched.is_empty() {
                continue;
            } else {
                url = matched;
            }
        }
        let image_res = item["imageName"].as_str().unwrap_or_default();
        let image_res_fixed = image_res.replace("voidrig.png", "voidrig-necramech.png"); // Fix Warframe Hub error
        let item_rig = WarframeOverview::new(
            name.to_string(),
            unique_name.to_string(),
            url.to_string(),
            class,
            image_res_fixed,
            item.clone(),
        );
        rigs.push(Box::new(item_rig) as Box<dyn ItemOverview>);
    }
    rigs
}

pub fn build_mod_like_rigs(items: &JsonValue, class: ItemClass) -> Vec<Box<dyn ItemOverview>> {
    let mut rigs = Vec::new();
    for item in items.members() {
        let name = item["name"].as_str().unwrap_or_default();
        let unique_name = item["uniqueName"].as_str().unwrap_or_default();
        let mut url = item["wikiaUrl"].as_str().unwrap_or_default();
        //debug!("{}", url);
        let exclude_codex = item["excludeFromCodex"].as_bool().unwrap_or(false);
        if unique_name == "/Lotus/Upgrades/Mods/Sentinel/Kubrow/ChargerFinisherMod" || // Retired from the game
            unique_name == "/Lotus/Upgrades/Mods/Syndicate/BallisticaMod" || // Not existent - at least not in the wiki
            unique_name == "/Lotus/Upgrades/Mods/PvPMods/Rifle/MarkTargetAddDamageMod" || // Wrong name and description
            unique_name == "/Lotus/Upgrades/Mods/TransmuteCores/BaseTransmuteCore" || // Retired from the game
            unique_name.ends_with("Expert")
        {
            continue;
        }
        if url.is_empty() && !exclude_codex && !unique_name.contains("/Randomized/") && !unique_name.contains("/Beginner/") {
            let matched = match_wiki(unique_name);
            if matched.is_empty() {
                continue;
            } else {
                url = matched;
            }
        }
        let item_rig = ModOverview::new(
            name.to_string(),
            unique_name.to_string(),
            url.to_string(),
            class,
            String::new(),
            item.clone(),
        );
        rigs.push(Box::new(item_rig) as Box<dyn ItemOverview>);
    }
    rigs
}

pub fn build_arcane_like_rigs(items: &JsonValue, class: ItemClass) -> Vec<Box<dyn ItemOverview>> {
    let mut rigs = Vec::new();
    for item in items.members() {
        let name = item["name"].as_str().unwrap_or_default();
        let unique_name = item["uniqueName"].as_str().unwrap_or_default();
        //let mut url = item["wikiaUrl"].as_str().unwrap_or_default();
        //debug!("{}", url);
        let exclude_codex = item["excludeFromCodex"].as_bool().unwrap_or(false);
        if exclude_codex || name.contains('/') {
            continue;
        }
        let image_res = item["imageName"].as_str().unwrap_or_default();
        let image_res_fixed = image_res.replace("voidrig.png", "voidrig-necramech.png"); // Fix Warframe Hub error
        let item_rig = ModOverview::new(
            name.to_string(),
            unique_name.to_string(),
            String::new(),
            class,
            image_res_fixed,
            item.clone(),
        );
        rigs.push(Box::new(item_rig) as Box<dyn ItemOverview>);
    }
    rigs
}