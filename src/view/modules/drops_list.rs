use glib::Type;
use gtk::{prelude::*, Label, LabelExt};
use gtk::{
    Box, CellRendererText, ListStore, Orientation, ScrolledWindow, ScrolledWindowBuilder,
    SortColumn, SortType, TreeView, TreeViewColumn,
};
use pango::{AttrList, Attribute};

use crate::{rigs::item::ItemOverview, utils::ItemClass};

use super::super::json::JsonValue;

pub fn load_drops(rig: &std::boxed::Box<dyn ItemOverview>) -> ScrolledWindow {
    let root = Box::new(Orientation::Vertical, 5);

    let source = rig.get_json();
    let drops = source["drops"].clone();

    if !drops.is_empty() {
        let drops_list = drops_from_json(drops);
        root.add(&drops_list);
    } else {
        let info_label;
        if let ItemClass::Relics = rig.get_class() {
            info_label = Label::new(Some("This Relic is currently vaulted"));
        } else {
            info_label = Label::new(Some("This item has no drop source\nYou may get it from the Market, Dojo, Syndicates\nor as Mission Reward"));
        }
        let scale = Attribute::new_scale(1.5).unwrap();
        let attrs = AttrList::new();
        attrs.insert(scale);
        info_label.set_attributes(Some(&attrs));
        root.add(&info_label);
    }

    root.set_vexpand(true);

    let alignment_box_h = Box::new(Orientation::Horizontal, 0);
    alignment_box_h.set_center_widget(Some(&root));

    let scrolled_window = ScrolledWindowBuilder::new().build();
    scrolled_window.add(&alignment_box_h);
    scrolled_window
}

pub fn drops_from_json(drops: JsonValue) -> TreeView {
    let drops_list = TreeView::new();
    let location_column = TreeViewColumn::new();
    let drop_chance_column = TreeViewColumn::new();
    let drop_type_column = TreeViewColumn::new();
    let drop_rarity_column = TreeViewColumn::new();
    let chance_value_column = TreeViewColumn::new();

    let drops_store = ListStore::new(&[
        Type::String,
        Type::String,
        Type::String,
        Type::String,
        Type::F32,
    ]);

    let location_renderer = CellRendererText::new();
    let chance_renderer = CellRendererText::new();
    let type_renderer = CellRendererText::new();
    let rarity_renderer = CellRendererText::new();
    let chance_value_renderer = CellRendererText::new();

    location_column.set_title("Drop Location");
    drop_chance_column.set_title("Drop Chance");
    drop_type_column.set_title("Drop Type");
    drop_rarity_column.set_title("Drop Rarity");

    location_column.pack_start(&location_renderer, true);
    drop_chance_column.pack_start(&chance_renderer, true);
    drop_type_column.pack_start(&type_renderer, true);
    drop_rarity_column.pack_start(&rarity_renderer, true);
    chance_value_column.pack_start(&chance_value_renderer, true);
    chance_value_column.set_visible(false);

    location_column.add_attribute(&location_renderer, "text", 0);
    drop_chance_column.add_attribute(&chance_renderer, "text", 2);
    drop_type_column.add_attribute(&type_renderer, "text", 1);
    drop_rarity_column.add_attribute(&rarity_renderer, "text", 3);
    chance_value_column.add_attribute(&chance_value_renderer, "text", 4);

    drops_store.set_sort_column_id(SortColumn::Index(4), SortType::Descending);
    drops_list.set_model(Some(&drops_store));

    drops_list.append_column(&location_column);
    drops_list.append_column(&drop_type_column);
    drops_list.append_column(&drop_chance_column);
    drops_list.append_column(&drop_rarity_column);

    for drop in drops.members() {
        let location = drop["location"].as_str().unwrap_or_default();
        let drop_type = drop["type"].as_str().unwrap_or_default();
        let chance = drop["chance"].as_f32().unwrap_or_default();
        let rarity = drop["rarity"].as_str().unwrap_or_default();

        let chance_percent = format!("{:.2}%", 100.0 * chance);

        let row = drops_store.append();
        drops_store.set_value(&row, 0, &location.to_value());
        drops_store.set_value(&row, 1, &drop_type.to_value());
        drops_store.set_value(&row, 2, &chance_percent.as_str().to_value());
        drops_store.set_value(&row, 3, &rarity.to_value());
        drops_store.set_value(&row, 4, &chance.to_value())
    }
    drops_list
}
