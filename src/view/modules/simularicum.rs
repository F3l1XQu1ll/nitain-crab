use core::f32;
use std::collections::HashMap;

use glib::clone;
use gtk::{
    Adjustment, BoxExt, Button, ButtonExt, ContainerExt, GridExt, Image, Label, Orientation,
    SpinButton, SpinButtonBuilder, SpinButtonExt, Switch, SwitchExt,
};

use crate::utils::{labeled_widget, scale_label};

macro_rules! weak_to {
    ($input:expr, $amount:expr) => {
        $input * (1.0 + ($amount as f32 / 100.0))
    };
}

macro_rules! resistant_to {
    ($input:expr, $amount:expr) => {
        $input * (1.0 - ($amount as f32 / 100.0))
    };
}

pub struct Simularicum {
    input_ui: DamageUi,
    output_ui: DamageUi,
}

pub trait Faction {
    fn apply_modifiers(&self, damage: Damage) -> Damage;
}

pub trait Weakness {
    fn apply_modifiers(&self, damage: Damage) -> Damage;
}

pub struct GrineerFaction {}
pub struct CorpusFaction {}
pub struct InfestedFaction {}

pub struct ClonedFlesh {}
pub struct FerriteArmor {}
pub struct AlloyArmor {}
pub struct Machinery {}
pub struct Flesh {}
pub struct Shield {}
pub struct ProtoShield {}
pub struct Robotic {}
pub struct Infested {}
pub struct InfestedFlesh {}
pub struct Fossilized {}
pub struct InfestedSinew {}

impl Weakness for ClonedFlesh {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact * (1.0 - 0.25),
            puncture: damage.puncture,
            slash: damage.slash * (1.0 + 0.25),
            cold: damage.cold,
            electricity: damage.electricity,
            heat: damage.heat * (1.0 + 0.25),
            toxin: damage.toxin,
            blast: damage.blast,
            corrosive: damage.corrosive,
            gas: damage.gas * (1.0 - 0.5),
            magnetic: damage.magnetic,
            radiation: damage.radiation,
            viral: damage.viral * (1.0 + 0.75),
        }
    }
}

impl Weakness for FerriteArmor {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact,
            puncture: damage.puncture * (1.0 * 0.5),
            slash: damage.slash * (1.0 - 0.15),
            cold: damage.cold,
            electricity: damage.electricity,
            heat: damage.heat,
            toxin: damage.toxin,
            blast: damage.blast * (1.0 - 0.25),
            corrosive: damage.corrosive * (1.0 + 0.75),
            gas: damage.gas,
            magnetic: damage.magnetic,
            radiation: damage.radiation,
            viral: damage.viral,
        }
    }
}

impl Weakness for AlloyArmor {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact,
            puncture: damage.puncture * (1.0 + 0.15),
            slash: damage.slash * (1.0 - 0.5),
            cold: damage.cold * (1.0 + 0.25),
            electricity: damage.electricity * (1.0 - 0.5),
            heat: damage.heat,
            toxin: damage.toxin,
            blast: damage.blast,
            corrosive: damage.corrosive,
            gas: damage.gas,
            magnetic: damage.magnetic * (1.0 - 0.5),
            radiation: damage.radiation * (1.0 + 0.75),
            viral: damage.viral,
        }
    }
}

impl Weakness for Flesh {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact * (1.0 - 0.25),
            puncture: damage.puncture,
            slash: damage.slash * (1.0 + 0.25),
            cold: damage.cold,
            electricity: damage.electricity,
            heat: damage.heat,
            toxin: damage.toxin * (1.0 + 0.5),
            blast: damage.blast,
            corrosive: damage.corrosive,
            gas: damage.gas * (1.0 - 0.25),
            magnetic: damage.magnetic,
            radiation: damage.radiation,
            viral: damage.viral * (1.0 + 0.5),
        }
    }
}

impl Weakness for Machinery {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact * (1.0 + 0.25),
            puncture: damage.puncture,
            slash: damage.slash,
            cold: damage.cold,
            electricity: damage.electricity * (1.0 + 0.5),
            heat: damage.heat,
            toxin: damage.toxin * (1.0 - 0.25),
            blast: damage.blast * (1.0 + 0.75),
            corrosive: damage.corrosive,
            gas: damage.gas,
            magnetic: damage.magnetic,
            radiation: damage.radiation,
            viral: damage.viral * (1.0 - 0.25),
        }
    }
}

impl Weakness for Shield {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact * (1.0 + 0.5),
            puncture: damage.puncture * (1.0 - 0.2),
            slash: damage.slash,
            cold: damage.cold * (1.0 + 0.5),
            electricity: damage.electricity,
            heat: damage.heat,
            toxin: damage.toxin,
            blast: damage.blast,
            corrosive: damage.corrosive,
            gas: damage.gas,
            magnetic: damage.magnetic * (1.0 + 0.75),
            radiation: damage.radiation * (1.0 - 0.25),
            viral: damage.viral,
        }
    }
}

impl Weakness for ProtoShield {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact * (1.0 + 0.15),
            puncture: damage.puncture * (1.0 - 0.5),
            slash: damage.slash,
            cold: damage.cold,
            electricity: damage.electricity,
            heat: damage.heat * (1.0 - 0.5),
            toxin: damage.toxin,
            blast: damage.blast,
            corrosive: damage.corrosive * (1.0 - 0.5),
            gas: damage.gas,
            magnetic: damage.magnetic * (1.0 + 0.75),
            radiation: damage.radiation,
            viral: damage.viral,
        }
    }
}

impl Weakness for Robotic {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact,
            puncture: damage.puncture * (1.0 + 0.25),
            slash: damage.slash * (1.0 - 0.25),
            cold: damage.cold,
            electricity: damage.electricity * (1.0 + 0.5),
            heat: damage.heat,
            toxin: damage.toxin * (1.0 - 0.25),
            blast: damage.blast,
            corrosive: damage.corrosive,
            gas: damage.gas,
            magnetic: damage.magnetic,
            radiation: damage.radiation * (1.0 + 0.25),
            viral: damage.viral,
        }
    }
}

impl Weakness for Infested {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact,
            puncture: damage.puncture,
            slash: weak_to!(damage.slash, 25),
            cold: damage.cold,
            electricity: damage.electricity,
            heat: weak_to!(damage.heat, 25),
            toxin: damage.toxin,
            blast: damage.blast,
            corrosive: damage.corrosive,
            gas: weak_to!(damage.gas, 75),
            magnetic: damage.magnetic,
            radiation: resistant_to!(damage.radiation, 50),
            viral: resistant_to!(damage.viral, 50),
        }
    }
}

impl Weakness for InfestedFlesh {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact,
            puncture: damage.puncture,
            slash: weak_to!(damage.slash, 50),
            cold: resistant_to!(damage.cold, 50),
            electricity: damage.electricity,
            heat: weak_to!(damage.heat, 50),
            toxin: damage.toxin,
            blast: damage.blast,
            corrosive: damage.corrosive,
            gas: weak_to!(damage.gas, 50),
            magnetic: damage.magnetic,
            radiation: damage.radiation,
            viral: damage.viral,
        }
    }
}

impl Weakness for Fossilized {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact,
            puncture: damage.puncture,
            slash: weak_to!(damage.slash, 15),
            cold: resistant_to!(damage.cold, 25),
            electricity: damage.electricity,
            heat: damage.heat,
            toxin: resistant_to!(damage.toxin, 50),
            blast: weak_to!(damage.blast, 50),
            corrosive: weak_to!(damage.corrosive, 75),
            gas: damage.gas,
            magnetic: damage.magnetic,
            radiation: resistant_to!(damage.radiation, 75),
            viral: damage.viral,
        }
    }
}

impl Weakness for InfestedSinew {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact,
            puncture: weak_to!(damage.puncture, 25),
            slash: damage.slash,
            cold: weak_to!(damage.cold, 25),
            electricity: damage.electricity,
            heat: damage.heat,
            toxin: damage.toxin,
            blast: resistant_to!(damage.blast, 50),
            corrosive: damage.corrosive,
            gas: damage.gas,
            magnetic: damage.magnetic,
            radiation: weak_to!(damage.radiation, 50),
            viral: damage.viral,
        }
    }
}

impl Faction for GrineerFaction {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact * (1.0 - 0.25) * (1.0 + 0.25),
            puncture: damage.puncture * (1.0 + 0.5) * (1.0 + 0.15),
            slash: damage.slash * (1.0 + 0.25) * (1.0 - 0.15) * (1.0 - 0.5),
            cold: damage.cold * (1.0 + 0.25),
            electricity: damage.electricity * (1.0 - 0.5) * (1.0 + 0.5),
            heat: damage.heat * (1.0 + 0.25),
            toxin: damage.toxin * (1.0 - 0.25),
            blast: damage.blast * (1.0 - 0.25) * (1.0 + 0.75),
            corrosive: damage.corrosive * (1.0 + 0.75),
            gas: damage.gas * (1.0 - 0.5),
            magnetic: damage.magnetic * (1.0 - 0.5),
            radiation: damage.radiation * (1.0 + 0.75),
            viral: damage.viral * (1.0 + 0.75) * (1.0 - 0.25),
        }
    }
}

impl Faction for CorpusFaction {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact * (1.0 - 0.25) * (1.0 + 0.5) * (1.0 + 0.15),
            puncture: damage.puncture * (1.0 - 0.20) * (1.0 - 0.5) * (1.0 + 0.25),
            slash: damage.slash * (1.0 + 0.25) * (1.0 - 0.25),
            cold: damage.cold * (1.0 + 0.5),
            electricity: damage.electricity * (1.0 + 0.5),
            heat: damage.heat * (1.0 - 0.5),
            toxin: damage.toxin * (1.0 + 0.5) * (1.0 - 0.25),
            blast: damage.blast,
            corrosive: damage.corrosive * (1.0 - 0.5),
            gas: damage.gas * (1.0 - 0.25),
            magnetic: damage.magnetic * (1.0 + 0.75) * (1.0 * 0.75),
            radiation: damage.radiation * (1.0 - 0.25) * (1.0 + 0.25),
            viral: damage.viral * (1.0 + 0.5),
        }
    }
}

impl Faction for InfestedFaction {
    fn apply_modifiers(&self, damage: Damage) -> Damage {
        Damage {
            impact: damage.impact,
            puncture: damage.puncture * (1.0 + 0.25),
            slash: damage.slash * (1.0 + 0.25) * (1.0 + 0.5) * (1.0 + 0.15),
            cold: damage.cold * (1.0 - 0.5) * (1.0 - 0.25) * (1.0 + 0.25),
            electricity: damage.electricity,
            heat: damage.heat * (1.0 + 0.25) * (1.0 * 0.5),
            toxin: damage.toxin * (1.0 - 0.5),
            blast: damage.blast * (1.0 + 0.5) * (1.0 - 0.5),
            corrosive: damage.corrosive * (1.0 + 0.75),
            gas: damage.gas * (1.0 + 0.75) * (1.0 + 0.5),
            magnetic: damage.magnetic,
            radiation: damage.radiation * (1.0 - 0.5) * (1.0 - 0.75) * (1.0 + 0.5),
            viral: damage.viral * (1.0 - 0.5),
        }
    }
}

trait PysicalDamage {
    fn set_impact(&mut self, damage: f32);
    fn get_impact(&self) -> f32;

    fn set_puncture(&mut self, damage: f32);
    fn get_puncture(&self) -> f32;

    fn set_slash(&mut self, damage: f32);
    fn get_slash(&self) -> f32;
}

trait ElementalDamage {
    fn set_cold(&mut self, damage: f32);
    fn get_cold(&self) -> f32;

    fn set_electricity(&mut self, damage: f32);
    fn get_electricity(&self) -> f32;

    fn set_heat(&mut self, damage: f32);
    fn get_heat(&self) -> f32;

    fn set_toxin(&mut self, damage: f32);
    fn get_toxin(&self) -> f32;
}

trait CombinedElements {
    fn set_blast(&mut self, damage: f32);
    fn get_blast(&self) -> f32;

    fn set_corrosive(&mut self, damage: f32);
    fn get_corrosive(&self) -> f32;

    fn set_gas(&mut self, damage: f32);
    fn get_gas(&self) -> f32;

    fn set_magnetic(&mut self, damage: f32);
    fn get_magnetic(&self) -> f32;

    fn set_radiation(&mut self, damage: f32);
    fn get_radiation(&self) -> f32;

    fn set_viral(&mut self, damage: f32);
    fn get_viral(&self) -> f32;
}

#[derive(Clone, Default)]
pub struct Damage {
    impact: f32,
    puncture: f32,
    slash: f32,
    cold: f32,
    electricity: f32,
    heat: f32,
    toxin: f32,
    blast: f32,
    corrosive: f32,
    gas: f32,
    magnetic: f32,
    radiation: f32,
    viral: f32,
}

impl PysicalDamage for Damage {
    fn set_impact(&mut self, damage: f32) {
        self.impact = damage;
    }

    fn get_impact(&self) -> f32 {
        self.impact
    }

    fn set_puncture(&mut self, damage: f32) {
        self.puncture = damage;
    }

    fn get_puncture(&self) -> f32 {
        self.puncture
    }

    fn set_slash(&mut self, damage: f32) {
        self.slash = damage;
    }

    fn get_slash(&self) -> f32 {
        self.slash
    }
}

impl ElementalDamage for Damage {
    fn set_cold(&mut self, damage: f32) {
        self.cold = damage;
    }

    fn get_cold(&self) -> f32 {
        self.cold
    }

    fn set_electricity(&mut self, damage: f32) {
        self.electricity = damage
    }

    fn get_electricity(&self) -> f32 {
        self.electricity
    }

    fn set_heat(&mut self, damage: f32) {
        self.heat = damage
    }

    fn get_heat(&self) -> f32 {
        self.heat
    }

    fn set_toxin(&mut self, damage: f32) {
        self.toxin = damage
    }

    fn get_toxin(&self) -> f32 {
        self.toxin
    }
}

impl CombinedElements for Damage {
    fn set_blast(&mut self, damage: f32) {
        self.blast = damage
    }

    fn get_blast(&self) -> f32 {
        self.blast
    }

    fn set_corrosive(&mut self, damage: f32) {
        self.corrosive = damage
    }

    fn get_corrosive(&self) -> f32 {
        self.corrosive
    }

    fn set_gas(&mut self, damage: f32) {
        self.gas = damage
    }

    fn get_gas(&self) -> f32 {
        self.gas
    }

    fn set_magnetic(&mut self, damage: f32) {
        self.magnetic = damage
    }

    fn get_magnetic(&self) -> f32 {
        self.magnetic
    }

    fn set_radiation(&mut self, damage: f32) {
        self.radiation = damage
    }

    fn get_radiation(&self) -> f32 {
        self.radiation
    }

    fn set_viral(&mut self, damage: f32) {
        self.viral = damage
    }

    fn get_viral(&self) -> f32 {
        self.viral
    }
}

impl Damage {
    fn balance_elements(&mut self) {
        if self.cold != 0.0 && self.heat != 0.0 {
            self.blast = self.blast + self.cold + self.heat;
            self.cold = 0.0;
            self.heat = 0.0;
        }

        if self.electricity != 0.0 && self.toxin != 0.0 {
            self.corrosive = self.corrosive + self.electricity + self.toxin;
            self.electricity = 0.0;
            self.toxin = 0.0;
        }

        if self.heat != 0.0 && self.toxin != 0.0 {
            self.gas = self.gas + self.heat + self.toxin;
            self.heat = 0.0;
            self.toxin = 0.0;
        }

        if self.cold != 0.0 && self.electricity != 0.0 {
            self.magnetic = self.magnetic + self.cold + self.electricity;
            self.cold = 0.0;
            self.electricity = 0.0;
        }

        if self.electricity != 0.0 && self.heat != 0.0 {
            self.radiation = self.radiation + self.electricity + self.heat;
            self.electricity = 0.0;
            self.heat = 0.0;
        }

        if self.cold != 0.0 && self.toxin != 0.0 {
            self.viral = self.viral + self.cold + self.toxin;
            self.cold = 0.0;
            self.toxin = 0.0;
        }
    }
}

impl Simularicum {
    pub fn new() -> Simularicum {
        Simularicum {
            input_ui: DamageUi::new(),
            output_ui: DamageUi::new(),
        }
    }

    fn set_output_ui(&mut self, ui: DamageUi) {
        self.output_ui = ui
    }

    fn set_input_ui(&mut self, ui: DamageUi) {
        self.input_ui = ui
    }

    pub fn build(&mut self) -> gtk::Box {
        let root = gtk::Box::new(gtk::Orientation::Horizontal, 0);

        let layout = gtk::Grid::new();
        root.set_center_widget(Some(&layout));

        let input_damage_ui = Self::build_damage_ui(true);
        let output_damage_ui = Self::build_damage_ui(false);
        self.set_input_ui(input_damage_ui.clone());
        self.set_output_ui(output_damage_ui.clone());

        /*let selection_list = ListBox::new();
        let grineer_row = SelectionRow::new(
            Label::new(Some("Grineer")),
            Image::from_icon_name(Some("object-select-symbolic"), gtk::IconSize::Button),
        );

        let corpus_row = SelectionRow::new(
            Label::new(Some("Cortpus")),
            Image::from_icon_name(Some("object-select-symbolic"), gtk::IconSize::Button),
        );

        let infested_row = SelectionRow::new(
            Label::new(Some("Infested")),
            Image::from_icon_name(Some("object-select-symbolic"), gtk::IconSize::Button),
        );

        //test_row.set_widget_name("Row1");
        let mode_label = Label::new(Some("grineer"));
        selection_list.connect_row_selected(
            glib::clone!(@strong grineer_row, @strong corpus_row, @strong infested_row, @strong mode_label => move |_,row| {
                //let name = row.get_widget_name();
                let index = row.unwrap().get_index();
                debug!("Row selected: {:?}", index);
                match index {
                    0 => {
                        grineer_row.get_image().set_opacity(1.0);
                        corpus_row.get_image().set_opacity(0.0);
                        infested_row.get_image().set_opacity(0.0);
                        mode_label.set_text("grineer");
                    }
                    1 => {
                        grineer_row.get_image().set_opacity(0.0);
                        corpus_row.get_image().set_opacity(1.0);
                        infested_row.get_image().set_opacity(0.0);
                        mode_label.set_text("corpus")
                    }
                    2 => {
                        grineer_row.get_image().set_opacity(0.0);
                        corpus_row.get_image().set_opacity(0.0);
                        infested_row.get_image().set_opacity(1.0);
                        mode_label.set_text("infested")
                    }
                    _ => {
                        grineer_row.get_image().set_opacity(0.0);
                        corpus_row.get_image().set_opacity(0.0);
                        infested_row.get_image().set_opacity(0.0);
                        mode_label.set_text("None")
                    }
                }
            }),
        );

        let grineer_lbr = ListBoxRow::new();
        let corpus_lbr = ListBoxRow::new();
        let infested_lbr = ListBoxRow::new();

        grineer_lbr.add(grineer_row.get_layout());
        corpus_lbr.add(corpus_row.get_layout());
        infested_lbr.add(infested_row.get_layout());

        selection_list.insert(&grineer_lbr, 0);
        selection_list.insert(&corpus_lbr, 1);
        selection_list.insert(&infested_lbr, 2);

        grineer_lbr.activate();*/

        let toggle_flesh = Switch::new();
        let toggle_cloned_flesh = Switch::new();
        let toggle_infested_flesh = Switch::new();
        let toggle_infested = Switch::new();
        let toggle_infested_sinew = Switch::new();
        let toggle_fossilized = Switch::new();
        let toggle_alloy_armor = Switch::new();
        let toggle_ferite_armor = Switch::new();
        let toggle_shield = Switch::new();
        let toggle_proto_shield = Switch::new();
        let toggle_machinery = Switch::new();
        let toggle_robotic = Switch::new();

        /*toggle_infested_flesh.connect_toggled(move |button| {
            active_modifiers.insert(BodyType::InfestedFlesh, button.get_active());
        });

        toggle_infested_sinew.connect_toggled(move |button| {
            active_modifiers.insert(BodyType::InfestedSinew, button.get_active());
        });

        toggle_fossilized.connect_toggled(move |button| {
            active_modifiers.insert(BodyType::Fossilized, button.get_active());
        });*/

        //grineer_lbr.set_selectable(false);
        //corpus_lbr.set_selectable(false);
        //infested_lbr.set_selectable(false);

        let tmp_flesh = toggle_flesh.clone();
        let tmp_cloned_flesh = toggle_cloned_flesh.clone();
        let tmp_infested_flesh = toggle_infested_flesh.clone();
        let tmp_infested = toggle_infested.clone();
        let tmp_infested_sinew = toggle_infested_sinew.clone();
        let tmp_fossilized = toggle_fossilized.clone();
        let tmp_alloy_armor = toggle_alloy_armor.clone();
        let tmp_ferite_armor = toggle_ferite_armor.clone();
        let tmp_shield = toggle_shield.clone();
        let tmp_proto_shield = toggle_proto_shield.clone();
        let tmp_machinery = toggle_machinery.clone();
        let tmp_robotic = toggle_robotic.clone();

        let button_update = Button::with_label("Update");
        button_update.connect_clicked(
            clone!(@strong input_damage_ui, @strong output_damage_ui  => move |_| {
                debug!("Updating!");

                let mut mods: Vec<Box<dyn Weakness>> = Vec::new();
                if tmp_flesh.get_active() {
                    mods.push(Box::new(Flesh{}));
                }

                if tmp_cloned_flesh.get_active() {
                    mods.push(Box::new(ClonedFlesh{}));
                }

                if tmp_infested_flesh.get_active() {
                    mods.push(Box::new(InfestedFlesh{}));
                }

                if tmp_infested.get_active() {
                    mods.push(Box::new(Infested{}));
                }

                if tmp_infested_sinew.get_active() {
                    mods.push(Box::new(InfestedSinew{}));
                }

                if tmp_fossilized.get_active() {
                    mods.push(Box::new(Fossilized{}));
                }

                if tmp_alloy_armor.get_active() {
                    mods.push(Box::new(AlloyArmor{}));
                }

                if tmp_ferite_armor.get_active() {
                    mods.push(Box::new(FerriteArmor{}));
                }

                if tmp_shield.get_active() {
                    mods.push(Box::new(Shield{}));
                }
                if tmp_proto_shield.get_active() {
                    mods.push(Box::new(ProtoShield{}));
                }
                if tmp_machinery.get_active() {
                    mods.push(Box::new(Machinery{}));
                }
                if tmp_robotic.get_active() {
                    mods.push(Box::new(Robotic{}));
                }

                Self::update_damage(input_damage_ui.clone(), mods, output_damage_ui.clone());


                //debug!("{:?}",mod_clone.get(&BodyType::Flesh));
                /*match mode_label.get_text().as_str() {
                    "grineer" => {
                        Self::update_damage(input_damage_ui.clone(), GrineerFaction::new(), output_damage_ui.clone())
                    }
                    "corpus" => {
                        Self::update_damage(input_damage_ui.clone(), CorpusFaction::new(), output_damage_ui.clone());
                    }
                    "infested" => {
                        Self::update_damage(input_damage_ui.clone(), InfestedFaction::new(), output_damage_ui.clone())
                    }
                    _ => ()
                }*/

            }),
        );

        //let input_gui = input_damage_ui.clone();
        //let output_gui = output_damage_ui.clone();

        let selector_layout = gtk::Box::new(Orientation::Vertical, 5);
        let row_cloned_flesh = labeled_widget(&toggle_cloned_flesh, "Cloned Flesh");
        selector_layout.add(&row_cloned_flesh);
        let row_ferrite_armor = labeled_widget(&toggle_ferite_armor, "Ferrite Armor");
        selector_layout.add(&row_ferrite_armor);
        let row_alloy_armor = labeled_widget(&toggle_alloy_armor, "Alloy Armor");
        selector_layout.add(&row_alloy_armor);
        let row_flesh = labeled_widget(&toggle_flesh, "Flesh");
        selector_layout.add(&row_flesh);
        let row_machinery = labeled_widget(&toggle_machinery, "Machinery");
        selector_layout.add(&row_machinery);
        let row_shield = labeled_widget(&toggle_shield, "Shield");
        selector_layout.add(&row_shield);
        let row_proto_shield = labeled_widget(&toggle_proto_shield, "Proto Shield");
        selector_layout.add(&row_proto_shield);
        let row_robotic = labeled_widget(&toggle_robotic, "Robotic");
        selector_layout.add(&row_robotic);
        let row_infested = labeled_widget(&toggle_infested, "Infested");
        selector_layout.add(&row_infested);
        let row_infested_flesh = labeled_widget(&toggle_infested_flesh, "Infested Flesh");
        selector_layout.add(&row_infested_flesh);
        let row_fossilized = labeled_widget(&toggle_fossilized, "Fossilized");
        selector_layout.add(&row_fossilized);
        let row_infested_sinew = labeled_widget(&toggle_infested_sinew, "Infested Sinew");
        selector_layout.add(&row_infested_sinew);

        //let update_button_layout = gtk::Box::new(Orientation::Vertical, 0);

        let input_label = Label::new(Some("Source Damage:"));
        let output_label = Label::new(Some("Output Damage:"));

        //selector_layout.add(&selection_list);
        //update_button_layout.add(&button_update);

        scale_label(&input_label, 1.5);
        scale_label(&output_label, 1.5);

        layout.attach(&input_label, 0, 0, 1, 1);
        layout.attach(&output_label, 2, 0, 1, 1);
        layout.attach(input_damage_ui.get_layout(), 0, 1, 1, 4);
        layout.attach(&selector_layout, 1, 2, 1, 1);
        layout.attach(&button_update, 1, 3, 1, 1);
        layout.attach(output_damage_ui.get_layout(), 2, 1, 1, 4);

        layout.set_column_spacing(10);
        layout.set_row_spacing(10);
        layout.set_row_homogeneous(false);

        root
    }

    fn build_damage_ui(editable: bool) -> DamageUi {
        let mut damage_ui = DamageUi::new();
        let row_impact = DamageRow::new("Impact", editable);
        let row_puncture = DamageRow::new("Puncture", editable);
        let row_slash = DamageRow::new("Slash", editable);

        let row_cold = DamageRow::new("Cold", editable);
        let row_electricity = DamageRow::new("Electricity", editable);
        let row_heat = DamageRow::new("Heat", editable);
        let row_toxin = DamageRow::new("Toxin", editable);

        let row_blast = DamageRow::new("Blast", editable);
        let row_corrosive = DamageRow::new("Corrosive", editable);
        let row_gas = DamageRow::new("Gas", editable);
        let row_magnetic = DamageRow::new("Magnetic", editable);
        let row_radiation = DamageRow::new("Radiation", editable);
        let row_viral = DamageRow::new("Viral", editable);

        damage_ui.add_row(row_impact, "impact");
        damage_ui.add_row(row_puncture, "puncture");
        damage_ui.add_row(row_slash, "slash");

        damage_ui.add_row(row_cold, "cold");
        damage_ui.add_row(row_electricity, "electricity");
        damage_ui.add_row(row_heat, "heat");
        damage_ui.add_row(row_toxin, "toxin");

        damage_ui.add_row(row_blast, "blast");
        damage_ui.add_row(row_corrosive, "corrosive");
        damage_ui.add_row(row_gas, "gas");
        damage_ui.add_row(row_magnetic, "magnetic");
        damage_ui.add_row(row_radiation, "radiation");
        damage_ui.add_row(row_viral, "viral");

        damage_ui
    }

    fn update_damage(damage: DamageUi, factions: Vec<Box<dyn Weakness>>, ui: DamageUi) {
        let mut input_damage = Damage {
            impact: damage.get_row("impact").unwrap().get_value() as f32,
            puncture: damage.get_row("puncture").unwrap().get_value() as f32,
            slash: damage.get_row("slash").unwrap().get_value() as f32,
            cold: damage.get_row("cold").unwrap().get_value() as f32,
            electricity: damage.get_row("electricity").unwrap().get_value() as f32,
            heat: damage.get_row("heat").unwrap().get_value() as f32,
            toxin: damage.get_row("toxin").unwrap().get_value() as f32,
            blast: damage.get_row("blast").unwrap().get_value() as f32,
            corrosive: damage.get_row("corrosive").unwrap().get_value() as f32,
            gas: damage.get_row("gas").unwrap().get_value() as f32,
            magnetic: damage.get_row("magnetic").unwrap().get_value() as f32,
            radiation: damage.get_row("radiation").unwrap().get_value() as f32,
            viral: damage.get_row("viral").unwrap().get_value() as f32,
        };

        input_damage.balance_elements();
        let mut output_damage = input_damage;
        for faction in factions {
            output_damage = faction.apply_modifiers(output_damage.clone());
        }

        Self::update_row("impact", output_damage.get_impact().into(), &ui);
        Self::update_row("puncture", output_damage.get_puncture().into(), &ui);
        Self::update_row("slash", output_damage.get_slash().into(), &ui);

        Self::update_row("cold", output_damage.get_cold().into(), &ui);
        Self::update_row("electricity", output_damage.get_electricity().into(), &ui);
        Self::update_row("heat", output_damage.get_heat().into(), &ui);
        Self::update_row("toxin", output_damage.get_toxin().into(), &ui);

        Self::update_row("blast", output_damage.get_blast().into(), &ui);
        Self::update_row("corrosive", output_damage.get_corrosive().into(), &ui);
        Self::update_row("gas", output_damage.get_gas().into(), &ui);
        Self::update_row("magnetic", output_damage.get_magnetic().into(), &ui);
        Self::update_row("radiation", output_damage.get_radiation().into(), &ui);
        Self::update_row("viral", output_damage.get_viral().into(), &ui);
    }

    fn update_row(name: &'static str, value: f64, ui: &DamageUi) {
        if let Some(output_row) = ui.get_row(name) {
            output_row.set_value(value);
        }
    }
}

#[derive(Clone)]
struct SelectionRow {
    label: Label,
    image: Image,
    layout: gtk::Box,
}

#[derive(Clone)]
struct DamageUi {
    rows: HashMap<&'static str, DamageRow>,
    layout: gtk::Box,
}

impl DamageUi {
    pub fn new() -> Self {
        Self {
            rows: HashMap::new(),
            layout: gtk::Box::new(Orientation::Vertical, 0),
        }
    }

    fn add_row(&mut self, row: DamageRow, name: &'static str) {
        let row_layout = gtk::Box::new(Orientation::Horizontal, 10);
        row_layout.pack_start(row.get_caption(), false, true, 0);
        row_layout.pack_end(row.get_spin(), false, false, 0);
        self.layout.add(&row_layout);
        self.rows.insert(name, row);
    }

    fn get_layout(&self) -> &gtk::Box {
        &self.layout
    }

    fn get_row(&self, name: &'static str) -> Option<&DamageRow> {
        self.rows.get(name)
    }
}

#[derive(Clone)]
struct DamageRow {
    caption: Label,
    spin: SpinButton,
}

impl DamageRow {
    pub fn new(caption: &str, editable: bool) -> Self {
        Self {
            caption: Label::new(Some(caption)),
            spin: SpinButtonBuilder::new()
                .adjustment(&Adjustment::new(0.0, 0.0, f64::MAX, 1.0, 50.0, 0.0))
                .sensitive(editable)
                .build(),
        }
    }

    fn get_value(&self) -> f64 {
        self.spin.get_value()
    }

    fn set_value(&self, value: f64) {
        self.spin.set_value(value);
    }

    fn get_spin(&self) -> &SpinButton {
        &self.spin
    }

    fn get_caption(&self) -> &Label {
        &self.caption
    }
}
