use std::thread;

use glib::clone;
use gtk::prelude::*;
use gtk::{
    FlowBox, FlowBoxChild, Notebook, Orientation, PolicyType, ScrolledWindowBuilder, SearchBar,
    SearchEntry, SelectionMode, Stack,
};

use crate::rigs::item::ItemOverview;
use crate::uplink::images::load_git_image;
use crate::utils::{match_item_class_name, ItemClass};
use crate::view::image_foundry;
use crate::view::modules::item_list::{build_closeable_tab, build_rig_container, load_items};
use crate::view::modules::rig_forge::{
    build_arcane_like_rigs, build_mod_like_rigs, build_warframe_like_rigs, build_weapon_like_rigs,
};

pub fn on_button_load_clicked(
    item_class: ItemClass,
    item_notebook: Notebook,
    state_display: &Stack,
) {
    state_display.set_visible_child_name("loading");

    let (sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
    thread::spawn(move || {
        let items = load_items(item_class);
        if let Err(e) = sender.send(items) {
            error!("{}", e.to_string());
        }
    });

    let tmp_state_display = state_display.clone();

    receiver.attach(None, move |items| {
        let box_items = FlowBox::new();
        box_items.set_selection_mode(SelectionMode::None);
        box_items.set_row_spacing(30);
        box_items.set_sort_func(Some(std::boxed::Box::from(sort_compare)));

        let search_entry = SearchEntry::new();
        search_entry.connect_search_changed(clone!(@weak box_items => move |entry| {
            filter_childs(&box_items, &*entry.get_text());
        }));

        let searchbar = SearchBar::new();
        searchbar.connect_entry(&search_entry);
        searchbar.set_show_close_button(false);
        searchbar.set_search_mode(true);
        searchbar.add(&search_entry);

        let rigs: Vec<std::boxed::Box<dyn ItemOverview>>;
        if let Some(items) = items {
            match item_class {
                ItemClass::Warframes => {
                    rigs = build_warframe_like_rigs(&items, ItemClass::Warframes);
                }
                ItemClass::Secondary => {
                    rigs = build_weapon_like_rigs(&items, ItemClass::Secondary);
                }
                ItemClass::Primary => {
                    rigs = build_weapon_like_rigs(&items, ItemClass::Primary);
                }
                ItemClass::Archwing => {
                    rigs = build_warframe_like_rigs(&items, ItemClass::Archwing);
                }
                ItemClass::ArchGun => {
                    rigs = build_weapon_like_rigs(&items, ItemClass::ArchGun);
                }
                ItemClass::Melee => {
                    rigs = build_weapon_like_rigs(&items, ItemClass::Melee);
                }
                ItemClass::Mods => {
                    rigs = build_mod_like_rigs(&items, ItemClass::Mods);
                }
                ItemClass::Arcanes => {
                    rigs = build_arcane_like_rigs(&items, ItemClass::Arcanes);
                }
                ItemClass::ArchMelee => {
                    rigs = build_weapon_like_rigs(&items, ItemClass::ArchMelee);
                }
                ItemClass::Relics => {
                    rigs = build_arcane_like_rigs(&items, ItemClass::Relics);
                }
                ItemClass::Resources => {
                    rigs = build_arcane_like_rigs(&items, ItemClass::Resources);
                }
            }

            for rig in rigs {
                let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
                //let tmp_rig = rig.clone();
                let image = rig.get_image().to_string();
                if !image.is_empty() {
                    thread::spawn(move || {
                        let bytes = load_git_image(image.as_str());
                        if let Err(e) = tx.send(bytes) {
                            error!("{}", e.to_string());
                        }
                    });

                    let name = rig.get_name().to_string();
                    let (rig_container, rig_image) = build_rig_container(rig, &item_notebook, true);

                    let child = FlowBoxChild::new();
                    child.add(&rig_container);
                    child.set_widget_name(name.as_str());
                    box_items.add(&child);

                    rx.attach(None, move |bytes| {
                        let pixbuf = image_foundry::image_from_stream(bytes, 124, 124)
                            .unwrap()
                            .get_pixbuf();
                        if let Some(pixbuf) = pixbuf {
                            rig_image.set_from_pixbuf(Some(&pixbuf));
                        }
                        Continue(false)
                    });
                } else {
                    let name = rig.get_name().to_string();
                    let (rig_container, _rig_image) =
                        build_rig_container(rig, &item_notebook, false);
                    let child = FlowBoxChild::new();
                    child.add(&rig_container);
                    child.set_widget_name(name.as_str());
                    box_items.add(&child)
                }
            }
        }

        let items_scroll = ScrolledWindowBuilder::new().build();
        let container = gtk::Box::new(Orientation::Vertical, 0);
        container.add(&searchbar);
        container.add(&items_scroll);
        items_scroll.add(&box_items);
        items_scroll.set_property_hscrollbar_policy(PolicyType::Never);
        items_scroll.set_vexpand(true);

        let tab_label = build_closeable_tab(
            match_item_class_name(item_class),
            &item_notebook,
            &container,
        );
        item_notebook.insert_page(&container, Some(&tab_label), None);
        //item_notebook.set_tab_detachable(&container, true);

        item_notebook.show_all();

        tmp_state_display.set_visible_child_name("ready");

        Continue(false)
    });
}

fn filter_childs(flowbox: &FlowBox, search: &str) {
    if search.is_empty() {
        flowbox.show_all();
    } else {
        for child in flowbox.get_children() {
            let name = child.get_widget_name();
            if name.to_lowercase().contains(&search.to_lowercase()) {
                if !child.is_visible() {
                    child.show_all();
                }
                continue;
            } else {
                child.hide()
            }
        }
    }
}

fn sort_compare(child_1: &FlowBoxChild, child_2: &FlowBoxChild) -> i32 {
    let name_1 = child_1.get_widget_name();
    let name_2 = child_2.get_widget_name();
    let value_1;
    let value_2;

    if name_1.ends_with("Exceptional") {
        value_1 = 1;
    } else if name_1.ends_with("Flawless") {
        value_1 = 2;
    } else if name_1.ends_with("Intact") {
        value_1 = 0;
    } else if name_1.ends_with("Radiant") {
        value_1 = 3;
    } else {
        value_1 = -1;
    }

    if name_2.ends_with("Exceptional") {
        value_2 = 1;
    } else if name_2.ends_with("Flawless") {
        value_2 = 2;
    } else if name_2.ends_with("Intact") {
        value_2 = 0;
    } else if name_2.ends_with("Radiant") {
        value_2 = 3;
    } else {
        value_2 = -1;
    }

    match value_1.cmp(&value_2) {
        std::cmp::Ordering::Less => -1,
        std::cmp::Ordering::Equal => 0,
        std::cmp::Ordering::Greater => 1,
    }
}
