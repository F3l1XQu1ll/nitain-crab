use glib::{clone, IsA};
use gtk::prelude::*;
use gtk::{
    Box, Button, ButtonBuilder, IconSize, Image, Label, ListBox, ListBoxRow, Notebook, Orientation,
    ReliefStyle, SelectionMode, Spinner, Stack, Widget,
};

use crate::rigs::item::ItemOverview;
use crate::uplink::create_client;
use crate::uplink::fetch::do_fetch_git;
use crate::utils::{match_item_class_name, ItemClass};
use crate::view::modules::arcanes_list::on_arcane_more_clicked;
use crate::view::modules::archwings_list::on_archwing_more_clicked;
use crate::view::modules::items_list_forge::on_button_load_clicked;
use crate::view::modules::mods_list::on_mod_more_clicked;
use crate::view::modules::primary_list::on_primary_more_clicked;
use crate::view::modules::warframes_list::on_warframe_more_clicked;

use super::super::json::JsonValue;
use crate::view::modules::resources_list::on_resource_more_clicked;

pub fn build_item_list() -> Box {
    let view_container = Box::new(Orientation::Horizontal, 0);

    let category_selector = Box::new(Orientation::Vertical, 5);

    let info_container = Notebook::new();
    info_container.set_hexpand(true);
    info_container.set_scrollable(true);
    info_container.set_property_enable_popup(true);

    let button_items_warframes = Button::new();
    let button_items_primaries = Button::new();
    let button_items_secondaries = Button::new();
    let button_items_melees = Button::new();
    let button_items_archwings = Button::new();
    let button_items_arch_guns = Button::new();
    let button_items_mods = Button::new();
    let button_items_arcanes = Button::new();
    let button_items_arch_melees = Button::new();
    let button_items_relics = Button::new();
    let button_items_resources = Button::new();

    let box_item_selection = Box::new(Orientation::Vertical, 5);
    box_item_selection.add(&button_items_warframes);
    box_item_selection.add(&button_items_primaries);
    box_item_selection.add(&button_items_secondaries);
    box_item_selection.add(&button_items_melees);
    box_item_selection.add(&button_items_archwings);
    box_item_selection.add(&button_items_arch_guns);
    box_item_selection.add(&button_items_arch_melees);
    box_item_selection.add(&button_items_mods);
    box_item_selection.add(&button_items_arcanes);
    box_item_selection.add(&button_items_relics);
    box_item_selection.add(&button_items_resources);

    let (button_items_warframes_content, warframes_icon_stack) =
        build_category_selector("Warframes");
    button_items_warframes.add(&button_items_warframes_content);

    let (button_items_primaries_content, primaries_icon_stack) =
        build_category_selector("Primaries");
    button_items_primaries.add(&button_items_primaries_content);

    let (button_items_secondaries_content, secondaries_icon_stack) =
        build_category_selector("Secondaries");
    button_items_secondaries.add(&button_items_secondaries_content);

    let (button_items_archwings_content, archwings_icon_stack) =
        build_category_selector("Archwings");
    button_items_archwings.add(&button_items_archwings_content);

    let (button_items_arch_guns_content, arch_guns_icon_stack) =
        build_category_selector("Arch-Guns");
    button_items_arch_guns.add(&button_items_arch_guns_content);

    let (button_items_melees_content, melees_icon_stack) = build_category_selector("Melees");
    button_items_melees.add(&button_items_melees_content);

    let (button_items_mods_content, mods_icon_stack) = build_category_selector("Mods");
    button_items_mods.add(&button_items_mods_content);

    let (button_items_arcanes_content, arcanes_icon_stack) = build_category_selector("Arcanes");
    button_items_arcanes.add(&button_items_arcanes_content);

    let (button_items_arch_melees_content, arch_melees_icon_stack) =
        build_category_selector("Arch-Melees");
    button_items_arch_melees.add(&button_items_arch_melees_content);

    let (button_items_relics_content, relics_icon_stack) = build_category_selector("Relics");
    button_items_relics.add(&button_items_relics_content);

    let (button_items_resources_content, resources_icon_stack) =
        build_category_selector("Resources");
    button_items_resources.add(&button_items_resources_content);

    button_items_warframes.connect_clicked(clone!(@weak info_container => move |_| {
        on_button_load_clicked(ItemClass::Warframes, info_container, &warframes_icon_stack);
    }));

    button_items_primaries.connect_clicked(clone!(@weak info_container => move |_| {
        on_button_load_clicked(ItemClass::Primary, info_container, &primaries_icon_stack);
    }));

    button_items_secondaries.connect_clicked(clone!(@weak info_container => move |_| {
        on_button_load_clicked(ItemClass::Secondary, info_container, &secondaries_icon_stack);
    }));

    button_items_archwings.connect_clicked(clone!(@weak info_container => move |_| {
        on_button_load_clicked(ItemClass::Archwing, info_container, &archwings_icon_stack);
    }));

    button_items_arch_guns.connect_clicked(clone!(@weak info_container => move |_| {
        on_button_load_clicked(ItemClass::ArchGun, info_container, &arch_guns_icon_stack);
    }));

    button_items_melees.connect_clicked(clone!(@weak info_container => move |_| {
        on_button_load_clicked(ItemClass::Melee, info_container, &melees_icon_stack);
    }));

    button_items_mods.connect_clicked(clone!(@weak info_container => move |_| {
        on_button_load_clicked(ItemClass::Mods, info_container, &mods_icon_stack);
    }));

    button_items_arcanes.connect_clicked(clone!(@weak info_container => move |_| {
        on_button_load_clicked(ItemClass::Arcanes, info_container, &arcanes_icon_stack);
    }));

    button_items_arch_melees.connect_clicked(clone!(@weak info_container => move |_| {
        on_button_load_clicked(ItemClass::ArchMelee, info_container, &arch_melees_icon_stack);
    }));

    button_items_relics.connect_clicked(clone!(@weak info_container => move |_| {
        on_button_load_clicked(ItemClass::Relics, info_container, &relics_icon_stack);
    }));

    button_items_resources.connect_clicked(clone!(@weak info_container => move |_| {
        on_button_load_clicked(ItemClass::Resources, info_container, &resources_icon_stack);
    }));

    let list = ListBox::new();
    list.set_selection_mode(SelectionMode::None);
    list.set_hexpand(false);
    let row_warframes = ListBoxRow::new();
    row_warframes.add(&box_item_selection);
    list.add(&row_warframes);

    category_selector.add(&list);

    view_container.add(&category_selector);
    view_container.add(&info_container);

    view_container
}

fn quick_load_item_git(item: &str) -> Option<JsonValue> {
    let runtime = tokio::runtime::Runtime::new().expect("Failed to initialize runtime");
    let warframes_response = runtime.block_on(do_fetch_git(item, create_client()));

    match warframes_response {
        Ok(warframes_string) => {
            let warframes_json = json::parse(warframes_string.as_str()).unwrap();
            //debug!("{}", warframes_json.to_string());
            Some(warframes_json)
        }
        Err(e) => {
            error!("{}", e.to_string());
            None
        }
    }
}

pub(crate) fn load_items(class: ItemClass) -> Option<JsonValue> {
    match class {
        ItemClass::Warframes => quick_load_item_git("Warframes.json"),
        ItemClass::Primary => quick_load_item_git("Primary.json"),
        ItemClass::Secondary => quick_load_item_git("Secondary.json"),
        ItemClass::Archwing => quick_load_item_git("Archwing.json"),
        ItemClass::ArchGun => quick_load_item_git("Arch-Gun.json"),
        ItemClass::Melee => quick_load_item_git("Melee.json"),
        ItemClass::Mods => quick_load_item_git("Mods.json"),
        ItemClass::Arcanes => quick_load_item_git("Arcanes.json"),
        ItemClass::ArchMelee => quick_load_item_git("Arch-Melee.json"),
        ItemClass::Relics => quick_load_item_git("Relics.json"),
        ItemClass::Resources => quick_load_item_git("Resources.json"),
    }
}

pub(crate) fn build_closeable_tab<T: IsA<Widget>>(
    label: &str,
    item_notebook: &Notebook,
    page_container: &T,
) -> Box {
    let tab_label = Box::new(Orientation::Horizontal, 10);
    let tab_label_label = Label::new(Some(label));
    let tab_label_button = ButtonBuilder::new().border_width(0).build();
    let tab_label_button_icon =
        Image::from_icon_name(Some("window-close-symbolic"), IconSize::Button);
    tab_label_button.add(&tab_label_button_icon);
    tab_label_button.set_focus_on_click(false);
    tab_label_button.set_relief(ReliefStyle::None);
    tab_label_button.connect_clicked(
        clone!( @weak item_notebook, @strong page_container => move | _ | {
            debug!("attempting to close tab");
            item_notebook.remove( &page_container);
        }),
    );

    tab_label.pack_start(&tab_label_label, false, false, 0);
    tab_label.pack_end(&tab_label_button, false, false, 0);
    tab_label.show_all();

    tab_label
}

fn build_category_selector(label: &str) -> (Box, Stack) {
    let image_arrow = Image::from_icon_name(Some("go-next-symbolic"), IconSize::Button);
    let loading_spinner = Spinner::new();
    loading_spinner.start();

    let button_items_content = Box::new(Orientation::Horizontal, 5);
    let label_button_items = Label::new(Some(label));
    let icon_stack = Stack::new();
    icon_stack.add_named(&image_arrow, "ready");
    icon_stack.add_named(&loading_spinner, "loading");
    icon_stack.set_visible_child_name("ready");
    button_items_content.pack_start(&label_button_items, false, false, 5);
    button_items_content.pack_end(&icon_stack, false, false, 5);
    (button_items_content, icon_stack)
}

pub(crate) fn build_rig_container(
    rig: std::boxed::Box<dyn ItemOverview>,
    item_notebook: &Notebook,
    build_image: bool,
) -> (Box, Image) {
    let rig_box = Box::new(Orientation::Vertical, 5);
    let rig_image = Image::from_icon_name(Some("image-loading-symbolic"), IconSize::Button);
    if build_image {
        rig_image.set_hexpand(true);
        rig_image.set_vexpand(true);
        rig_box.add(&rig_image);
    }
    let label_rig = Label::new(Some(rig.get_name()));
    let label_class;
    match rig.get_class() {
        ItemClass::Warframes
        | ItemClass::Secondary
        | ItemClass::Archwing
        | ItemClass::ArchGun
        | ItemClass::ArchMelee
        | ItemClass::Relics => {
            label_class = Label::new(Some(match_item_class_name(*rig.get_class())));
        }
        ItemClass::Melee | ItemClass::Primary | ItemClass::Arcanes | ItemClass::Resources => {
            let source = rig.get_json();
            let melee_type = source["type"].as_str().unwrap_or_default();
            label_class = Label::new(Some(melee_type));
        }
        ItemClass::Mods => {
            let source = rig.get_json();
            let mod_type = source["type"].as_str().unwrap_or_default();
            let mod_owner = source["compatName"].as_str().unwrap_or_default();
            let desc = format!("{} / {}", mod_type, mod_owner);
            label_class = Label::new(Some(&*desc));
        }
    }

    let box_labels = Box::new(Orientation::Vertical, 5);
    box_labels.pack_start(&label_rig, false, false, 5);
    box_labels.pack_end(&label_class, false, false, 5);
    rig_box.add(&box_labels);

    let rig_button = Button::new();
    //let button_label = Label::new(Some("More"));
    //rig_button.add(&button_label);
    rig_button.add(&rig_box);
    match rig.get_class() {
        ItemClass::Warframes => {
            rig_button.connect_clicked(clone!( @weak item_notebook => move |_| {
                on_warframe_more_clicked(&rig, &item_notebook)
            }));
        }
        ItemClass::Primary => {
            rig_button.connect_clicked(clone!( @weak item_notebook => move |_| {
                on_primary_more_clicked(&rig, &item_notebook)
            }));
        }
        ItemClass::Secondary => {
            rig_button.connect_clicked(clone!( @weak item_notebook => move |_| {
                on_primary_more_clicked(&rig, &item_notebook)
            }));
        }
        ItemClass::Archwing => {
            rig_button.connect_clicked(clone!( @weak item_notebook => move |_| {
                on_archwing_more_clicked(&rig, &item_notebook)
            }));
        }
        ItemClass::ArchGun => {
            rig_button.connect_clicked(clone!( @weak item_notebook => move |_| {
                on_primary_more_clicked(&rig, &item_notebook)
            }));
        }
        ItemClass::Melee => {
            rig_button.connect_clicked(clone!( @weak item_notebook => move |_| {
                on_primary_more_clicked(&rig, &item_notebook)
            }));
        }
        ItemClass::Mods => {
            rig_button.connect_clicked(clone!( @weak item_notebook => move |_| {
                on_mod_more_clicked(&rig, &item_notebook)
            }));
        }
        ItemClass::Arcanes => {
            rig_button.connect_clicked(clone!( @weak item_notebook => move |_| {
                on_arcane_more_clicked(&rig, &item_notebook)
            }));
        }
        ItemClass::ArchMelee => {
            rig_button.connect_clicked(clone!( @weak item_notebook => move |_| {
                on_primary_more_clicked(&rig, &item_notebook)
            }));
        }

        ItemClass::Relics => {
            rig_button.connect_clicked(clone!( @weak item_notebook => move |_| {
                on_arcane_more_clicked(&rig, &item_notebook)
            }));
        }
        ItemClass::Resources => {
            rig_button.connect_clicked(clone!(@weak item_notebook => move |_| {
                on_resource_more_clicked(&rig, &item_notebook)
            }));
        }
    }

    let rig_container = Box::new(Orientation::Vertical, 5);
    //rig_container.add(&rig_box);
    rig_container.pack_start(&rig_button, false, false, 5);

    (rig_container, rig_image)
}
