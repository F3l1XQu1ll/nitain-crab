use glib::{clone, PRIORITY_DEFAULT};
use gtk::prelude::*;
use gtk::{Box, Button, Label, Orientation, ScrolledWindowBuilder, Stack};

use json::JsonValue;

use crate::message;
use crate::uplink;
use crate::view::universal::load_universal_view;

use std::thread;

pub fn build_relics_main_section(main_stack: &Stack) -> Button {
    let relics_row = Button::new();
    let box_relics = Box::new(Orientation::Horizontal, 0);
    let label_relics = Label::new(None);
    label_relics.set_label("Relics");
    let label_macro = Label::new(None);
    label_macro.set_label("Macro");

    box_relics.add(&label_relics);
    box_relics.pack_end(&label_macro, false, false, 5);

    relics_row.add(&box_relics);

    relics_row.connect_clicked(clone!(@weak main_stack => move |_| {
        let (tx_relics, rx_relics) = glib::MainContext::channel(PRIORITY_DEFAULT);
        let _loader_relics = thread::spawn(move || {
            let relics_json = uplink::extractor::wiki_vault::scrape_wiki_vault_list();
            //tx_relics.send(json_array);
            let mut relic_names: Vec<String> = Vec::new();
            let mut vaulted_array = Vec::new();
            for member in relics_json.members() {
                let name = member["name"].as_str().unwrap();
                if !relic_names.iter().any(|e| e == name){
                    if !member["vaulted"].as_bool().unwrap(){
                        let new_member = json::object!{
                            name: member["name"].as_str(),
                        };
                        vaulted_array.push(new_member);
                    }
                    relic_names.push(name.to_string());
                }else{
                    continue;
                }
            }

            let mut res_vec = Vec::new();
            let mut sort_vec: Vec<JsonValue> = Vec::new();

            let mut i: i64 = 1;
            for node in vaulted_array.clone() {
                //let name = node["name"].as_str().unwrap().to_string();
                let prices_object = uplink::extractor::market_prices::scrape_market_prices(
                    node["name"].as_str().unwrap().replace(" ", "_").to_lowercase()
                    .as_str()
                );

                if prices_object["price"].as_f64().unwrap() > -1.0 {
                    res_vec.push(prices_object);
                }

                debug!("Loaded item info {} of {}", i, vaulted_array.len());

                i+=1;
            }

            /* Simple Bubble Sort */
            for _i in 0..res_vec.len() {
                for j in 1..res_vec.len() {
                    if res_vec[j-1]["price"].as_f64().unwrap() > res_vec[j]["price"].as_f64().unwrap() {
                        let old_val = res_vec[j-1].clone();
                        res_vec[j-1] = res_vec[j].clone();
                        res_vec[j] = old_val;
                    }
                }
            }

            for item in res_vec.iter().skip(res_vec.len()-6) {
                sort_vec.push(item.clone());
            }

            let json_array = json::JsonValue::Array(sort_vec);

            if let Err(e) = tx_relics.send(json_array) {
                warn!("{}", e.to_string())
            }
        });

        rx_relics.attach(None, move |data| {
            let (sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
            //let data = uplink::get_nexus_data(suggestion["apiUrl"].as_str().unwrap());
            //let data_list = ViewSub::new().unpack(data, sender);
            let data_list = load_universal_view(data, sender); //build_view_warframe(data);
            let data_window = ScrolledWindowBuilder::new().build();

            let main_stack_tmp = main_stack.clone();
            receiver.attach(None, move |msg|{
                match msg {
                    message::NitainMessage::NavBackLast => {
                        main_stack_tmp.set_visible_child_name("main");
                        main_stack_tmp.remove(&main_stack_tmp.get_child_by_name("data_view").unwrap());
                    },
                    _ => {warn!("Caught unexpected message")}
                }

                glib::Continue(true)
            });

            data_window.add(&data_list);
            main_stack.add_named(&data_window, "data_view");
            main_stack.show_all();
            main_stack.set_visible_child_name("data_view");

            glib::Continue(true)
        });
    }));

    relics_row
}
