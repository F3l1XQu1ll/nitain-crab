use std::collections::HashMap;

use gdk_pixbuf::Pixbuf;
use glib::clone;
use gtk::{Align, Box, Image, Orientation, PolicyType, ScrolledWindowBuilder, Stack, StackSidebar, StackSwitcher, StackTransitionType, ToggleButton};
use gtk::prelude::*;

pub fn build_maps() -> Box {
    let maps_stack = Stack::new();
    let maps_switch = StackSwitcher::new();
    maps_switch.set_stack(Some(&maps_stack));
    maps_stack.set_transition_type(StackTransitionType::SlideLeftRight);

    let maps_box = Box::new(Orientation::Vertical, 5);
    let switch_holder = Box::new(Orientation::Horizontal, 0);
    switch_holder.set_center_widget(Some(&maps_switch));
    maps_box.add(&switch_holder);
    maps_box.add(&maps_stack);
    maps_box.set_halign(Align::Fill);
    maps_box.set_hexpand(true);
    maps_box.set_vexpand(true);

    let cetus_base = Stack::new();
    cetus_base.set_homogeneous(false);
    let mut cetus_maps = HashMap::new();
    cetus_maps.insert("fishing", "Fishing");
    cetus_maps.insert("landmark", "Landmarks and Resources");
    cetus_maps.insert("mining", "Mining");

    let fortuna_base = Stack::new();
    let mut fortuna_maps = HashMap::new();
    fortuna_maps.insert("full", "Full Map");

    let necralisk_base = Stack::new();
    let mut necralisk_maps = HashMap::new();
    necralisk_maps.insert("full", "Full Map");

    let cetus_control_box = StackSidebar::new();
    let fortuna_control_box = StackSidebar::new();
    let necralisk_control_box = StackSidebar::new();

    cetus_control_box.set_stack(&cetus_base);
    fortuna_control_box.set_stack(&fortuna_base);
    necralisk_control_box.set_stack(&fortuna_base);

    for map in cetus_maps {
        let path_base = "/com/archyt/Nitain_Crab/maps/cetus/";
        let button = build_map_button(map, path_base);
        cetus_base.add_titled(&button, map.0, map.1);
    }

    for map in fortuna_maps {
        let path_base = "/com/archyt/Nitain_Crab/maps/fortuna/";
        let button = build_map_button(map, path_base);
        fortuna_base.add_titled(&button, map.0, map.1);
    }

    for map in necralisk_maps {
        let path_base = "/com/archyt/Nitain_Crab/maps/necralisk/";
        let button = build_map_button(map, path_base);
        necralisk_base.add_titled(&button, map.0, map.1);
    }

    let cetus_scroll = ScrolledWindowBuilder::new()
        .propagate_natural_width(true)
        .hscrollbar_policy(PolicyType::Automatic)
        .build();
    cetus_scroll.add(&cetus_base);
    let fortuna_scroll = ScrolledWindowBuilder::new()
        .propagate_natural_width(true)
        .hscrollbar_policy(PolicyType::Automatic)
        .build();
    fortuna_scroll.add(&fortuna_base);
    let necralisk_scroll = ScrolledWindowBuilder::new()
        .propagate_natural_width(true)
        .hscrollbar_policy(PolicyType::Automatic)
        .build();
    necralisk_scroll.add(&necralisk_base);

    let cetus_composite_box = Box::new(Orientation::Horizontal, 5);
    cetus_composite_box.add(&cetus_control_box);
    cetus_composite_box.add(&cetus_scroll);

    let fortuna_composite_box = Box::new(Orientation::Horizontal, 5);
    fortuna_composite_box.add(&fortuna_control_box);
    fortuna_composite_box.add(&fortuna_scroll);

    let necralisk_composite_box = Box::new(Orientation::Horizontal, 5);
    necralisk_composite_box.add(&necralisk_control_box);
    necralisk_composite_box.add(&necralisk_scroll);

    maps_stack.add_titled(&cetus_composite_box, "cetus", "Cetus");
    maps_stack.add_titled(&fortuna_composite_box, "fortuna", "Fortuna");
    maps_stack.add_titled(&necralisk_composite_box, "necralisk", "Necralisk");

    let maps_holder = Box::new(Orientation::Horizontal, 0);
    maps_holder.set_center_widget(Some(&maps_box));
    maps_holder.set_halign(Align::Fill);
    maps_holder.set_valign(Align::Fill);

    maps_holder
}

fn build_map_button(map: (&str, &str), path_base: &str) -> Box {
    let res_name = map.0;
    let res_path = format!("{}{}", path_base, res_name);
    let map_pxb_large = Pixbuf::from_resource_at_scale(res_path.as_str(), 512 * 2, 512 * 2, true).unwrap();
    let map_pxb_smal = Pixbuf::from_resource_at_scale(res_path.as_str(), 512, 512, true).unwrap();
    let map_img = Image::from_pixbuf(Some(&map_pxb_smal));
    let img_button = ToggleButton::new();
    img_button.add(&map_img);
    img_button.set_active(false);
    img_button.connect_clicked(clone!(@weak map_img => move |button| {
        if button.get_active() {
            map_img.set_from_pixbuf(Some(&map_pxb_large));
        } else {
            map_img.set_from_pixbuf(Some(&map_pxb_smal));
        }
    }));

    map_img.set_vexpand(true);

    let map_holder = Box::new(Orientation::Vertical, 0);
    map_holder.set_center_widget(Some(&img_button));
    map_holder
}