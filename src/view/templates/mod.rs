pub(crate) mod warframe_base_stats;
pub(crate) mod value;
pub(crate) mod warframe_abilities;
pub(crate) mod weapon_base_stats;
pub(crate) mod acquisition;