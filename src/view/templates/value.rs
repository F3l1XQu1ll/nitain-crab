use gtk::prelude::*;
use gtk::{Box, Button, Entry, IconSize, Image, Label, Orientation, Popover};

pub fn build_value_ui(desc: &str, value: &str, info: Option<String>) -> Box {
    let box_value = Box::new(Orientation::Horizontal, 5);
    let label_desc = Label::new(Some(desc));
    let entry_value = Entry::new();
    entry_value.set_text(value);
    entry_value.set_editable(false);
    entry_value.set_property_width_request(20);

    box_value.pack_start(&label_desc, true, true, 0);

    if value.len() as i32 > entry_value.get_property_width_request() {
        let value_copy = value.to_string();
        let button_more = Button::new();
        button_more.set_label("Read");
        button_more.connect_clicked(move |button| {
            let popover = Popover::new(Some(button));
            let label_info = Label::new(Some(value_copy.as_str()));
            label_info.set_max_width_chars(40);
            label_info.set_line_wrap(true);
            popover.add(&label_info);
            popover.show_all();
        });

        box_value.pack_end(&button_more, false, false, 0);
    }

    if let Some(i) = info {
        let button_info = Button::new();
        let info_icon =
            Image::from_icon_name(Some("dialog-information-symbolic"), IconSize::Button);
        button_info.add(&info_icon);
        button_info.connect_clicked(move |button| {
            let popover = Popover::new(Some(button));
            //let scrolled_window = ScrolledWindowBuilder::new().hscrollbar_policy(PolicyType::Never).build();
            let label_info = Label::new(Some(i.as_str()));
            label_info.set_max_width_chars(40);
            label_info.set_line_wrap(true);
            //scrolled_window.add(&label_info);
            popover.add(&label_info);
            popover.show_all();
        });

        box_value.pack_end(&button_info, false, false, 0);
    }

    box_value.pack_end(&entry_value, false, false, 0);

    box_value.set_hexpand(true);

    box_value
}
