use gtk::{Box, Grid, Label, Orientation, PositionType, ScrolledWindowBuilder, Stack, StackSidebar, StackSwitcher, TextTag, TextView, WrapMode};
use gtk::prelude::*;
use pango::{Attribute, AttrList};

use super::super::json::JsonValue;

pub fn ui_template_warframe_abilities(data: Vec<JsonValue>) -> Grid {
    let ability_selector = StackSidebar::new();
    let ability_stack = Stack::new();
    ability_stack.set_homogeneous(false);
    ability_selector.set_stack(&ability_stack);

    let root = Grid::new();

    for datum in data {
        let name = datum["name"].as_str().unwrap();
        let stack_values = Stack::new();
        stack_values.set_homogeneous(false);
        let label_name = Label::new(Some(name));
        let attributes = AttrList::new();
        attributes.insert(Attribute::new_scale(1.5).unwrap());
        label_name.set_attributes(Some(&attributes));
        let stack_switcher_values = StackSwitcher::new();
        stack_switcher_values.set_stack(Some(&stack_values));
        let switcher_holder = Box::new(Orientation::Horizontal, 0);
        switcher_holder.set_center_widget(Some(&stack_switcher_values));

        for entry in datum.entries() {
            if entry.0 == "name" {
                continue;
            }
            let title = entry.0;
            let value = entry.1.as_str().unwrap();
            let attributes_value = AttrList::new();

            let formated_val = String::from(value);//.replace(". ", ".\n");

            let formatted_label = TextView::new();
            let formatted_label_buffer = formatted_label.get_buffer().unwrap();
            formatted_label.set_wrap_mode(WrapMode::WordChar);
            formatted_label.set_property_margin(10);
            formatted_label.set_vexpand(true);
            formatted_label.set_property_width_request(100);
            formatted_label.set_editable(false);
            formatted_label.set_pixels_above_lines(10);

            let mut insert_iter = formatted_label_buffer.get_iter_at_offset(0);

            for line in formated_val.lines() {
                let trimmed_line = line.trim_start();
                let indent_depth = line.chars().count() - trimmed_line.chars().count();
                let new_indent = indent_depth / 4;

                let mut new_line = String::new();

                let mut indent = String::new();
                for _i in 0..new_indent {
                    indent.push('\t');
                }

                new_line.push_str(indent.as_str());
                new_line.push_str(trimmed_line);

                new_line.push('\n');
                formatted_label_buffer.insert(&mut insert_iter, new_line.as_str());
            }

            for i in 0..formatted_label_buffer.get_line_count() - 1 {
                let mut line = formatted_label_buffer.get_iter_at_line(i);

                let mut indent_depth = 0;
                while line.get_char() != None && line.get_char().unwrap() == '\t' {
                    let mut delete_end = line.clone();
                    delete_end.forward_chars(1);
                    formatted_label_buffer.delete(&mut line, &mut delete_end);

                    indent_depth += 1;
                }

                let indent_tag = TextTag::new(None);
                indent_tag.set_property_left_margin(indent_depth * 40);
                indent_tag.set_property_scale(1.2);
                formatted_label_buffer.get_tag_table().unwrap().add(&indent_tag);

                let start = formatted_label_buffer.get_iter_at_line(i);

                let mut end;
                if formatted_label_buffer.get_line_count() - 1 == i {
                    end = formatted_label_buffer.get_end_iter();
                } else {
                    end = formatted_label_buffer.get_iter_at_line(i + 1);
                    end.backward_char();
                }

                formatted_label_buffer.apply_tag(&indent_tag, &start, &end);
            }

            let mut iter = formatted_label_buffer.get_iter_at_line(0);
            while iter.get_char() != None {
                iter.forward_line();
            }

            let text_scroll = ScrolledWindowBuilder::new().build();
            text_scroll.add(&formatted_label);

            attributes_value.insert(Attribute::new_scale(1.1).unwrap());
            stack_values.add_titled(&text_scroll, title, title);
        }

        let v_holder = Box::new(Orientation::Vertical, 5);
        v_holder.add(&label_name);
        v_holder.add(&switcher_holder);
        v_holder.add(&stack_values);
        v_holder.set_hexpand(true);
        v_holder.set_vexpand(true);

        ability_stack.add_titled(&v_holder, name, name);
    }

    root.attach(&ability_selector, 0, 1, 1, 1);
    root.attach_next_to(&ability_stack, Some(&ability_selector), PositionType::Right, 1, 1);

    root.set_vexpand(true);
    root.set_hexpand(true);
    root.set_row_spacing(5);

    root
}