use gdk_pixbuf::Pixbuf;
use glib::value::GetError;
use glib::Type;
use gtk::prelude::*;
use gtk::{
    Box, CellRendererPixbuf, CellRendererText, ListStore, Orientation, PolicyType,
    ScrolledWindowBuilder, Stack, TreeView, TreeViewColumn,
};

use crate::rigs::components::{Component, UniversalComponent};
use crate::rigs::item::ItemOverview;
use crate::uplink::images::load_git_image;
use crate::view::image_foundry::image_from_stream;
use crate::view::modules::drops_list::drops_from_json;

pub fn template_parts(rig: &std::boxed::Box<dyn ItemOverview>) -> Box {
    let source = rig.get_json();
    let parts = source["components"].clone();
    let mut components = Vec::new();

    let root = Box::new(Orientation::Vertical, 5);

    if !parts.is_null() {
        for part in parts.members() {
            let unique_name = part["uniqueName"].as_str().unwrap();
            let name = part["name"].as_str().unwrap();
            let desc = part["description"].as_str().unwrap();
            let count = part["itemCount"].as_u32().unwrap();
            let image = part["imageName"].as_str().unwrap();
            let tradable = part["tradable"].as_bool().unwrap();
            let drops_src = part["drops"].clone();
            let drops;
            if drops_src.is_empty() {
                drops = None;
            } else {
                drops = Some(drops_src);
            }
            let part_rig =
                UniversalComponent::new(unique_name, name, desc, count, image, tradable, drops);
            components.push(part_rig);
        }
        let parts_list = TreeView::new();
        let column_name = TreeViewColumn::new();
        let column_count = TreeViewColumn::new();
        let column_tradable = TreeViewColumn::new();
        let column_id = TreeViewColumn::new();

        let renderer_name = CellRendererText::new();
        let renderer_count = CellRendererText::new();
        let renderer_tradable = CellRendererText::new();
        let renderer_icon = CellRendererPixbuf::new();
        let renderer_id = CellRendererText::new();

        renderer_id.set_visible(false);

        column_name.pack_start(&renderer_icon, true);
        column_name.pack_start(&renderer_name, true);
        column_count.pack_start(&renderer_count, true);
        column_tradable.pack_start(&renderer_tradable, true);
        column_id.pack_start(&renderer_id, true);

        column_name.add_attribute(&renderer_name, "text", 1);
        column_name.add_attribute(&renderer_icon, "pixbuf", 0);
        column_count.add_attribute(&renderer_count, "text", 2);
        column_tradable.add_attribute(&renderer_tradable, "text", 3);
        column_id.add_attribute(&renderer_id, "text", 4);

        column_name.set_title("Name");
        column_count.set_title("Count");
        column_tradable.set_title("Tradable");

        parts_list.append_column(&column_name);
        parts_list.append_column(&column_count);
        parts_list.append_column(&column_tradable);
        parts_list.append_column(&column_id);

        let model = ListStore::new(&[
            Pixbuf::static_type(),
            Type::String,
            Type::U32,
            Type::Bool,
            Type::String,
        ]);
        parts_list.set_model(Some(&model));

        let drops_stack = Stack::new();
        let drops_placeholder = Box::new(Orientation::Horizontal, 0);
        drops_stack.add_named(&drops_placeholder, "placeholder");
        drops_stack.set_visible_child_name("placeholder");
        drops_stack.set_homogeneous(false);
        drops_stack.set_property_expand(true);

        //let tmp_model = model.clone();
        let tmp_stack = drops_stack.clone();
        let selection = parts_list.get_selection();
        selection.connect_changed(move |selection| {
            if let Some(selection) = selection.get_selected() {
                let part_id = selection.0.get_value(&selection.1, 4);
                let id_str: Result<Option<String>, GetError> = part_id.get();
                if let Ok(Some(id)) = id_str {
                    if let Some(child) = tmp_stack.get_child_by_name(id.as_str()) {
                        tmp_stack.set_visible_child(&child);
                    } else {
                        tmp_stack.set_visible_child_name("placeholder");
                    }
                }
            } else {
                tmp_stack.set_visible_child_name("placeholder");
            }
        });

        for component in components {
            let iter = model.append();
            let image_src = component.get_image();
            let image_bytes = load_git_image(image_src);
            let icon = image_from_stream(image_bytes, 24 * 2, 24 * 2);
            if let Ok(icon) = icon {
                if let Some(pixbuf) = icon.get_pixbuf() {
                    model.set(&iter, &[0_u32], &[&Some(pixbuf)])
                }
            }

            if let Some(drops) = component.get_drops() {
                let drop_table = drops_from_json(drops);
                let drop_scroll = ScrolledWindowBuilder::new()
                    .hscrollbar_policy(PolicyType::Never)
                    .build();
                drop_scroll.add(&drop_table);
                drops_stack.add_named(&drop_scroll, component.get_unique_name());
            }

            model.set_value(&iter, 4, &component.get_unique_name().to_value());
            model.set_value(&iter, 1, &component.get_name().to_value());
            model.set_value(&iter, 2, &component.get_count().to_value());
            model.set_value(&iter, 3, &component.get_tradable().to_value());
        }

        let parts_holder = Box::new(Orientation::Horizontal, 0);
        parts_holder.set_center_widget(Some(&parts_list));
        root.pack_start(&parts_holder, false, false, 5);
        root.pack_start(&drops_stack, true, true, 5);

        if let Some(iter) = model.get_iter_first() {
            selection.select_iter(&iter);
        }
    }

    let h_holder = Box::new(Orientation::Horizontal, 0);
    h_holder.set_center_widget(Some(&root));
    h_holder
}
