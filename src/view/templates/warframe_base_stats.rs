use gtk::{Box, ContainerExt, Grid, GridExt, Orientation, WidgetExt};
use gtk::prelude::*;

use crate::view::templates::value::build_value_ui;

use super::super::json::JsonValue;

pub fn template_base_stats_warframe(data: JsonValue) -> JsonValue {
    let hp = data["health"].as_i32().unwrap() as f64;
    let shield = data["shield"].as_i32().unwrap() as f64;
    let armor = data["armor"].as_i32().unwrap() as f64;
    let energy = data["energy"].as_i32().unwrap() as f64;

    let damage_reduction = armor / (armor + 300.0); //see https://warframe.fandom.com/wiki/Armor
    let ehp = hp / (1.0 - damage_reduction);

    let warframe_base_stats = json::object! {
        health: hp,
        shield: shield,
        armor: armor,
        damage_reduction: damage_reduction,
        effective_hp: ehp,
        energy: energy,
    };

    warframe_base_stats
}

pub fn ui_template_base_stats_warframe(data: JsonValue) -> Grid {
    let v_holder = Box::new(Orientation::Vertical, 5);
    let h_holder = Box::new(Orientation::Horizontal, 0);
    let root = Grid::new();


    let val_health = data["health"].as_f64().unwrap();
    let val_shield = data["shield"].as_f64().unwrap();
    let val_armor = data["armor"].as_f64().unwrap();
    let val_dmg_red = data["damage_reduction"].as_f64().unwrap();
    let val_ehp = data["effective_hp"].as_f64().unwrap();
    let val_energy = data["energy"].as_f64().unwrap();

    let hp_info = String::from("Total amount of Damage a Warframe can take before going into Bleedout.");
    let shield_info = String::from("Shields absorb Damage, except Toxin.\nDepleted Shields grant a moment of invulnerability.\nDamage interrupts Recharge.");
    let armor_info = String::from("Reduces damage to Health by a certain percentage.\nShields are not affected by Armor.");
    let dmg_red_info = String::from("Damage Reduction applied by Armor.\nDamage Reduction = Armor / (Armor + 300)");
    let ehp_info = String::from("Health in terms of survivability supplied by Damage Reduction.\nEffective Health = Health / (1 - Damage Reduction)");
    let energy_info = String::from("Used to cast Warframe Abilities and can be replenished during missions.");

    let hp = build_value_ui("Health (Max Rank)", val_health.to_string().as_str(), Some(hp_info));
    let shield = build_value_ui("Shield (Max Rank)", val_shield.to_string().as_str(), Some(shield_info));
    let armor = build_value_ui("Armor", val_armor.to_string().as_str(), Some(armor_info));
    let dmg_red = build_value_ui("Damage Reduction", val_dmg_red.to_string().as_str(), Some(dmg_red_info));
    let ehp = build_value_ui("Effective Health", val_ehp.to_string().as_str(), Some(ehp_info));
    let energy = build_value_ui("Energy (Max Rank)", val_energy.to_string().as_str(), Some(energy_info));

    v_holder.add(&hp);
    v_holder.add(&shield);
    v_holder.add(&armor);
    v_holder.add(&dmg_red);
    v_holder.add(&ehp);
    v_holder.add(&energy);

    v_holder.set_hexpand(true);

    h_holder.set_center_widget(Some(&v_holder));

    root.attach(&h_holder, 0, 1, 1, 1);

    root.set_vexpand(true);
    root.set_hexpand(true);
    root.set_row_spacing(5);

    root
}