use gtk::{Box, Grid, Label, Orientation, ScrolledWindowBuilder};
use gtk::prelude::*;

use crate::view::templates::value::build_value_ui;

use super::super::json::JsonValue;

pub fn ui_template_base_stats_weapon(data: JsonValue) -> Grid {
    let v_holder = Box::new(Orientation::Vertical, 5);
    let h_holder = Box::new(Orientation::Horizontal, 0);
    let root = Grid::new();

    //let weapon_title = data["title"].as_str();
    //let title_box = Box::new(Orientation::Horizontal, 0);
    //let title_label = Label::new(weapon_title);
    //title_box.set_center_widget(Some(&title_label));

    let sections_box = Box::new(Orientation::Vertical, 5);
    let sections = data["sections"].clone();
    for section in sections.members() {
        let name = section["name"].as_str();
        let name_box = Box::new(Orientation::Horizontal, 0);
        let name_label = Label::new(name);
        name_box.set_center_widget(Some(&name_label));

        let attr_box = Box::new(Orientation::Vertical, 5);
        for member in section["attrs"].members() {
            //let name = member["name"].as_str();
            let title = member["title"].as_str();
            let value = member["value"].as_str();
            let attr_label = build_value_ui(title.unwrap(), value.unwrap(), None);
            attr_box.add(&attr_label);
        }
        sections_box.add(&name_box);
        sections_box.add(&attr_box);
    }


    //v_holder.add(&title_box);
    v_holder.add(&sections_box);


    v_holder.set_hexpand(true);
    v_holder.set_vexpand(true);

    h_holder.set_center_widget(Some(&v_holder));

    let sections_scroll = ScrolledWindowBuilder::new().build();
    sections_scroll.add(&h_holder);

    root.attach(&sections_scroll, 0, 1, 1, 1);

    root.set_vexpand(true);
    root.set_hexpand(true);
    root.set_row_spacing(5);

    root
}