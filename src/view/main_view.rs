use std::{collections::HashMap, thread};

use glib::{clone, MainContext, PRIORITY_DEFAULT};
use gtk::prelude::*;
use gtk::{
    Box, Button, Grid, Label, Orientation, Popover, PositionType, ScrolledWindowBuilder,
    SearchEntry, Stack, StackTransitionType,
};
use json::JsonValue;
use unicode_segmentation::UnicodeSegmentation;

use crate::message;
use crate::message::NitainMessage;
use crate::uplink;
use crate::uplink::extractor::warframe::{scrape_warframe_abilities, scrape_warframe_base};
use crate::uplink::extractor::weapon::scrape_weapon_info;
use crate::uplink::images::load_nexus_image;
use crate::view::relics::build_relics_main_section;
use crate::view::templates::warframe_abilities::ui_template_warframe_abilities;
use crate::view::templates::warframe_base_stats::{
    template_base_stats_warframe, ui_template_base_stats_warframe,
};
use crate::view::templates::weapon_base_stats::ui_template_base_stats_weapon;
use crate::view::{image_foundry, universal::load_universal_view};

pub fn build_main() -> Stack {
    let search = SearchEntry::new();

    let main_stack = Stack::new();
    main_stack.set_transition_type(StackTransitionType::SlideLeft);
    let main_list = Box::new(Orientation::Vertical, 5);
    let main_box = Box::new(Orientation::Vertical, 10);
    let scrolled_window = ScrolledWindowBuilder::new().build();

    search.connect_search_changed(clone!(@weak main_list, @weak main_stack => move|search|{
        on_search_changed(search, main_stack, main_list);
    }));

    let main_holder = Box::new(Orientation::Horizontal, 0);
    main_holder.set_center_widget(Option::from(&main_list));

    scrolled_window.add(&main_holder);
    scrolled_window.set_vexpand(true);
    scrolled_window.set_hexpand(true);

    let search_holder = Box::new(Orientation::Horizontal, 0);
    search_holder.set_center_widget(Some(&search));

    main_box.add(&search_holder);
    main_box.add(&scrolled_window);
    main_box.set_hexpand(true);
    main_box.set_vexpand(true);

    main_stack.add_named(&main_box, "main");
    main_stack
}

fn display_data(receiver: glib::Receiver<NitainMessage>, main_stack: Stack, data_list: Grid) {
    let data_window = ScrolledWindowBuilder::new().build();

    let main_stack_tmp = main_stack.clone();
    receiver.attach(None, move |msg| {
        match msg {
            message::NitainMessage::NavBackLast => {
                main_stack_tmp.set_visible_child_name("main");
                main_stack_tmp.remove(&main_stack_tmp.get_child_by_name("data_view").unwrap());
            }
            _ => {
                warn!("Caught unexpected message")
            }
        }

        glib::Continue(true)
    });

    data_window.add(&data_list);
    main_stack.add_named(&data_window, "data_view");
    main_stack.show_all();
    main_stack.set_visible_child_name("data_view");
}

fn on_search_changed(search: &SearchEntry, main_stack: Stack, main_list: Box) {
    let query = search.get_text();
    if query.as_str().starts_with('!') {
        if let "!relics" = query.as_str() {
            let relics_row = build_relics_main_section(&main_stack);
            main_list.add(&relics_row);
            main_stack.show_all();
        }
    } else if query.graphemes(true).count() >= 3 {
        //let suggestions = uplink::get_nexus_completion(query.as_str(), 5);

        for old_sugggestions in main_list.get_children() {
            main_list.remove(&old_sugggestions);
        }

        let (tx, rx) = glib::MainContext::channel(PRIORITY_DEFAULT);
        let _loader_suggestions = thread::spawn(move || {
            let suggestions = uplink::get_nexus_completion(query.as_str(), 5);
            if let Err(e) = tx.send(suggestions) {
                warn!("{}", e.to_string())
            }
        });

        rx.attach(None, move |suggestions| {
            /*
            Fix duplicated and old entries reappearing
             */
            for old_sugggestions in main_list.get_children() {
                main_list.remove(&old_sugggestions);
            }

            let mut item_categories: HashMap<String, gtk::Box> = HashMap::new();
            for suggestion in suggestions {
                let row = Button::new();
                let content_grid = Grid::new();
                content_grid.set_row_spacing(10);
                content_grid.set_column_spacing(2);
                content_grid.set_row_homogeneous(true);
                let row_box = Box::new(Orientation::Horizontal, 10);

                let value_category: String = suggestion["category"].as_str().unwrap().to_string();
                match value_category.as_str() {
                    "Glyphs" => { continue; }
                    "Skins" => { continue; }
                    _ => {}
                }

                let value_image = suggestion["imgUrl"].as_str().unwrap().to_string();
                let name_label = Label::new(suggestion["name"].as_str());
                let category_label = Label::new(Option::from(value_category.as_str()));

                if !(item_categories.contains_key(value_category.as_str())) {
                    item_categories.insert(value_category.clone(), Box::new(Orientation::Vertical, 0));
                }

                item_categories.get(&value_category.clone()).unwrap().add(&row);

                //let go_button = Button::new();
                //let image = Image::from_icon_name(Option::from("go-next"), IconSize::Button);
                //go_button.set_image(Option::from(&image));
                //go_button.set_always_show_image(true);

                row.connect_clicked(clone!(@weak main_stack, @strong value_category, @strong suggestion => move |row| {
                    on_row_clicked(row, value_category.clone(), suggestion.clone(), main_stack);
                }));

                let (tx_icon_bytes, rx_icon_bytes) = MainContext::channel(PRIORITY_DEFAULT);
                let _icon_loader = thread::spawn(move || {
                    let icon_bytes = load_nexus_image(&value_image);
                    if let Err(e) = tx_icon_bytes.send(icon_bytes) {
                        warn!("{}", e.to_string())
                    }
                });

                rx_icon_bytes.attach(None, move |icon_bytes| {
                    content_grid.attach(&name_label, 0, 0, 1, 1);
                    content_grid.attach(&Label::new(Option::from(" - ")), 1, 0, 1, 1);
                    content_grid.attach(&category_label, 2, 0, 1, 1);
                    content_grid.attach_next_to(
                        &image_foundry::image_from_stream(icon_bytes, 42, 42).unwrap(),
                        Option::from(&name_label),
                        PositionType::Left, 1, 1,
                    );
                    row_box.pack_start(&content_grid, true, false, 0);
                    //row_box.pack_end(&go_button, false, true, 0);
                    row.add(&row_box);
                    row.show_all();
                    glib::Continue(false)
                });
            }

            for category in item_categories {
                main_list.add(&category.1);
            }

            main_stack.show_all();
            Continue(true)
        });
    }
}

fn on_row_clicked(row: &Button, value_category: String, suggestion: JsonValue, main_stack: Stack) {
    let popover = Popover::new(Option::from(row));
    let box_popover = gtk::Box::new(Orientation::Vertical, 10);

    match value_category.as_str() {
        "Warframes" => {
            //let api_url_tmp = String::from(suggestion["apiUrl"].as_str().unwrap());
            let name_tmp = String::from(suggestion["name"].as_str().unwrap());
            let button_template_base_stats = Button::with_label("Base Stats");
            button_template_base_stats.connect_clicked(clone!(@weak main_stack, @strong name_tmp, @strong value_category => move |_|{
                on_warframe_template_base_stats_clicked(name_tmp.clone(), value_category.clone(), main_stack);
            }));

            let button_wiki_abilities = Button::with_label("Wiki Abilities");
            button_wiki_abilities.connect_clicked(clone!(@weak main_stack, @strong name_tmp, @strong value_category => move |_|{
                on_warframe_template_abilities_clicked(name_tmp.clone(), value_category.clone(), main_stack);
            }));

            box_popover.add(&button_template_base_stats);
            box_popover.add(&button_wiki_abilities);
        }
        "Primary" | "Melee" | "Secondary" => {
            let name_tmp = String::from(suggestion["name"].as_str().unwrap());
            let button_template_base_stats = Button::with_label("Base Stats");
            button_template_base_stats.connect_clicked(clone!(@weak main_stack, @strong name_tmp, @strong value_category => move |_|{
                on_weapon_template_base_stats_clicked(name_tmp.clone(), value_category.clone(), main_stack);
            }));

            box_popover.add(&button_template_base_stats);
        }
        _ => {}
    }

    let button_load_all = Button::with_label("Load All");
    //let api_url_tmp = suggestion["apiUrl"].clone();
    let name_tmp = String::from(suggestion["name"].as_str().unwrap());
    button_load_all.connect_clicked(clone!(@weak main_stack, @strong value_category => move |_|{
        on_item_load_all_clicked(name_tmp.clone(), value_category.clone(), main_stack);
    }));

    box_popover.add(&button_load_all);
    popover.add(&box_popover);
    popover.show_all();
}

fn on_warframe_template_base_stats_clicked(
    name_tmp: String,
    value_category: String,
    main_stack: Stack,
) {
    let (_sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
    //let data = uplink::get_nexus_data(api_url_tmp.as_str());
    let data = uplink::get_warframe_hub_data(&name_tmp, &value_category);
    let wiki_url = data["wikiaUrl"].as_str().unwrap();
    let scraped_data = scrape_warframe_base(wiki_url, name_tmp.ends_with("Prime"));
    let templated_data = template_base_stats_warframe(scraped_data);

    let data_list = ui_template_base_stats_warframe(templated_data);
    display_data(receiver, main_stack, data_list);
}

fn on_warframe_template_abilities_clicked(
    name_tmp: String,
    value_category: String,
    main_stack: Stack,
) {
    let (_sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
    //let data = uplink::get_nexus_data(api_url_tmp.as_str());
    let data = uplink::get_warframe_hub_data(&name_tmp, &value_category);

    let abilities = data["abilities"].members();
    let mut ability_names = Vec::new();
    for ability in abilities {
        ability_names.push(String::from(ability["name"].as_str().unwrap()));
    }

    let scraped_abilites = scrape_warframe_abilities(ability_names);
    let ui = ui_template_warframe_abilities(scraped_abilites);
    display_data(receiver, main_stack, ui);
}

fn on_weapon_template_base_stats_clicked(
    name_tmp: String,
    value_category: String,
    main_stack: Stack,
) {
    let (_sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
    //let data = uplink::get_nexus_data(api_url_tmp.as_str());
    let data = uplink::get_warframe_hub_data(&name_tmp, &value_category);

    let item_info;
    if data.is_array() {
        item_info = data[0].clone();
    } else {
        item_info = data;
    }

    //let templated_info = template_base_stats_weapon(item_info);
    //let ui = ui_template_base_stats_weapon(templated_info["stats"].clone(), sender);
    let wiki_url = item_info["wikiaUrl"].as_str().unwrap();
    let scraped_data = scrape_weapon_info(wiki_url);
    let ui = ui_template_base_stats_weapon(scraped_data);
    display_data(receiver, main_stack, ui);
}

fn on_item_load_all_clicked(name_tmp: String, value_category: String, main_stack: Stack) {
    let (sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
    //let data = uplink::get_nexus_data(api_url_tmp.as_str().unwrap());
    let data = uplink::get_warframe_hub_data(&name_tmp, &value_category);
    //let data_list = ViewSub::new().unpack(data, sender);
    let data_list = load_universal_view(data, sender); //build_view_warframe(data);
    display_data(receiver, main_stack, data_list);
}
