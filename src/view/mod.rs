extern crate cairo;
extern crate gio;
extern crate gtk;
extern crate json;

mod image_foundry;
pub(crate) mod main_view;
pub(crate) mod maps_rework;
pub(crate) mod modules;
pub(crate) mod relics;
pub(crate) mod templates;
mod universal;
pub(crate) mod world;
