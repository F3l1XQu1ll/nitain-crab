use bytes::Bytes;
use gtk::{IconSize, Image};

pub fn image_from_stream(image_bytes: Bytes, width: u32, height: u32) -> Result<Image, Image> {
    let memory_stream = gio::MemoryInputStream::from_bytes(&glib::Bytes::from(&image_bytes));
    let pixbuf = gdk_pixbuf::Pixbuf::from_stream_at_scale(&memory_stream, width as i32, height as i32, true, Option::from(&gio::Cancellable::new()));
    match pixbuf {
        Ok(pixbuf) => {
            Ok(gtk::Image::from_pixbuf(Option::from(&pixbuf)))
        }
        Err(error) => {
            warn!("Failed to load Pixbuf from Stream {}", error);
            Err(gtk::Image::from_icon_name(Option::from("image-x-generic-symbolic"), IconSize::Button))
        }
    }
}