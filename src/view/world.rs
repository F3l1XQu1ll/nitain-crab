use gtk::prelude::*;

use crate::view::modules::worldstate::fissures::FissuresUi;
use crate::view::modules::worldstate::news_list::NewsList;
use crate::view::modules::worldstate::nightwave_view::NightwaveUi;
use crate::view::modules::worldstate::timers::TimersUi;
use glib::clone;
use gtk::{Box, Button, Label, Orientation};
use libhandy::{CarouselExt, CarouselIndicatorLinesExt, Clamp};

pub fn build_world() -> gtk::Box {
    let world_box = Box::new(Orientation::Vertical, 5);

    let world_view = libhandy::Carousel::new();

    debug!("");
    let world_switch = crate::utils::generate_hdy_carousel_indicator_lines();
    world_switch.set_carousel(Some(&world_view));

    //let world_stack = Stack::new();
    //world_stack.set_transition_type(StackTransitionType::SlideLeftRight);

    let wip_label = Label::new(Option::from("Work in progress :)"));

    let wip_holder = Clamp::new();
    let wip_layout = gtk::Box::new(Orientation::Vertical, 5);
    wip_layout.add(&wip_label);
    wip_holder.set_property_expand(true);
    wip_holder.add(&wip_layout);
    world_view.insert(&wip_holder, 0);

    let (nav_back_sender, nav_back_receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

    let news_list = NewsList::new(nav_back_sender.clone());
    let nightwave = NightwaveUi::new(nav_back_sender.clone());
    let timers = TimersUi::new(nav_back_sender.clone());
    let fissures = FissuresUi::new(nav_back_sender);

    let tmp_view = world_view.clone();
    let tmp_layout = wip_holder;
    nav_back_receiver.attach(None, move |_| {
        tmp_view.scroll_to(&tmp_layout);
        Continue(true)
    });

    let news_view = news_list.load();
    let nightwave_view = nightwave.load();
    let timers_view = timers.load();
    let fissures_view = fissures.load();
    world_view.insert(&news_view, 1);
    world_view.insert(&nightwave_view, 2);
    world_view.insert(&timers_view, 3);
    world_view.insert(&fissures_view, 4);

    let button_news = Button::with_label("News");
    button_news.connect_clicked(clone!(@weak world_view => move |_| {
        world_view.scroll_to(&news_view);
    }));
    let button_nightwave = Button::with_label("Nightwave");
    button_nightwave.connect_clicked(clone!(@weak world_view => move |_| {
        world_view.scroll_to(&nightwave_view);
    }));
    let button_timers = Button::with_label("Timers");
    button_timers.connect_clicked(clone!(@weak world_view => move |_| {
        world_view.scroll_to(&timers_view);
    }));
    let button_fissures = Button::with_label("Fissures");
    button_fissures.connect_clicked(clone!(@weak world_view => move |_| {
        world_view.scroll_to(&fissures_view);
    }));

    wip_layout.add(&button_news);
    wip_layout.add(&button_nightwave);
    wip_layout.add(&button_timers);
    wip_layout.add(&button_fissures);

    world_box.add(&world_view);
    world_box.add(&world_switch);

    world_box
}
