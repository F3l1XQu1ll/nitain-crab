use super::json::JsonValue;
use gtk::{Stack, Entry, EntryExt, EditableExt, Button, ButtonExt, Image, IconSize, ContainerExt,
          Clipboard, WidgetExt, Box, Orientation, StackExt, StackSidebar, StackSidebarExt, BoxExt,
          Grid, GridExt, ScrolledWindowBuilder, Label, LabelExt, ScrolledWindowExt};
use glib::{clone, Sender};
use crate::message::NitainMessage;

pub fn load_universal_view(data: JsonValue, sender: Sender<NitainMessage>) -> Grid {
    /*let main_stack = Stack::new();
    main_stack.set_homogeneous(false);
    let stack_sidebar_main = StackSidebar::new();
    stack_sidebar_main.set_stack(&main_stack);
    stack_sidebar_main.set_vexpand(true);

    for entry in data.entries() {
        debug!("{} - {}", entry.0, entry.1);
        if entry.1.is_string() || entry.1.is_number() {
            let value_view = build_value_view(entry.1);
            main_stack.add_titled(&value_view, entry.0, entry.0.to_string().to_uppercase().as_str());
        } else if entry.1.is_array() {
            let values_box = build_values_box(entry.1);
            main_stack.add_titled(&values_box, entry.0, entry.0.to_string().to_uppercase().as_str());
        } else if entry.1.is_object() {
            let object_box = build_object_box(entry.1);
            main_stack.add_titled(&object_box, entry.0, entry.0.to_string().to_uppercase().as_str());
        }
    }*/

    let button_nav_back = Button::new();
    button_nav_back.set_label("Back");
    button_nav_back.connect_clicked(move |_| {
        if let Err(e) = sender.send(NitainMessage::NavBackLast) {
            warn!("{}", e.to_string())
        }
    });

    let main_grid = Grid::new();

    main_grid.attach(&button_nav_back, 0, 0, 1, 1);
    //main_grid.attach_next_to(&stack_sidebar_main, Option::from(&button_nav_back),
    //                         PositionType::Bottom, 1, 1);
    //main_grid.attach_next_to(&main_stack, Option::from(&stack_sidebar_main),
    //                         PositionType::Right, 1, 1);

    if data.is_object() {
        let object_view = build_object_box(&data);
        main_grid.attach(&object_view, 0, 1, 1, 1);
    } else if data.is_array() {
        let array_view = build_values_box(&data);
        main_grid.attach(&array_view, 0, 1, 1, 1);
    } else if data.is_number() || data.is_string() {
        let value_view = build_value_view(&data);
        main_grid.attach(&value_view, 0, 1, 1, 1);
    }


    main_grid.set_hexpand(true);
    main_grid.set_vexpand(true);

    main_grid
}

fn build_value_view(data: &JsonValue) -> Box {
    let value = value_to_string(data);

    let button_copy = Button::new();
    let image_copy = Image::from_icon_name(Option::from("edit-copy-symbolic"), IconSize::Button);
    button_copy.add(&image_copy);

    let grid_component = Grid::new();
    grid_component.set_vexpand(true);
    grid_component.set_hexpand(true);
    grid_component.set_column_spacing(5);

    if value.len() > 22_usize {
        let label_value = Label::new(None);
        label_value.set_label(value.as_str());
        label_value.set_line_wrap(true);
        label_value.set_hexpand(true);
        label_value.set_vexpand(true);

        let scroll = ScrolledWindowBuilder::new().build();
        scroll.add(&label_value);
        scroll.set_vexpand(true);
        scroll.set_hexpand(true);
        scroll.set_propagate_natural_height(true);
        scroll.set_propagate_natural_width(true);

        grid_component.attach(&scroll, 0, 0, 1, 1);

        button_copy.connect_clicked(clone!(@weak label_value => move |_| {
            Clipboard::set_text(&gtk::Clipboard::get(&gdk::SELECTION_CLIPBOARD),
            label_value.get_label().as_str());
        }));
    } else {
        let entry_value = Entry::new();
        entry_value.set_text(value.as_str());
        entry_value.set_editable(false);

        grid_component.attach(&entry_value, 0, 0, 1, 1);

        button_copy.connect_clicked(clone!(@weak entry_value => move |_| {
            Clipboard::set_text(&gtk::Clipboard::get(&gdk::SELECTION_CLIPBOARD), entry_value.get_text().as_str());
        }));
    }

    grid_component.attach(&button_copy, 1, 0, 1, 1);

    let box_alignment_h = Box::new(Orientation::Horizontal, 0);
    let box_alignment_v = Box::new(Orientation::Vertical, 0);
    box_alignment_h.set_center_widget(Option::from(&grid_component));
    box_alignment_v.set_center_widget(Option::from(&box_alignment_h));
    box_alignment_v.set_hexpand(true);

    box_alignment_v
}

fn value_to_string(data: &JsonValue) -> String {
    if data.is_string() {
        data.as_str().unwrap().to_string()
    } else if data.is_number() {
        data.as_number().unwrap().to_string()
    } else {
        format!("Unknown value type! {}", data)
    }
}

fn build_values_box(data: &JsonValue) -> Box {
    let alignment_box_h = Box::new(Orientation::Horizontal, 0);
    let alignment_box_v = Box::new(Orientation::Vertical, 0);

    let list = Box::new(Orientation::Vertical, 5);
    for member in data.members() {
        if member.is_number() || member.is_string() {
            let value_view = build_value_view(member);
            list.add(&value_view);
        } else if member.is_object() {
            let object_view = build_object_box(member);
            list.add(&object_view);
        }
    }

    alignment_box_h.set_center_widget(Option::from(&list));
    alignment_box_v.set_center_widget(Option::from(&alignment_box_h));

    alignment_box_v
}

fn build_object_box(data: &JsonValue) -> Box {
    let alignment_box_h = Box::new(Orientation::Horizontal, 0);
    let alignment_box_v = Box::new(Orientation::Vertical, 0);

    let stack_object = Stack::new();
    stack_object.set_homogeneous(false);
    let sidebar_object = StackSidebar::new();
    sidebar_object.set_stack(&stack_object);

    for entry in data.entries() {
        if entry.1.is_string() || entry.1.is_number() {
            let value_view = build_value_view(entry.1);
            stack_object.add_titled(&value_view, entry.0, entry.0.to_uppercase().as_str());
        } else if entry.1.is_array() {
            let array_view = build_values_box(entry.1);
            stack_object.add_titled(&array_view, entry.0, entry.0.to_uppercase().as_str());
        } else if entry.1.is_object() {
            let object_view = build_object_box(entry.1);
            stack_object.add_titled(&object_view, entry.0, entry.0.to_uppercase().as_str());
        }
    }

    alignment_box_h.add(&sidebar_object);
    alignment_box_h.set_center_widget(Option::from(&alignment_box_v));
    alignment_box_v.set_center_widget(Option::from(&stack_object));

    alignment_box_h
}
