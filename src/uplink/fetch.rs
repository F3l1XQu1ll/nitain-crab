use std::path::PathBuf;

use downloader::{Download, Downloader};
use reqwest::Client;

use crate::uplink::cache;
use crate::uplink::cache::init::get_app_cache_dir;

//use super::load_overframe_items;

pub async fn do_fetch_stream(
    url: &str,
    client: Client,
) -> glib::bitflags::_core::result::Result<bytes::Bytes, reqwest::Error> {
    let request_url = reqwest::Url::parse(url).unwrap();
    let response: bytes::Bytes = client.get(request_url).send().await?.bytes().await?;
    Ok(response)
}

pub async fn do_fetch(url: &str, client: Client) -> Result<String, reqwest::Error> {
    let request_url = reqwest::Url::parse(url).unwrap();
    let response: String = client.get(request_url).send().await?.text().await?;
    //let json_response = json::parse(response.as_str());
    //json_response
    //json_response
    Ok(response)
}

pub async fn do_fetch_git(file: &str, client: Client) -> Result<String, reqwest::Error> {
    let mut path = PathBuf::new();
    path.push("data");
    path.push("json");
    path.push(file);
    if let Some(file) = cache::manage::get(path) {
        Ok(file)
    } else {
        let url = "https://raw.githubusercontent.com/WFCD/warframe-items/master/data/json/"
            .to_owned()
            + file;
        let response = do_fetch(url.as_str(), client).await?;
        Ok(response)
    }
}

pub fn do_fetch_github_api(url: &str) -> String {
    let client = reqwest::blocking::Client::new();
    match client
        .get(url)
        .header("Accept", "application/vnd.github.v3+json")
        .header("User-Agent", "request")
        .send()
    {
        Ok(response) => {
            if response.status() == reqwest::StatusCode::OK {
                match response.text() {
                    Ok(text) => text,
                    Err(_) => "Failed to read response!".to_string(),
                }
            } else {
                "Response was ".to_string() + response.status().as_str()
            }
        }
        Err(_) => "{\"inner_message\": \"Request failed!\"}".to_string(),
    }
}

pub fn fetch_github_bytes(url: &str) -> bool {
    let path = get_app_cache_dir().unwrap();
    let mut downloader = Downloader::builder()
        .download_folder(path.as_path())
        .parallel_requests(8)
        .user_agent("request")
        .build()
        .unwrap();
    let dl = Download::new(url);
    let result = downloader.download(&[dl]).unwrap();
    if !result.is_empty() {
        //let download_summary = result[0].clone();
        return match result.get(0).unwrap() {
            Ok(result) => {
                info!("Download successful: {}", result);
                true
            }
            Err(e) => {
                error!("Download error: {}", e.to_string());
                false
            }
        };
    }
    false
}

//#[deprecated(note = "load_overframe_items is deprecated")]
pub struct OverframeLink {}

#[deprecated(note = "load_overframe_items is deprecated")]
#[allow(deprecated)]
impl OverframeLink {
    fn block_load(&self, url: &str) -> Result<reqwest::blocking::Response, reqwest::Error> {
        reqwest::blocking::get(url)
    }

    fn walk_responses(&self, name: &str) -> u32 {
        let json_response = super::load_overframe_items();
        let results = json_response["results"].clone();
        //debug!("{}", json_response.get_keys());
        //debug!("{}", results.to_string());
        for result in results.members() {
            let res_name = result["name"].as_str().unwrap_or_default();
            if name.to_uppercase() == res_name && result["tag"].as_str().unwrap_or_default() != "" {
                    debug!("Found item!");
                    return result["id"].as_u32().unwrap_or_default();
            }
        }

        //debug!("Next iteration …");
        //let next = json_response["next"].as_str().unwrap_or_default();
        self.walk_responses(name)
    }

    pub(crate) fn find_builds(&self, name: &str) -> Option<String> {
        //let item_url = format!("https://overframe.gg/api/v1/items/");
        let item_id = self.walk_responses(name);
        if item_id > 0 {
            debug!("{}", item_id);
            let item_def_url = format!("https://overframe.gg/api/v1/builds/?item_id={}", item_id);
            match self.block_load(&item_def_url) {
                Ok(response) => {
                    debug!("Builds fetched");
                    let response_text = response.text().unwrap_or_default();
                    //debug!("{}", response_text);
                    Some(response_text)
                }
                Err(_) => {
                    warn!("Failed to load item definition at {}", item_def_url);
                    None
                }
            }
        } else {
            None
        }
    }
}
