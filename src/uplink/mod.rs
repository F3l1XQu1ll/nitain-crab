use log::*;
use reqwest::Client;

pub(crate) mod cache;
pub(crate) mod extractor;
pub(crate) mod fetch;
pub(crate) mod images;

/*pub struct Uplink {
    sender: glib::Sender<String>,
    socket: WebSocket<client::AutoStream>,
    ground_sender: glib::Sender<String>,
}*/

pub(crate) fn fetch(url: String) -> String {
    match reqwest::blocking::get(&url) {
        Ok(response) => {
            if response.status() == reqwest::StatusCode::OK {
                match response.text() {
                    Ok(text) => text,
                    Err(_) => "Failed to read response!".to_string(),
                }
            } else {
                "{\"inner_message\": \"Response was ".to_string()
                    + response.status().as_str()
                    + "\"}"
            }
        }
        Err(_) => "{\"inner_message\": \"Request failed!\"}".to_string(),
    }
}

pub fn get_nexus_completion(request: &str, limit: u32) -> Vec<json::JsonValue> {
    let mut url = "https://api.nexushub.co/warframe/v1/suggestions?query=".to_string();
    url = url + request + "&limit=" + limit.to_string().as_str();
    println!("Fetching {}", url);
    let suggestions_string = fetch(url);
    let suggestions_json = json::parse(&suggestions_string.as_str()).unwrap();

    let mut suggestions: Vec<json::JsonValue> = Vec::new();
    for suggestion in suggestions_json.members() {
        suggestions.push(suggestion.clone());
    }
    suggestions
}

pub fn get_warframe_hub_data(reqwest: &str, category: &str) -> json::JsonValue {
    let base_url = "https://api.warframestat.us";
    let url = format!(
        "{}/{}/{}",
        base_url,
        category
            .replace("Node", "items")
            .replace("Misc", "items")
            .replace("Sigils", "items")
            .replace("Primary", "Weapons")
            .replace("Secondary", "Weapons")
            .replace("Melee", "Weapons"),
        reqwest
    );
    println!("Fetching {}", url);
    let response = fetch(url);
    json::parse(&response).unwrap()
}

/*impl Uplink {
    pub fn new(
        ground_sender: glib::Sender<String>, // to send messages to the uplinks owner
    ) -> Uplink {
        let (mut socket, response) =
            connect(Url::parse("wss://api.nexushub.co/ws").unwrap()).expect("Can't connect");
        println!("Connected to the nexushub api backend!");
        println!("Response HTTP code: {}", response.status());
        println!("Response contains the following headers:");
        for (ref header, _value) in response.headers() {
            println!("* {}", header);
        }

        let (uplink_sender, uplink_receiver): (glib::Sender<String>, glib::Receiver<String>) = // sender to send commands to the uplink, receiver to handle these commands
            glib::MainContext::channel(glib::PRIORITY_DEFAULT);

        uplink_receiver.attach(None, |command| {
            socket.write_message(Message::Text(command)).unwrap();
            glib::Continue(true)
        });

        Uplink {
            sender: uplink_sender,
            ground_sender: ground_sender,
            socket: socket,
        }
    }

    fn start_worker(&self) {
        thread::spawn(move || loop {
            let msg = self.read();
            if msg.starts_with("\"primus::ping") {
                println!("Received ping!");
                let response_msg = msg.replace("ping", "pong");
                println!("Sending response: {}", response_msg);
            //self.write(response_msg);
            } else {
                // self.ground_sender.send(msg);
            }
        });
    }

    fn read(mut self) -> String {
        self.socket
            .read_message()
            .expect("Error reading message")
            .to_text()
            .unwrap()
            .to_string()
    }

    fn write(mut self, command: String) {
        self.socket.write_message(Message::Text(command)).unwrap()
    }
}*/

/*pub struct NexusLink {}

impl NexusLink {
    pub fn new(flotilla_sender: glib::Sender<String>) -> mpsc::Sender<String> {
        let (uplink_sender, uplink_receiver): (glib::Sender<String>, glib::Receiver<String>) =
            glib::MainContext::channel(glib::PRIORITY_DEFAULT);

        let (tx, rx): (mpsc::Sender<String>, mpsc::Receiver<String>) = mpsc::channel();
        thread::spawn(move || {
            let (mut socket, response) =
                connect(Url::parse("wss://api.nexushub.co/ws").unwrap()).expect("Can't connect");
            println!("Connected to the nexushub api backend!");
            println!("Response HTTP code: {}", response.status());
            println!("Response contains the following headers:");
            for (ref header, _value) in response.headers() {
                println!("* {}", header);
            }

            loop {
                let mut command_buffer_string = "".to_string();
                if let Ok(msg) = rx.try_recv() {
                    println!("{}", msg);
                    command_buffer_string = msg;
                }
                let recv = socket
                    .read_message()
                    .expect("Error reading message")
                    .to_text()
                    .unwrap()
                    .to_string();
                println!("{}", recv);
                if recv.starts_with("\"primus::ping") {
                    println!("Received ping!");
                    let response_msg = recv.replace("ping", "pong");
                    println!("Sending response: {}", response_msg);
                    socket.write_message(Message::Text(response_msg)).unwrap();
                } else {
                    flotilla_sender.send(recv);
                }

                if command_buffer_string != "".to_string() {
                    socket
                        .write_message(Message::Text(command_buffer_string.clone()))
                        .unwrap();
                    command_buffer_string = "".to_string();
                }
            }
        });
        tx
    }
}*/

pub async fn do_fetch(url: &str) -> std::result::Result<String, reqwest::Error> {
    let reqwest_url = reqwest::Url::parse(url).unwrap();
    let response = reqwest::get(reqwest_url).await?.text().await?;
    Ok(response)
}

#[deprecated(note = "Cloudfare killed it …")]
pub fn load_overframe_items() -> json::JsonValue {
    let rt = tokio::runtime::Runtime::new().expect("Failed to initialize the tokio runtime!");
    let async_response_item_count =
        rt.block_on(do_fetch("https://overframe.gg/api/v1/items/?limit=1"));
    match async_response_item_count {
        Ok(json_str) => {
            debug!("Response Body was '{}'", json_str);
            let json_object_item_count = json::parse(json_str.as_str()).unwrap();
            let item_count = json_object_item_count["count"].as_u32().unwrap();
            let async_response_items = rt.block_on(do_fetch(&*format!(
                "https://overframe.gg/api/v1/items/?limit={}",
                item_count.to_string().as_str()
            )));
            match async_response_items {
                Ok(json_response) => {
                    //debug!("Response Body for full item list was '{}'", json_response);
                    let json_object_items = json::parse(json_response.as_str()).unwrap();
                    json_object_items
                }
                Err(error) => {
                    warn!("{}", error.to_string());
                    return json::object! {
                        message: error.to_string()
                    };
                }
            }
        }
        Err(error) => {
            warn!("{}", error.to_string());
            return json::object! {
                message: error.to_string()
            };
        }
    }
}

pub fn create_client() -> Client {
    reqwest::Client::new()
}
