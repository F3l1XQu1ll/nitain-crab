use json::JsonValue;
use select::document::Document;
use select::node::Node;
use select::predicate::{Class, Name, Predicate};

use crate::uplink::create_client;
use crate::uplink::fetch::do_fetch;

pub fn scrape_weapon_info(weapon_url: &str) -> JsonValue {
    let rt = tokio::runtime::Runtime::new().expect("Failed to initialize the tokio runtime!");

    debug!("fetching {}", weapon_url);

    let async_page = rt.block_on(do_fetch(weapon_url, create_client())).unwrap();
    return if async_page.starts_with("{\"error") {
        json::object! {
            url: weapon_url,
            health_dat: json::object! {
                base: -1,
                max: -1,
                rank: -1,
            },
            shield_dat: json::object!{
                base: -1,
                max: -1,
                rank: -1,
            },
            armor: -1,
            health: -1,
            shield: -1,
        }
    } else {
        let document = Document::from(async_page.as_str());
        let aside = document.find(Name("aside")).next().unwrap();
        let titles: Vec<Node> = aside.find(Class("pi-title")).collect();
        //debug!("titles: {}", titles.len());
        let title_node = titles.first().unwrap();
        let title = title_node.text();

        let sections: Vec<Node> = aside.find(Name("section")).collect();
        //debug!("sections: {}", sections.len());
        let mut sections_json = json::Array::new();
        for section in sections {
            if section.attr("class").unwrap().contains("pi-collapse") {
                //debug!("section: {}", section.html());
                let name_node = section.find(Class("pi-header")).next().unwrap();
                let name = name_node.text();

                let attrs: Vec<Node> = section.find(Name("div").and(Class("pi-data"))).collect();
                let mut attrs_json = json::Array::new();
                for attr in attrs {
                    if let Some(name) = attr.attr("data-source") {
                        let title_node = attr.find(Class("pi-data-label")).next().unwrap();
                        let title = title_node.text();
                        let mut value: String = String::new();
                        match name {
                            "polarities" => {
                                let imgs: Vec<Node> = attr.find(Name("img")).collect();
                                if imgs.is_empty() {
                                    value.push_str("None");
                                } else {
                                    let mut values: Vec<&str> = Vec::new();
                                    for img in imgs {
                                        let alt = img.attr("alt").unwrap();
                                        let value_str = alt.split_whitespace().next().unwrap();
                                        values.push(value_str);
                                    }

                                    let first_value_str = values[0];
                                    value = first_value_str.to_string();
                                    for value_part in values.iter().take(values.len()).skip(1) {
                                        value.push_str(", ");
                                        value.push_str(value_part);
                                    }
                                }
                            }
                            "exilus polarity" => {
                                let img = attr.find(Name("img")).next().unwrap();
                                let alt = img.attr("alt").unwrap();
                                let value_str = alt.split_whitespace().next().unwrap();
                                value = value_str.to_string();
                            }
                            "stance polarity" => {
                                let img = attr.find(Name("img")).next().unwrap();
                                let alt = img.attr("alt").unwrap();
                                let value_str = alt.split_whitespace().next().unwrap();
                                value = value_str.to_string();
                            }
                            _ => {
                                let value_node = attr.find(Class("pi-data-value")).next().unwrap();
                                if let Some(span) = value_node
                                    .find(Name("span").and(Class("damagetype-tooltip")))
                                    .next()
                                {
                                    //debug!("{}", span.html());
                                    let data_param = span.attr("data-param").unwrap();
                                    //debug!("{}", data_param);
                                    let mut parts = Vec::new();
                                    for child in value_node.children() {
                                        if let Some(text_part) = child.as_text() {
                                            //debug!("{}", text_part);
                                            parts.push(text_part);
                                        }
                                    }
                                    value.push_str(parts[0]);
                                    //debug!("{}", value);
                                    if parts.len() > 1 {
                                        for part in parts.iter().skip(1) {
                                            value.push_str(data_param);
                                            value.push_str(part);
                                        }
                                    } else {
                                        if !value.ends_with(' ') {
                                            value.push(' ');
                                        }
                                        value.push_str(data_param);
                                    }

                                    //debug!("{}", value);
                                } else {
                                    value = value_node.text();
                                    //debug!("{}", value);
                                }
                            }
                        }

                        let attr_json = json::object! {
                            name: name,
                            title: title,
                            value: value,
                        };

                        attrs_json.push(attr_json);
                    } else {
                        continue;
                    }
                }

                let section_json = json::object! {
                    name: name,
                    attrs: attrs_json
                };
                sections_json.push(section_json);
            } else {
                let data_nodes = section.find(Class("pi-data-value"));
                let last_section = sections_json.last_mut().unwrap();
                for data_node in data_nodes {
                    let name = data_node.attr("data-source").unwrap();
                    let span = data_node.find(Name("span")).next().unwrap();
                    let title = span.attr("data-param").unwrap();
                    debug!("{}", title);
                    debug!("{}", span.html());
                    let parent = span.parent().unwrap();
                    let value = parent.text().trim().to_string();
                    let attr_json = json::object! {
                        name: name,
                        title: title,
                        value: value,
                    };
                    if let Err(e) = last_section["attrs"].push(attr_json) {
                        warn!("{}", e.to_string());
                    }
                }
            }
        }

        let mut info_json = json::object! {
            url: weapon_url,
            title: title,
        };
        if let Err(e) = info_json.insert("sections", sections_json) {
            warn!("{}", e.to_string());
        }

        info_json
    };
}
