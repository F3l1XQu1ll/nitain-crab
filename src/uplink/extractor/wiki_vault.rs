use crate::uplink::fetch::do_fetch;
use crate::uplink::create_client;

extern crate select;

use select::document::Document;
use select::predicate::Name;
use json::JsonValue;
use self::select::node::Node;

pub fn scrape_wiki_vault_list() -> JsonValue {
    let rt = tokio::runtime::Runtime::new().expect("Failed to initialize the tokio runtime!");
    let async_page = rt.block_on(do_fetch("https://warframe.fandom.com/wiki/Void_Relic/ByRewards/SimpleTable", create_client())).unwrap();

    let document = Document::from(async_page.as_str());
    let tables: Vec<Node> = document.find(Name("tbody")).collect();
    debug!("{}", tables.len());

    // assume only one table
    //let table = tables[0];

    let mut rows: Vec<Node> = tables[0].find(Name("tr")).collect();
    rows.remove(0); // remove header
    debug!("{}", rows.len());

    let mut objects = Vec::new();
    for row in rows {
        debug!("{}", row.text());
        let columns: Vec<Node> = row.find(Name("td")).collect();
        debug!("{}", columns.len());
        let tier = columns[2].text();
        debug!("{}", tier);
        let name = columns[3].first_child().unwrap().attr("data-param").unwrap().to_string();
        let vaulted_str = columns[5].text().replace("\n", ""); // todo: get value
        debug!("{}", vaulted_str);
        let vaulted;
        let str_vaulted = vaulted_str.as_str();
        debug!("'{}'", str_vaulted);
        match vaulted_str.as_str() {
            "Yes" => {
                debug!("In yes part");
                vaulted = true
            }
            _ => { vaulted = false }
        }

        debug!("{}", vaulted);

        let object = json::object! {
            tier: tier.to_lowercase(),
            name: name.to_lowercase(),
            vaulted: vaulted,
        };

        objects.push(object);
    }

    json::JsonValue::Array(objects)
}