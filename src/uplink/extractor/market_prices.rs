extern crate select;

use json::JsonValue;
use log::*;
use select::document::Document;

use crate::uplink::create_client;
use crate::uplink::fetch::do_fetch;

use self::select::node::Node;
use self::select::predicate::Attr;

pub fn scrape_market_prices(item: &str) -> JsonValue {// -> JsonValue {
    let rt = tokio::runtime::Runtime::new().expect("Failed to initialize the tokio runtime!");

    debug!("fetching {}", "https://warframe.market/items/".to_string() + item + "_intact");

    let async_page = rt.block_on(do_fetch(("https://warframe.market/items/".to_string() + item + "_intact").as_str(), create_client())).unwrap();
    return if async_page.starts_with("{\"error") {
        json::object! {
            name: item.to_string(),
            price: -1.0,
            url: "https://warframe.market/items/".to_string() + item + "_intact",
        }
    } else {
        let document = Document::from(async_page.as_str());

        let desc: Vec<Node> = document.find(Attr("property", "og:description")).collect();
        let desc_str = desc[0].attr("content").unwrap();
        let desc_parts: Vec<&str> = desc_str.split(" | ").collect(); // Price | Volume | Desc

        // "Price: {n} platinum"
        let price: f64 = desc_parts[0].replace("Price: ", "").replace(" platinum", "").parse().unwrap_or(-1.0);
        debug!("extracted price '{}'", price);

        json::object! {
            name: item.to_string(),
            price: price,
            url: "https://warframe.market/items/".to_string() + item + "_intact",
        }
    }
}