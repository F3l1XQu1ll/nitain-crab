use std::collections::HashMap;

use select::document::Document;
use select::{
    node::Node,
    predicate::{Attr, Name},
};

use crate::uplink::create_client;
use crate::uplink::fetch::do_fetch;

pub fn load_relic_drops(name: &str) -> Option<HashMap<String, String>> {
    debug!("Will load relic '{}'", name);
    let runtime = tokio::runtime::Runtime::new().unwrap();
    let async_page = runtime.block_on(do_fetch(
        "https://n8k6e2y6.ssl.hwcdn.net/repos/hnfvc0o3jnfvc873njb03enrf56.html",
        create_client(),
    ));
    if let Ok(page) = async_page {
        let document = Document::from(page.as_str());
        let rewards_label = document.find(Attr("id", "relicRewards")).next();
        if let Some(rewards_label) = rewards_label {
            debug!("found rewards_label");
            //debug!("{}", rewards_label.next().unwrap().html());
            //debug!("Split {}", split.html());
            //debug!("{}", split.next().unwrap().html());
            let table = rewards_label.next().unwrap();
            let headers = table.find(Name("th"));
            for header in headers {
                let relic_name = header.text();
                let fixed_name = relic_name
                    .replace("(", "")
                    .replace(")", "")
                    .replace("Relic", "")
                    .replace("  ", " ");
                debug!("Fixed {}", fixed_name);
                if name == fixed_name {
                    debug!("found info!");
                    //let parent = header.parent().unwrap().parent().unwrap();
                    //debug!("{}", parent.html());
                    let mut drops_list = HashMap::new();
                    let rows = header.parent().unwrap().parent().unwrap().find(Name("tr"));
                    for row in rows.skip(1) {
                        //debug!("{}", row.html());
                        if row.attr("class").unwrap_or_default() == "blank-row" {
                            break;
                        } else {
                            let columns = row.find(Name("td")).collect::<Vec<Node>>();
                            let drop_name = columns[0].text();
                            let drop_chance = columns[1].text();
                            debug!("{} {}", drop_name, drop_chance);
                            drops_list.insert(drop_name, drop_chance);
                        }
                    }
                    return Some(drops_list);
                }
            }
        }
    }
    None
}
