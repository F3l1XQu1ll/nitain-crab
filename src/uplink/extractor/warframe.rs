use json::JsonValue;
use select::document::Document;
use select::node::Node;
use select::predicate::{Attr, Class, Name, Predicate};

use crate::uplink::create_client;
use crate::uplink::fetch::do_fetch;

pub fn scrape_warframe_base(warframe_url: &str, prime: bool) -> JsonValue {
    let rt = tokio::runtime::Runtime::new().expect("Failed to initialize the tokio runtime!");

    debug!("fetching {}", warframe_url);

    let async_page = rt.block_on(do_fetch(warframe_url, create_client())).unwrap();
    return if async_page.starts_with("{\"error") {
        json::object! {
            url: warframe_url,
            health_dat: json::object! {
                base: -1,
                max: -1,
                rank: -1,
            },
            shield_dat: json::object!{
                base: -1,
                max: -1,
                rank: -1,
            },
            energy_dat: json::object!{
                base: -1,
                max: -1,
                rank: -1,
            },
            armor: -1,
            health: -1,
            shield: -1,
            energy: -1,
        }
    } else {
        let document = Document::from(async_page.as_str());

        let health_section: Vec<Node> = document.find(Attr("data-source", "health")).collect();
        let shield_section: Vec<Node> = document.find(Attr("data-source", "shieldcapacity")).collect();
        let armor_section: Vec<Node> = document.find(Attr("data-source", "armor")).collect();
        let energy_section: Vec<Node> = document.find(Attr("data-source", "powercapacity")).collect();
        let health_node;
        let shield_node;
        let armor_node;
        let energy_node;
        if prime && health_section.len() > 1 {
            health_node = health_section[1];
            shield_node = shield_section[1];
            armor_node = armor_section[1];
            energy_node = energy_section[1];
        } else {
            health_node = health_section[0];
            shield_node = shield_section[0];
            armor_node = armor_section[0];
            energy_node = energy_section[0];
        }
        let hp_info = health_node.find(Class("pi-data-value")).next().unwrap().text();

        let shield_info = shield_node.find(Class("pi-data-value")).next().unwrap().text();

        let armor_info = armor_node.find(Class("pi-data-value")).next().unwrap().text().parse().unwrap_or(-1);

        let energy_info = energy_node.find(Class("pi-data-value")).next().unwrap().text();

        let hp_values_str = filter_values(hp_info);
        let hp_values: Vec<&str> = hp_values_str.split_whitespace().collect();
        let hp_base = hp_values[0].parse().unwrap_or(-1);
        let hp_max = hp_values[1].parse().unwrap_or(-1);
        let hp_rank = hp_values[2].parse().unwrap_or(-1);

        let shield_values_str = filter_values(shield_info);
        let shield_values: Vec<&str> = shield_values_str.split_whitespace().collect();
        let shield_base = shield_values[0].parse().unwrap_or(-1);
        let shield_max = shield_values[1].parse().unwrap_or(-1);
        let shield_rank = shield_values[2].parse().unwrap_or(-1);

        let energy_values_str = filter_values(energy_info);
        let energy_values: Vec<&str> = energy_values_str.split_whitespace().collect();
        let energy_base = energy_values[0].parse().unwrap_or(-1);
        let energy_max = energy_values[1].parse().unwrap_or(-1);
        let energy_rank = energy_values[2].parse().unwrap_or(-1);

        json::object! {
            url: warframe_url,
            health_dat: json::object! {
                base: hp_base,
                max: hp_max,
                rank: hp_rank,
            },
            shield_dat: json::object!{
                base: shield_base,
                max: shield_max,
                rank: shield_rank,
            },
            energy_dat: json::object!{
                base: energy_base,
                max: energy_max,
                rank: energy_rank,
            },
            armor: armor_info,
            health: hp_max,
            shield: shield_max,
            energy: energy_max,
        }
    };
}

pub fn scrape_warframe_abilities(names: Vec<String>) -> Vec<JsonValue> {
    let rt = tokio::runtime::Runtime::new().expect("Failed to initialize the tokio runtime!");

    let mut ablities = Vec::new();

    for name in names {
        let url = String::from("https://warframe.fandom.com/wiki/") + name
            .replace("Wormhole", "Worm_Hole") //Fix Nova
            .replace("Amp", "Amp_(Ability)") //Fix Octavia
            .replace("Xata'S Whisper", "Xatas_Whisper") //Fix Xaku
            .replace("Grasp Of Lohk", "Grasp_of_Lohk") //Fix Xaku
            .replace("Thumper", "Thumper_(Elytron)") // Fix Elytron
            .replace(" ", "_")
            .as_str();
        debug!("{}", url);
        let client = create_client();
        let async_page = rt.block_on(do_fetch(url.as_str(), client)).unwrap();
        if async_page.starts_with("{\"error") {
            ablities.push(
                json::object! {
                    name: name,
                    info: "Failed to load information!",
                }
            );
        } else {
            let name_adj = name
                .replace("Wormhole", "Worm_Hole") // Fix Nova
                .replace("&", "and") //Fix Equinox
                .replace("Xata'S Whisper", "Xatas_Whisper") //Fix Xaku
                .replace(" ", "_")
                ;
            let document = Document::from(async_page.as_str());
            let collapsible_name = "mw-customcollapsible-".to_owned() + name_adj.as_str();
            let collapsible_name_of_lower = collapsible_name.replace("Of", "of");
            debug!("collabsible: '{}'", collapsible_name.as_str());
            //debug!("{}", document.find(Attr("id", collapsible_name.as_str())).count());
            let ability_collapsible = document.find(
                Attr("id", collapsible_name.as_str()).or(
                    Attr("id", collapsible_name_of_lower.as_str()))
            ).next().unwrap();
            let tabbertabs: Vec<Node> = ability_collapsible.find(Attr("class", "tabbertab")).collect();

            let mut ability_obj = json::object! {
                name: name.as_str(),
            };

            for tabbertab in tabbertabs {
                let title = tabbertab.attr("title").unwrap();
                let sub_infos: Vec<Node> = tabbertab.find(Name("li")).collect();
                if sub_infos.is_empty() {
                    let sub_info: Vec<Node> = tabbertab.find(Name("p")).collect();
                    if !sub_info.is_empty() {
                        let mut iter = sub_info.iter();
                        let info;
                        iter.next();
                        info = iter.next().unwrap().text();

                        if info != *"" {
                            match ability_obj.insert(title, info) {
                                Ok(_) => {}
                                Err(e) => { warn!("{}", e.to_string()) }
                            }
                        }
                    }
                } else {
                    let mut info_str = String::new();
                    let mut depth: Vec<u32> = Vec::new();
                    for sub_info in sub_infos {
                        let text = sub_info.text();
                        if text.lines().count() == 0 {
                            continue;
                        }

                        let line = text.lines().next().unwrap_or("");
                        let cur_line_child_count = (text.lines().count() - 1) as u32;

                        //debug!("depth {}", depth.len());
                        if !depth.is_empty() && depth[depth.len() - 1] == 0 {
                            let index = depth.len() - 1;
                            depth.remove(index);
                        } else if !depth.is_empty() {
                            let new_count = depth[depth.len() - 1] - 1;
                            let index = depth.len() - 1;
                            depth[index] = new_count;
                        }

                        if !depth.is_empty() {
                            for _i in 0..depth.len() {
                                info_str.push_str("    ");
                            }
                        }

                        info_str.push_str("∙ ");
                        info_str.push_str(line);
                        info_str.push('\n');

                        if cur_line_child_count > 0 {
                            //debug!("childs {}", cur_line_child_count);
                            depth.push(cur_line_child_count);
                        }
                    }

                    //debug!("{}", info_str);
                    match ability_obj.insert(title, info_str) {
                        Ok(_) => {}
                        Err(e) => { warn!("{}", e.to_string()) }
                    }
                }
            }

            ablities.push(ability_obj);
        }
    }
    ablities
}

fn filter_values(base_string: String) -> String {
    base_string.replace("(", "").replace(")", "").replace("at rank ", "")
}