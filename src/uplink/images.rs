use std::path::PathBuf;

use bytes::Bytes;
use reqwest::Client;

use crate::uplink::cache;
use crate::uplink::fetch::do_fetch_stream;

pub fn load_image(request: &str, client: Client) -> Bytes {
    let rt = tokio::runtime::Runtime::new().expect("Failed to initialize the tokio runtime!");
    let async_response = rt.block_on(do_fetch_stream(request, client.clone()));
    match async_response {
        Ok(image_bytes) => {
            image_bytes
        }
        Err(error) => {
            warn!("{}", error.to_string());
            load_image(request, client)
        }
    }
}

fn load_image_texture(texture: &str, client: Client, base_url: &str) -> Bytes {
    let request = base_url.to_string() + texture;
    load_image(&*request, client)
}

#[allow(dead_code)]
pub fn load_overframe_image(texture: &str, client: Client) -> Bytes {
    let base_url = "https://cdn.overframe.gg/MobileExport";
    load_image_texture(texture, client, base_url)
}

pub fn load_nexus_image(texture: &str) -> Bytes {
    let base_url = "https://nexushub.co";
    let client = Client::new();
    load_image_texture(texture, client, base_url)
}

pub fn load_git_image(texture: &str) -> Bytes {
    let mut path = PathBuf::new();
    path.push("data");
    path.push("img");
    path.push(texture);
    if let Some(bytes) = cache::manage::get_bytes(path) {
        bytes
    } else {
        let base_url = "https://raw.githubusercontent.com/WFCD/warframe-items/master/data/img/";
        let client = Client::new();
        load_image_texture(texture, client, base_url)
    }
}