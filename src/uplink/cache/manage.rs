use std::fs::{read, read_to_string, write};
use std::path::PathBuf;

use bytes::Bytes;

use crate::uplink::cache::init::{
    check_cache_exits, check_dir_exists, get_app_cache_dir, init_cache,
};

#[allow(dead_code)]
pub fn put(name: &str, content: &Bytes) -> Result<(), &'static str> {
    if check_cache_exits() {
        if let Some(mut path) = get_app_cache_dir() {
            path.push(name);
            if let Err(e) = write(path, content) {
                error!("{}", e.to_string());
                Err("Failed to write file")
            } else {
                Ok(())
            }
        } else {
            Err("Could not get path to cache!")
        }
    } else {
        match init_cache() {
            Ok(_) => {
                if let Err(e) = put(name, content) {
                    error!("{}", e.to_string());
                    Err("Failed to write file")
                } else {
                    Ok(())
                }
            }
            Err(e) => {
                error!("{}", e.to_string());
                Err("Failed to create Cache")
            }
        }
    }
}

pub fn check_file_exists(name: &str) -> bool {
    if let Some(mut path) = get_app_cache_dir() {
        path.push(name);
        let file = path.as_path();
        file.exists()
    } else {
        error!("Could not get path");
        false
    }
}

pub fn get(file: PathBuf) -> Option<String> {
    if check_cache_exits() {
        if let Some(mut path) = get_assets_dir() {
            path.push(file);
            if path.exists() {
                if let Ok(result) = read_to_string(path) {
                    Some(result)
                } else {
                    error!("Failed to read file!");
                    None
                }
            } else {
                error!("File not found! {}", path.to_str().unwrap());
                None
            }
        } else {
            error!("No cache dir");
            None
        }
    } else {
        error!("No cache!");
        None
    }
}

pub fn get_bytes(file: PathBuf) -> Option<Bytes> {
    if check_cache_exits() {
        if let Some(mut path) = get_assets_dir() {
            path.push(file);
            if path.exists() {
                if let Ok(result) = read(path) {
                    let bytes = Bytes::from(result);
                    Some(bytes)
                } else {
                    error!("Failed to read file!");
                    None
                }
            } else {
                error!("File not found! {}", path.to_str().unwrap());
                None
            }
        } else {
            error!("No chace dir!");
            None
        }
    } else {
        error!("No cache!");
        None
    }
}

pub fn get_assets_dir() -> Option<PathBuf> {
    let mut cache_path = get_app_cache_dir().unwrap();
    cache_path.push("assets");
    if !check_cache_exits() && !check_dir_exists(cache_path.as_path()) {
        None
    } else {
        let assets_content = cache_path.as_path().read_dir();
        if let Ok(mut assets_content) = assets_content {
            if let Some(Ok(first_enry)) = assets_content.next() {
                Some(first_enry.path())
            } else {
                None
            }
        } else {
            None
        }
    }
}
