use std::fs::{create_dir_all, remove_dir_all};
use std::path::{Path, PathBuf};

pub fn check_cache_exits() -> bool {
    if let Some(path) = get_app_cache_dir() {
        check_dir_exists(path.as_path())
    } else {
        panic!("Cache not supported!");
    }
}

pub fn get_app_cache_dir() -> Option<PathBuf> {
    let path = glib::get_user_cache_dir();
    if let Some(mut path) = path {
        path.push("nitain_crab");
        Some(path)
    } else {
        None
    }
}

pub fn init_cache() -> Result<(), &'static str> {
    if !check_cache_exits() {
        let path = get_app_cache_dir();
        if let Some(path) = path {
            if let Err(e) = create_dir_all(path.as_path()) {
                error!("{}", e.to_string());
                Err("Failed to create cache dir! Check debug info for details.")
            } else {
                Ok(())
            }
        } else {
            Err("Could not get path!")
        }
    } else {
        Ok(())
    }
}

pub fn clean_cache() -> Result<(), &'static str> {
    if check_cache_exits() {
        if let Some(path) = get_app_cache_dir() {
            if let Err(e) = remove_dir_all(path.as_path()) {
                error!("{}", e.to_string());
                Err("Failed to remove cache dir! Check debug info for details.")
            } else {
                init_cache()
            }
        } else {
            Err("Failed to get path!")
        }
    } else {
        Ok(())
    }
}

pub fn check_dir_exists(path: &Path) -> bool {
    path.exists()
}