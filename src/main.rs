#![windows_subsystem = "windows"]

extern crate gio;
extern crate gtk;
extern crate libhandy;
//extern crate webkit2gtk;
#[macro_use]
extern crate log;

use std::fs::{create_dir_all, File};
use std::process::exit;
use std::thread;

use gdk_pixbuf::Pixbuf;
use gio::prelude::*;
use gtk::{prelude::*, ToggleButton};
use gtk::{
    AboutDialogBuilder, Application, ApplicationWindow, Button, ButtonsType, DialogFlags,
    HeaderBar, IconSize, Image, License, MessageDialog, MessageType, Orientation, ResponseType,
    Stack, StackSwitcher, StackTransitionType,
};
use zip::ZipArchive;

use glib::clone;

use crate::gtk::SettingsExt;
use crate::uplink::cache;
use crate::uplink::cache::init::{clean_cache, get_app_cache_dir, init_cache};
use crate::uplink::fetch::{do_fetch_github_api, fetch_github_bytes};

mod message;
mod rigs;
mod uplink;
#[macro_use]
mod utils;
mod view;
mod world;

fn main() {
    env_logger::init();
    error!("[OK] Errors");
    warn!("[OK] Warnings");
    info!("[OK] Info");
    debug!("[OK] Debug");
    println!("Hello, world!");

    let application = Application::new(Some("com.archyt.nitaincrab"), Default::default())
        .expect("failed to initialize the gtk application");

    application.connect_activate(|app| {
        libhandy::init();
        // Load resources
        let resources_bytes = include_bytes!("../resources/resources.gresource");
        let resource_data = glib::Bytes::from(&resources_bytes[..]);
        //let res = gio::Resource::new_from_data(&resource_data).unwrap();
        let res = gio::Resource::from_data(&resource_data).unwrap();
        gio::resources_register(&res);

        let settings = gtk::Settings::get_default().unwrap();
        settings.set_property_gtk_application_prefer_dark_theme(true);
        settings.set_property_gtk_theme_name(Some("Adwaita"));
        settings.set_property_gtk_icon_theme_name(Some("Adwaita"));

        let css_provider = gtk::CssProvider::new();
        css_provider.load_from_resource("app_css");
        let screen = gdk::Screen::get_default();
        if let Some(screen) = screen {
            gtk::StyleContext::add_provider_for_screen(
                &screen,
                &css_provider,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );
        }

        let window = ApplicationWindow::new(app);
        window.set_title("Nitain Crab");
        let icon =
            Pixbuf::from_resource_at_scale("/com/archyt/Nitain_Crab/crab.svg", 124, 124, true)
                .unwrap();
        window.set_icon(Some(&icon));

        let main_stack = Stack::new();
        main_stack.set_transition_type(StackTransitionType::SlideLeftRight);

        let main_layout = gtk::Box::new(Orientation::Vertical, 0);
        let hidden_title = make_titlebar(&main_stack, icon.clone(), &window, &main_layout);
        //hidden_title.set_no_show_all(true);
        main_layout.add(&hidden_title);

        let title_bar = make_titlebar(&main_stack, icon, &window, &main_layout);
        window.set_titlebar(Option::from(&title_bar));

        let main_view_experimantal = view::modules::item_list::build_item_list();
        main_stack.add_titled(&main_view_experimantal, "experiment", "Items");

        let _view_stack = view::main_view::build_main();
        //main_stack.add_titled(&view_stack, "view", "Items");

        let world_stack = view::world::build_world();
        main_stack.add_titled(&world_stack, "world", "Worldstate");

        let maps_view = view::maps_rework::build_maps();
        main_stack.add_titled(&maps_view, "maps", "Maps");

        //let overframe_view = view::modules::builds::build_overframe_browsing_context();
        //main_stack.add_titled(&overframe_view, "overframe", "Overframe.gg");

        let mut simularicum = view::modules::simularicum::Simularicum::new();
        let simularicum_view = simularicum.build();
        main_stack.add_titled(&simularicum_view, "simulations", "Simularicum");

        main_layout.add(&main_stack);
        window.add(&main_layout);

        if let Ok(()) = init_cache() {
            let release_latest = do_fetch_github_api(
                "http://api.github.com/repos/wfcd/warframe-items/releases/latest",
            );
            let release_json = json::parse(release_latest.as_str()).unwrap();
            let name = release_json["name"]
                .as_str()
                .expect("Failed to download latest release information!");
            if !cache::manage::check_file_exists(name) {
                debug!("Assets not found");
                if let Err(e) = clean_cache() {
                    error!("{}", e.to_string());
                } else {
                    debug!("Cleaned Cache");
                }
                let zip_url = release_json["zipball_url"].as_str().unwrap();
                debug!("Zip: {}", zip_url);
                let info_dialog = MessageDialog::new(
                    Some(&window),
                    DialogFlags::MODAL,
                    MessageType::Info,
                    ButtonsType::None,
                    "Downloading Assets\nPlease wait …",
                );
                let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
                let tmp_url = String::from(zip_url);
                let tmp_name = String::from(name);
                let tmp_dialog = info_dialog.clone();
                thread::spawn(move || {
                    if !fetch_github_bytes(tmp_url.as_str()) {
                        error!("Assets download failed!");
                    } else {
                        let mut path = get_app_cache_dir().unwrap();
                        path.push(tmp_name);
                        let file = File::open(path).unwrap();
                        //let mut archive = ZipArchive::new(file).unwrap();
                        if let Ok(mut archive) = ZipArchive::new(file) {
                            let mut assets_path = get_app_cache_dir().unwrap();
                            assets_path.push("assets");
                            create_dir_all(assets_path.clone())
                                .expect("Failed to create assets folder");
                            let result = archive.extract(assets_path.as_path());
                            match result {
                                Ok(_) => {
                                    info!("Extracted Assets")
                                }
                                Err(e) => {
                                    error!("Failed to extract assets: {}", e.to_string())
                                }
                            }
                        } else {
                            error!("Failed to load downloaded archive!");
                            if let Err(e) = clean_cache() {
                                error!("{}", e.to_string());
                            } else {
                                exit(-1);
                            }
                        }
                    }
                    if let Err(e) = tx.send(true) {
                        warn!("{}", e.to_string())
                    }
                });
                rx.attach(None, move |_| {
                    debug!("Got notification");
                    tmp_dialog.close();
                    Continue(false)
                });

                let _response = info_dialog.run();
                info_dialog.close();
                //info_dialog.close();
            }
        } else {
            let error_dialog = MessageDialog::new(
                Some(&window),
                DialogFlags::MODAL,
                MessageType::Error,
                ButtonsType::Ok,
                "Cache initialisation failed!",
            );
            error_dialog.set_property_secondary_text(Some(
                "Could not initialize the cache.\nCheck the debug info.",
            ));
            if let ResponseType::Ok = error_dialog.run() {
                exit(0);
            }
        }

        window.show_all();
        hidden_title.hide();
    });

    application.run(&[]);
}

fn make_titlebar(
    main_stack: &Stack,
    icon: Pixbuf,
    window: &ApplicationWindow,
    main_layout: &gtk::Box,
) -> HeaderBar {
    let title_bar = HeaderBar::new();
    title_bar.set_show_close_button(true);
    let main_stack_switcher = StackSwitcher::new();
    main_stack_switcher.set_stack(Option::from(main_stack));
    let title_box = gtk::Box::new(Orientation::Horizontal, 10);
    title_box.set_center_widget(Option::from(&main_stack_switcher));
    title_bar.set_custom_title(Option::from(&title_box));

    title_bar.set_widget_name("Titlebar");

    const VERSION: &str = env!("CARGO_PKG_VERSION");

    let about_button = Button::new();
    let about_button_icon = Image::from_icon_name(Some("help-about-symbolic"), IconSize::Button);
    about_button.add(&about_button_icon);
    about_button.connect_clicked(move |_| {
        let about_builder = AboutDialogBuilder::new()
            .version(VERSION)
            .logo(&icon)
            .program_name("Nitain Crab")
            .website("https://gitlab.com/F3l1XQu1ll/nitain-crab")
            .authors(vec![
                "ArchyT aka Piikkisika".to_string(),
                "KenoS".to_string(),
            ])
            .artists(vec![
                "EliverLara - Theme, Icons".to_string(),
                "Kio-reki - Cetus Maps".to_string(),
                "Frame_Mastery - Fortuna Map".to_string(),
                "Charybdis2 - Necralisk Map".to_string(),
                "ArchyT - App Icon".to_string(),
            ])
            .license_type(License::Gpl30);
        let about = about_builder.build();
        //about.add_button("Close", ResponseType::Close);
        let response = about.run();
        match response {
            ResponseType::Close => about.close(),
            ResponseType::Cancel => about.close(),
            _ => about.close(),
        }
    });

    title_box.pack_end(&about_button, false, false, 0);

    let toggle_on_top = ToggleButton::new();
    toggle_on_top.set_label("Keep above");
    let tmp_window = window.clone();
    toggle_on_top.connect_toggled(move |button| {
        tmp_window.set_keep_above(button.get_active());
        tmp_window.grab_focus();
    });

    let toggle_fullscreen = ToggleButton::new();
    toggle_fullscreen.add(&image_by_name!(
        "view-fullscreen-symbolic",
        gtk::IconSize::Button
    ));
    // toggle_fullscreen.set_active(fullscreen);
    toggle_fullscreen.connect_clicked(
        clone!(@weak window, @weak main_layout, @weak title_bar, @weak toggle_on_top => move |button| {
            debug!("Fullscreen? {}", button.get_active());
            if button.get_active() {
                for child in  main_layout.get_children(){
                    if child.get_widget_name() == "Titlebar" {
                        child.show_all();
                    }
                }
                toggle_on_top.hide();
                title_bar.set_show_close_button(false);
                window.fullscreen();
                //window.remove(&title_bar);
                //main_layout.add(&title_bar);
            }else{
                let main_first_child = main_layout.get_children()[0].clone();
                main_first_child.hide();
                window.unfullscreen();
                toggle_on_top.show_all();
                title_bar.set_show_close_button(true);
                //window.show_all();
            }
        }),
    );

    let tmp_toggle_fullscreen = toggle_fullscreen.clone();
    //let tmp_toggle_on_top = toggle_on_top.clone();
    window.connect_window_state_event(move |_, state| {
        tmp_toggle_fullscreen.set_active(
            state
                .get_new_window_state()
                .contains(gdk::WindowState::FULLSCREEN),
        );
        //debug!("{:?}", state.get_new_window_state());
        Inhibit(false)
    });

    title_box.pack_start(&toggle_on_top, false, false, 10);
    title_box.pack_end(&toggle_fullscreen, false, false, 10);

    title_bar
}
