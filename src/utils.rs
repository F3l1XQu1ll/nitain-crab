use gtk::{prelude::BuilderExtManual, BoxExt, LabelExt};
use json::JsonValue;

pub fn json_extract_string(src: JsonValue) -> String {
    src.as_str().unwrap().to_string()
}

#[derive(Clone, Copy)]
pub enum ItemClass {
    Warframes,
    Primary,
    Secondary,
    Archwing,
    ArchGun,
    Melee,
    Mods,
    Arcanes,
    ArchMelee,
    Relics,
    Resources,
}

pub fn match_item_class_name(class: ItemClass) -> &'static str {
    match class {
        ItemClass::Warframes => "Warframes",
        ItemClass::Primary => "Primaries",
        ItemClass::Secondary => "Secondaries",
        ItemClass::Archwing => "Archwings",
        ItemClass::ArchGun => "Arch-Guns",
        ItemClass::Melee => "Melees",
        ItemClass::Mods => "Mods",
        ItemClass::Arcanes => "Arcanes",
        ItemClass::ArchMelee => "Arch-Melees",
        ItemClass::Relics => "Relics",
        ItemClass::Resources => "Resources",
    }
}

pub fn match_wiki(unique_name: &str) -> &str {
    match unique_name {
        // Archwings
        "/Lotus/Powersuits/Archwing/StandardJetPack/StandardJetPack" => {
            "https://warframe.fandom.com/wiki/Odonata"
        }
        "/Lotus/Powersuits/Archwing/SupportJetPack/SupportJetPack" => {
            "https://warframe.fandom.com/wiki/Amesha"
        }
        "/Lotus/Powersuits/Archwing/DemolitionJetPack/DemolitionJetPack" => {
            "https://warframe.fandom.com/wiki/Elytron"
        }
        "/Lotus/Powersuits/Archwing/StealthJetPack/StealthJetPack" => {
            "https://warframe.fandom.com/wiki/Itzal"
        }
        "/Lotus/Powersuits/Archwing/PrimeJetPack/PrimeJetPack" => {
            "https://warframe.fandom.com/wiki/Odonata"
        }
        // Bane Mods
        "/Lotus/Upgrades/Mods/Rifle/Expert/PrimedWeaponFactionDamageCorpus" => {
            "https://warframe.fandom.com/wiki/Primed_Bane_of_Corpus"
        }
        "/Lotus/Upgrades/Mods/Rifle/WeaponFactionDamageCorpus" => {
            "https://warframe.fandom.com/wiki/Bane_of_Corpus"
        }
        "/Lotus/Upgrades/Mods/Rifle/WeaponFactionDamageCorrupted" => {
            "https://warframe.fandom.com/wiki/Bane_of_Corrupted"
        }
        "/Lotus/Upgrades/Mods/Rifle/Expert/PrimedWeaponFactionDamageCorrupted" => {
            "https://warframe.fandom.com/wiki/Primed_Bane_of_Corrupted"
        }
        "/Lotus/Upgrades/Mods/Rifle/Expert/PrimedWeaponFactionDamageGrineer" => {
            "https://warframe.fandom.com/wiki/Primed_Bane_of_Grineer"
        }
        "/Lotus/Upgrades/Mods/Rifle/WeaponFactionDamageGrineer" => {
            "https://warframe.fandom.com/wiki/Bane_of_Grineer"
        }
        "/Lotus/Upgrades/Mods/Rifle/Expert/PrimedWeaponFactionDamageInfested" => {
            "https://warframe.fandom.com/wiki/Primed_Bane_of_Infested"
        }
        "/Lotus/Upgrades/Mods/Rifle/WeaponFactionDamageInfested" => {
            "https://warframe.fandom.com/wiki/Bane_of_Infested"
        }
        // Syndicate Mods
        "/Lotus/Upgrades/Mods/Syndicate/JawSwordMod" => {
            "https://warframe.fandom.com/wiki/Blade_of_Truth"
        }
        // Precepts
        "/Lotus/Types/Friendly/Pets/CatbrowPetPrecepts/CatbrowCatsEyePrecept" => {
            "https://warframe.fandom.com/wiki/Cat's_Eye"
        }
        // Augments
        "/Lotus/Powersuits/Sandman/SandmanBlastAugmentCard" => {
            "https://warframe.fandom.com/wiki/Desiccation's_Curse"
        }
        "/Lotus/Powersuits/Harlequin/IllusionAugmentCard" => {
            "https://warframe.fandom.com/wiki/Hall_of_Malevolence"
        }
        "/Lotus/Powersuits/Cowgirl/GunFuPvPAugmentCard" => {
            "https://warframe.fandom.com/wiki/Mesa's_Waltz"
        }
        "/Lotus/Powersuits/Brawler/BrawlerPunchAugmentCard" => {
            "https://warframe.fandom.com/wiki/Path_of_Statues"
        }
        "/Lotus/Powersuits/Trinity/WellOfLifeAugmentCard" => {
            "https://warframe.fandom.com/wiki/Pool_of_Life"
        }
        "/Lotus/Powersuits/Ranger/RangerQuiverPvPAugmentCard" => {
            "https://warframe.fandom.com/wiki/Power_of_Three"
        }
        "/Lotus/Powersuits/Necro/CloneTheDeadAugmentCard" => {
            "https://warframe.fandom.com/wiki/Shield_of_Shadows"
        }
        // Auras
        "/Lotus/Upgrades/Mods/Aura/RobotPoorAimAuraMod" => {
            "https://warframe.fandom.com/wiki/EMP_Aura"
        }
        // Stances
        "/Lotus/Weapons/Tenno/Melee/MeleeTrees/FistCmbThreeMeleeTree" => {
            "https://warframe.fandom.com/wiki/Fracturing_Wind"
        }
        // Mods
        "/Lotus/Upgrades/Mods/Shotgun/WeaponFireIterationsMod" => {
            "https://warframe.fandom.com/wiki/Hell's_Chamber"
        }
        "/Lotus/Upgrades/Mods/Melee/Event/ProjectNightwatch/RipkasNightwatchMod" => {
            "https://warframe.fandom.com/wiki/Hunter's_Bonesaw"
        }
        "/Lotus/Upgrades/Mods/Warframe/WarframeCatMod" => {
            "https://warframe.fandom.com/wiki/Kavat's_Grace"
        }
        "/Lotus/Upgrades/Mods/PvPMods/Shotgun/PassiveReloadMod" => {
            "https://warframe.fandom.com/wiki/Lock_and_Load"
        }
        "/Lotus/Upgrades/Mods/Warframe/AvatarLootRadarMod" => {
            "https://warframe.fandom.com/wiki/Thief's_Wit"
        }
        "/Lotus/Upgrades/Mods/Syndicate/FurisMod" => {
            "https://warframe.fandom.com/wiki/Winds_of_Purity"
        }
        // Railjack
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/ZektiFreeSuperWeaponAmmo" => {
            "https://warframe.fandom.com/wiki/Artillery_Cheap_Shot"
        }
        "/Lotus/Upgrades/Mods/Railjack/Abilities/CrewShipEMPAbilityCard" => {
            "https://warframe.fandom.com/wiki/Blackout_Pulse"
        }
        "/Lotus/Upgrades/Mods/Railjack/Piloting/ZektiSpeed" => {
            "https://warframe.fandom.com/wiki/Conic_Nozzle"
        }
        "/Lotus/Upgrades/Mods/Railjack/Abilities/CrewShipFlaresAbilityCard" => {
            "https://warframe.fandom.com/wiki/Countermeasures"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/LavanDamageOnKill" => {
            "https://warframe.fandom.com/wiki/Crimson_Fugue"
        }
        "/Lotus/Upgrades/Mods/Railjack/Piloting/ZektiNonCombatSpeed" => {
            "https://warframe.fandom.com/wiki/Cruising_Speed"
        }
        "/Lotus/Upgrades/Mods/Railjack/Engineering/LavanMaxShieldOnKill" => {
            "https://warframe.fandom.com/wiki/Defensive_Fire"
        }
        "/Lotus/Upgrades/Mods/Railjack/Tactical/FireExtinguish" => {
            "https://warframe.fandom.com/wiki/Fire_Suppression"
        }
        "/Lotus/Upgrades/Mods/Railjack/Engineering/LavanShieldOnCrit" => {
            "https://warframe.fandom.com/wiki/Fortifying_Fire"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/ZektiSuperWeaponDamage" => {
            "https://warframe.fandom.com/wiki/Forward_Artillery"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/VidarGunnerWeaponDamage" => {
            "https://warframe.fandom.com/wiki/Hyperstrike"
        }
        "/Lotus/Upgrades/Mods/Railjack/Piloting/ZektiSiegeMatrixAura" => {
            "https://warframe.fandom.com/wiki/Indomitable_Matrix"
        }
        "/Lotus/Upgrades/Mods/Railjack/Piloting/ZektiBoostSpeed" => {
            "https://warframe.fandom.com/wiki/Ion_Burn"
        }
        "/Lotus/Upgrades/Mods/Railjack/Engineering/VidarDefensiveMatrix" => {
            "https://warframe.fandom.com/wiki/Ironclad_Matrix"
        }
        "/Lotus/Upgrades/Mods/Railjack/Abilities/CrewShipAttractorAbilityCard" => {
            "https://warframe.fandom.com/wiki/Munitions_Vortex"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/VidarOffensiveMatrix" => {
            "https://warframe.fandom.com/wiki/Onslaught_Matrix"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/LavanFreeOrdnanceAmmo" => {
            "https://warframe.fandom.com/wiki/Ordnance_Cheap_Shot"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/LavanOrdnanceSpeed" => {
            "https://warframe.fandom.com/wiki/Ordnance_Velocity"
        }
        "/Lotus/Upgrades/Mods/Railjack/Engineering/LavanEngineerMatrix" => {
            "https://warframe.fandom.com/wiki/Orgone_Tuning_Matrix"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/LavanMaxOrdnanceMunitions" => {
            "https://warframe.fandom.com/wiki/Overloader"
        }
        "/Lotus/Upgrades/Mods/Railjack/Abilities/CrewShipRamAbilityCard" => {
            "https://warframe.fandom.com/wiki/Particle_Ram"
        }
        "/Lotus/Upgrades/Mods/Railjack/Abilities/CrewShipPhoenixAbilityCard" => {
            "https://warframe.fandom.com/wiki/Phoenix_Blaze"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/VidarGunnerWeaponCritChance" => {
            "https://warframe.fandom.com/wiki/Predator"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/LavanProtectiveShots" => {
            "https://warframe.fandom.com/wiki/Protective_Shots"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/LavanFastOrdnanceLock" => {
            "https://warframe.fandom.com/wiki/Quicklock"
        }
        "/Lotus/Upgrades/Mods/Railjack/Piloting/LavanHijackMatrixAura" => {
            "https://warframe.fandom.com/wiki/Raider_Matrix"
        }
        "/Lotus/Upgrades/Mods/Railjack/Piloting/LavanMultiToolPower" => {
            "https://warframe.fandom.com/wiki/Revo_Reducer"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/LavanReducedOrdnanceReload" => {
            "https://warframe.fandom.com/wiki/Ripload"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/LavanBypassShieldOrdnance" => {
            "https://warframe.fandom.com/wiki/Scourging_Warheads"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/VidarGunnerWeaponCritDamage" => {
            "https://warframe.fandom.com/wiki/Section_Density"
        }
        "/Lotus/Upgrades/Mods/Railjack/Abilities/CrewShipAttackAbilityCard" => {
            "https://warframe.fandom.com/wiki/Seeker_Volley"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/VidarSentientKiller" => {
            "https://warframe.fandom.com/wiki/Sentient_Scalpel"
        }
        "/Lotus/Upgrades/Mods/Railjack/Abilities/CrewShipThumperAbilityCard" => {
            "https://warframe.fandom.com/wiki/Shatter_Burst"
        }
        "/Lotus/Upgrades/Mods/Railjack/Abilities/CrewShipTetherAbilityCard" => {
            "https://warframe.fandom.com/wiki/Tether"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/VidarTurretRangeAndSpeed" => {
            "https://warframe.fandom.com/wiki/Turret_Velocity"
        }
        "/Lotus/Upgrades/Mods/Railjack/Abilities/CrewShipBlackHoleAbilityCard" => {
            "https://warframe.fandom.com/wiki/Void_Hole"
        }
        "/Lotus/Upgrades/Mods/Railjack/Tactical/VoidCloak" => {
            "https://warframe.fandom.com/wiki/Void_Cloak"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/LavanOrdnanceDamage" => {
            "https://warframe.fandom.com/wiki/Warhead"
        }
        "/Lotus/Upgrades/Mods/Railjack/Gunnery/LavanCritBypassShieldTurret" => {
            "https://warframe.fandom.com/wiki/Waveband_Disruptor"
        }

        &_ => {
            debug!("Unknown item '{}'; can not resolve wiki!", unique_name);
            ""
        }
    }
}

pub fn scale_label(label: &gtk::Label, scale_factor: f32) {
    if let Some(scale) = pango::Attribute::new_scale(scale_factor.into()) {
        let attrs_list = pango::AttrList::new();
        attrs_list.insert(scale);
        label.set_attributes(Some(&attrs_list));
    }
}

#[allow(unused)]
macro_rules! label {
    ($text: expr) => {
        gtk::Label::new(Some($text));
    };
}

macro_rules! block_load {
    ($url: expr) => {
        tokio::runtime::Runtime::new()
            .expect("Failed to initialize the tokio runtime!")
            .block_on(crate::uplink::do_fetch($url))
    };
}

macro_rules! image_by_name {
    ($name: expr) => {
        gtk::Image::from_icon_name(Some($name), gtk::IconSize::Dialog)
    };
    ($name: expr, $size: expr) => {
        gtk::Image::from_icon_name(Some($name), $size)
    };
}

pub fn labeled_widget<T: glib::IsA<gtk::Widget>>(widget: &T, label: &str) -> gtk::Box {
    let layout = gtk::BoxBuilder::new()
        .orientation(gtk::Orientation::Horizontal)
        .spacing(5)
        .build();
    let label_wdg = gtk::Label::new(Some(label));

    layout.pack_start(&label_wdg, false, false, 0);
    layout.pack_end(widget, false, false, 0);

    layout
}

pub fn generate_hdy_carousel_indicator_lines() -> libhandy::CarouselIndicatorLines {
    let source = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <!-- Generated with glade 3.38.2 -->
    <interface>
      <requires lib=\"gtk+\" version=\"3.24\"/>
      <requires lib=\"libhandy\" version=\"0.0\"/>
      <object class=\"HdyCarouselIndicatorLines\" id=\"indicator\">
        <property name=\"visible\">True</property>
        <property name=\"can-focus\">False</property>
      </object>
    </interface>";
    let builder = gtk::Builder::from_string(source);
    builder.get_object("indicator").unwrap()
}

macro_rules! benchmark {
    ($code: stmt) => {
        let benchmark_start = std::time::SystemTime::now();
        let benchmark_end = std::time::SystemTime::now();
        $code
        let benchmark_difference = benchmark_end.duration_since(benchmark_start).unwrap();
        debug!("Measured {:?}", benchmark_difference);
    };
    ($name:expr, $code: stmt) =>{
        debug!("Measuring {:?}", $name);
        let benchmark_start = std::time::SystemTime::now();
        let benchmark_end = std::time::SystemTime::now();
        $code
        let benchmark_difference = benchmark_end.duration_since(benchmark_start).unwrap();
        debug!("{:?} done; Measured {:?}", $name, benchmark_difference);
    }
}
