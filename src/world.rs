extern crate chrono;
extern crate gio;
extern crate glib;
extern crate gtk;
extern crate json;
extern crate timer;

pub struct NightwaveChallengeTypeDef {
    pub id: String,
    pub activation: String,
    pub expiry: String,
    pub is_daily: bool,
    pub is_elite: bool,
    pub title: String,
    pub desc: String,
    pub reputation: u32,
}

pub struct NightwaveTypeDef {
    pub id: String,
    pub activation: String,
    pub expiry: String,
    pub reward_types: Vec<String>,
    pub season: u32,
    pub tag: String,
    pub phase: u32,
    pub possible_challenges: Vec<NightwaveChallengeTypeDef>,
    pub active_challenges: Vec<NightwaveChallengeTypeDef>,
}

impl NightwaveChallengeTypeDef {
    pub fn new(
        id: String,
        activation: String,
        expiry: String,
        is_daily: bool,
        is_elite: bool,
        title: String,
        desc: String,
        reputation: u32,
    ) -> NightwaveChallengeTypeDef {
        NightwaveChallengeTypeDef {
            id,
            activation,
            expiry,
            is_daily,
            is_elite,
            title,
            desc,
            reputation,
        }
    }
}

impl NightwaveTypeDef {
    pub fn new(
        id: String,
        activation: String,
        expiry: String,
        reward_types: Vec<String>,
        season: u32,
        tag: String,
        phase: u32,
        possible_challenges: Vec<NightwaveChallengeTypeDef>,
        active_challenges: Vec<NightwaveChallengeTypeDef>,
    ) -> NightwaveTypeDef {
        NightwaveTypeDef {
            id,
            activation,
            expiry,
            reward_types,
            season,
            tag,
            phase,
            possible_challenges,
            active_challenges,
        }
    }
}
