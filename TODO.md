## TODO  

- [x] Auto-refresh Worldstate  
- [x] "Back to Overview" button at Fissures  
- [ ] Better layout for value stacks  
- [ ] Make builds from overframe.gg browseable  
- [ ] Merge builds into itemview  
- [ ] Templates  
    - [x] Basic Warframe Info
    - [x] Abilities Info
    - [x] Weapon stats
- [x] More accurate info from wiki regarding rank 30 items
