## Nitain Crab

Nitain Crab is a frontend for data provided by sites like  
 - hub.warframestat.us  
 - nexushub.com/warframe  
 - warframe.fandom.com  

The application uses the respective API for each platform and select-rs to acquire the necessary information. 

The provided maps belong to the associated creators:  
- Kio-reki_fishing.png - "Kio-reki"  
- Kio-reki_landmark_resources.png - "Kio-reki"  
- Kio-reki_mining.png - "Kio-reki"  
- Frame_Mastery_full.png - "Frame_Mastery"  
- Fandom_full.png - "/u/CMDR_Charybdis"/"Charybdis2"  
Your work is very appreciated!

### Building
For building on Linux have a look at the [Install](#Install) section.

On windows you will need msys2.
- Install the latest version from https://www.msys2.org/
- Run mingw64.exe (usually in C:\\msys2\) NOT msys64.exe!
- Execute ```pacman -Syu```
- Install build dependencies with ```pacman -S [package]``` (you will need the Mingw version): 
    - mingw-w64-x86_64-rust
    - mingw-w64-x86_64-pkg-config
    - mingw-w64-x86_64-gtk3
    - mingw-w64-x86_64-glib2
    - git
- now clone the repository (```git clone```)
- cd into the repository
- Execute ```cargo build --release```
    - If cargo complains about missing dependencies, look them up at https://packages.msys2.org/search?t=binpkg and install the mingw version (if there is one)
- After building, you can package the application using the install script.
    - ```chmod +x install.sh```
    - ```./install.sh windows```
        - The script will copy the compiled executables into the mingw /bin directory (C:\\msys2\mingw64\bin)
        - Ignore the warning about root access - on a real linux machine you would need root priviliges for the installation
    - Copy the mingw directory (C:\\msys2\mingw64) to a new location
    - Remove bloat like documentations, or leave the files untouched
        - Normally the bundles are very oversized, so removing bloat is recommended!
        - You can copy the bundle to another machine or remove the msys2 PATH variable to test what dependencies the application needs.
        - Make sure to test online functionality and link-buttons in the News view!
    - Zip the bundle for distribution

### Install
You can find binaries for the Windows platform at https://f3l1xqu1ll.itch.io/nitain-crab  
For Linux, install  
- gcc  
- gtk3  
- base-devel  
- rust  

Download the source from this repository, extract it and in the extracted run  
```cargo build --release```  
If there are any errors, install missing dependencies and inform us about that!  

If the build was successful, make the installation script executable:  
```chmod +x install.sh```  
Then execute ```sudo ./install.sh linux```.

Now nitain_crab is installed.

You may wish to create a nitain_crab.desktop file in ~/.local/share/applications/  

```
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Nitain Crab
Comment=Keep the knifes away!
Exec=nitain_crab
Icon=crab.svg
Terminal=false
```
