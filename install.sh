platform=$1

echo "Execute as root!"

if [[ $platform == "windows" ]]; then
  cp target/release/nitain_crab.exe /mingw64/bin/
  cp resources/crab.svg /mingw64/usr/share/icons/hicolor/scalable/apps/
  exit 0;
elif [[ $platform == "linux" ]]; then
  cp target/release/nitain_crab /usr/bin/
  cp resources/crab.svg /usr/share/icons/hicolor/scalable/apps/
  exit 0;
fi


echo "Usage: ./install.sh [platform]"
echo "platform: 'linux'/'windows'"

exit 1;
